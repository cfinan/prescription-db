=======================
``prescription_db`` API
=======================

The prescription-db package exposes some components in an API. Some of the build functions are available, although most will probably use the associated command line scripts.

Probably of most interest is the ORM, which can be used to issue queries in a database agnostic manor.

Some of the stats functions may also be useful.

.. toctree::
   :maxdepth: 4

   api/prescription_db

