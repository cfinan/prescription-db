``prescription_db`` package
===========================

``prescription_db.add_dicts`` module
------------------------------------

.. autofunction:: prescription_db.add_dicts.add_dict_file

``prescription_db.add_dmd`` module
----------------------------------

.. autofunction:: prescription_db.add_dmd.add_dmd

``prescription_db.add_file`` module
-----------------------------------

.. autofunction:: prescription_db.add_file.add_file

``prescription_db.add_snomed`` module
-------------------------------------

.. autofunction:: prescription_db.add_snomed.add_snomed

.. include:: ../data_dict/pres-db-orm.rst
