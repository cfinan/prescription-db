=========================
Current build information
=========================

Below is some basic build information/statistics for the current build. An SQLite copy can be downloaded `here <https://filedn.eu/lIeh2yR6LSw58g5HQMThnmR/pres_db_downloads/pres-db-dmd_2023_5_2.db.gz>`_. This will be to be passed through ``gunzip`` before use.

This is an I will endeavour to update these as frequently as possible. There were generated with the pres-db-stats :ref:`script <pres_db_stats>`.

Summary counts
--------------

The database contains the following number of unique prescription strings from all the sources that have been imported.

.. include:: ./stats/pres-str-total-count.rst

To understand how much coverage the various ENR sources have against the DM+D some counts wwere generated. This include prescription strings that are potentially truncated by virtue they end in an ellipsis (``...``), I noticed these in the UK Biobank derived data.

.. include:: ./stats/pres-str-source-counts.rst

Of the strings that are not present in the DM+D, there are several possible explainations:

1. They are incomplete, for example, the truncated strings.
2. They are gibberish, for example they could just be some random text that has crept in there.
3. They date from prior to 2014, when the DM+D in it's current guise was first released.
4. They are derived from other non-DM+D sources.

To investigate this, any Snomed codes that are associated with the strings that do not overlap with the DM+D were interrogated to identify the namespace from which they are derived.

.. include:: ./stats/pres-str-no-dmd-namespace.rst

The namespaces that are -1 are either badly formed codes, or completely missing codes (most likely)


