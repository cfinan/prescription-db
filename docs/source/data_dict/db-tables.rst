``data_source`` table
---------------------

A representation of the ``data_source`` table. A data source is a single source of data that can have one or more versions.

1. ``data_source_id`` (``INTEGER``) - Auto-incremented index.
2. ``data_source_name`` (``VARCHAR(255)``) - The name for the data source.
3. ``data_source_pretty`` (``VARCHAR(255)``) - The pretty name for the data source, i.e. one that can be used for display items.

``source_version`` table
------------------------

A representation of the ``source_version`` table. A source version is a specific version of a data source.

1. ``source_version_id`` (``INTEGER``) - Auto-incremented index.
2. ``data_source_id`` (``INTEGER``) - Foreign key link to the ``data_source`` table. Foreign key to ``data_source.data_source_id``.
3. ``version_name`` (``VARCHAR(255)``) - The version name/ID for the data source.

``pres_string`` table
---------------------

A representation of the ``pres_string`` table. This holds all of the raw prescription strings.

1. ``pres_str_id`` (``INTEGER``) - Auto-incremented index.
2. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
3. ``pres_str_name`` (``VARCHAR(700) COLLATE "NOCASE"``) - The text for the prescription string. This is indexed.
4. ``is_dmd`` (``BOOLEAN``) - Is the prescription string directly from the DM+D.
5. ``is_amp`` (``BOOLEAN``) - Is the prescription string an AMP.
6. ``is_truncated`` (``BOOLEAN``) - Are thee indicators that the prescription string has been truncated?.

``pres_string_snomed`` table
----------------------------

A representation of the ``pres_string_snomed`` table. This holds all the SnomedCT un-normalised relationship annotations for the prescriptions.

1. ``pres_str_snomed_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_id`` (``INTEGER``) - Foreign key link to the prescription string table. Foreign key to ``pres_string.pres_str_id``.
3. ``rel_distance`` (``INTEGER``) - The relationship distance.
4. ``concept_id`` (``BIGINT``) - The snomed concept ID for the relationship component.
5. ``concept_term`` (``VARCHAR(1000) COLLATE "NOCASE"``) - The snomed concept term for the relationship component.
6. ``type_id`` (``BIGINT``) - The snomed concept ID for the relationship type.
7. ``type_term`` (``VARCHAR(1000) COLLATE "NOCASE"``) - The snomed concept term for the relationship type.
8. ``char_type_id`` (``BIGINT``) - The snomed concept ID for the relationship characteristic type.
9. ``char_type_term`` (``VARCHAR(1000) COLLATE "NOCASE"``) - The snomed concept term for the relationship characteristic type.
10. ``modifier_id`` (``BIGINT``) - The snomed concept ID for the modifier type.
11. ``modifier_term`` (``VARCHAR(1000) COLLATE "NOCASE"``) - The snomed concept term for the modifier type.
12. ``code_match`` (``BOOLEAN``) - Has the relationship been matched on snomed concept code and not string.

``pres_string_occurs`` table
----------------------------

Table holding a mapping between the prescription strings and the data source they occur in. This differs from the source version in the ``pres_string`` table as this will have all the source versions that the string has been observed in not just the first one it was imported from.

1. ``pres_str_occurs_id`` (``INTEGER``) - Auto-incremented index.
2. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
3. ``pres_str_id`` (``INTEGER``) - Foreign key link to the ``pres_string`` table. Foreign key to ``pres_string.pres_str_id``.

``pres_string_code`` table
--------------------------

A representation of the ``pres_string_code`` table. This maps prescription strings back to associated DM+D (snomed) codes.

1. ``pres_str_code_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_id`` (``INTEGER``) - Foreign key link to the ``pres_string`` table. Foreign key to ``pres_string.pres_str_id``.
3. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
4. ``pres_str_code`` (``BIGINT``) - The DM+D code associated with the prescription string. This is indexed.
5. ``is_dmd`` (``BOOLEAN``) - Is the prescription string code directly from the DM+D as opposed to being imported from an EHR file.
6. ``is_verhoeff_valid`` (``BOOLEAN``) - Is the prescription string code valid according to the Verhoeff function.
7. ``check_digit`` (``INTEGER``) - The check digit of the SnomedCT code.
8. ``partition_id`` (``VARCHAR(7)``) - The partition ID of the SnomedCT code.
9. ``has_namespace`` (``BOOLEAN``) - Based on the partition ID does the SnomedCT code have a namespace ID.
10. ``code_type`` (``VARCHAR(12)``) - Based on the partition ID what is the type of code.
11. ``is_partition_valid`` (``BOOLEAN``) - Is the partition ID valid?.
12. ``namespace_id`` (``INTEGER``) - The namespace ID of the code.
13. ``is_namespace_valid`` (``BOOLEAN``) - Is the namespace ID valid?.
14. ``organisation`` (``VARCHAR(255)``) - The organisation that the SnomedCT code is defined from (based on the namespace ID).

``pres_string_count`` table
---------------------------

A representation of the ``pres_string_count`` table. This contains summary level counts for the number of times the prescription string is observed in the EHR records (``source_version_id``).

1. ``pres_str_count_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_id`` (``INTEGER``) - Foreign key link to the ``pres_string`` table. Foreign key to ``pres_string.pres_str_id``.
3. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
4. ``pres_str_count`` (``BIGINT``) - A count associated with the ``pres_string`` in the EHR. This is indexed.

``pres_string_meta_type`` table
-------------------------------

Counts of prescription strings observed in EHR records.

1. ``pres_str_meta_type_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_meta_type_name`` (``VARCHAR(255)``) - The name of the prescription string metadata type.

``pres_string_meta`` table
--------------------------

A representation of the ``pres_string_meta`` table. This contains any additional metadata that has been imported from a prescription string file.

1. ``pres_str_meta_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_id`` (``INTEGER``) - Foreign key link to the ``pres_string`` table. Foreign key to ``pres_string.pres_str_id``.
3. ``pres_str_meta_type_id`` (``INTEGER``) - Foreign key link to the ``pres_string`` metadata type table. Foreign key to ``pres_string_meta_type.pres_str_meta_type_id``.
4. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
5. ``pres_str_meta_value`` (``TEXT COLLATE "NOCASE"``) - The prescription string metadata value. This is indexed.

``token_lkp`` table
-------------------

A representation of the ``token_lkp`` table. This contains all of the tokens extracted from all of the the prescription strings.

1. ``token_id`` (``INTEGER``) - Auto-incremented index.
2. ``token_value`` (``VARCHAR(255)``) - The prescription string token value. This is indexed.

``pres_string_tokens`` table
----------------------------

A representation of the ``pres_string_tokens`` table. This contains mappings of tokens to prescription strings.

1. ``pres_str_token_id`` (``INTEGER``) - Auto-incremented index.
2. ``pres_str_id`` (``INTEGER``) - The foreign key link to the ``pres_string`` table. Foreign key to ``pres_string.pres_str_id``.
3. ``token_id`` (``INTEGER``) - The foreign key link to the ``token_lkp`` table. Foreign key to ``token_lkp.token_id``.
4. ``token_str_idx`` (``INTEGER``) - The rank position that the token occupies in the prescription string.
5. ``token_start_pos`` (``INTEGER``) - The starting character position that the token occupies in the prescription string.
6. ``token_end_pos`` (``INTEGER``) - The ending character position that the token occupies in the prescription string.
7. ``in_bracket`` (``BOOLEAN``) - Does the token occur within a bracketed region of the prescription string.
8. ``dict_id`` (``INTEGER``) - The link to dictionary term that was used to extract/describe the token. Foreign key to ``dict.dict_id``.
9. ``sub_class_id`` (``INTEGER``) - The link to sub class that the prescription string token belongs to. Foreign key to ``sub_class_lkp.sub_class_id``.

``base_form_lkp`` table
-----------------------

A representation of the ``base_form_lkp`` table. These are very basic forms for prescription components.

1. ``base_form_id`` (``INTEGER``) - Auto-incremented index.
2. ``base_form_name`` (``VARCHAR(50)``) - The name of the base level form.

``base_type_lkp`` table
-----------------------

A representation of the ``base_type_lkp`` table. These are very basic types for prescription components.

1. ``base_type_id`` (``INTEGER``) - Auto-incremented index.
2. ``base_type_name`` (``VARCHAR(50)``) - The name of the base type.

``route_admin_lkp`` table
-------------------------

A representation of the ``route_admin_lkp`` table. These are route of administration indicators.

1. ``route_admin_id`` (``INTEGER``) - Auto-incremented index.
2. ``route_admin_name`` (``VARCHAR(60)``) - The name of the route of administration.

``class_lkp`` table
-------------------

A representation of the ``class_lkp`` table. These are allowed types for prescription string dictionaries.

1. ``class_id`` (``INTEGER``) - Auto-incremented index.
2. ``class_name`` (``VARCHAR(50)``) - The name of the dict class.

``sub_class_lkp`` table
-----------------------

A representation of the ``sub_class_lkp`` table. Allowed types for prescription string dictionaries, slightly more specific that classes.

1. ``sub_class_id`` (``INTEGER``) - Auto-incremented index.
2. ``class_id`` (``INTEGER``) - Link back to the dict class. Foreign key to ``class_lkp.class_id``.
3. ``sub_class_name`` (``VARCHAR(50)``) - The name of the dict sub-class.

``dict`` table
--------------

A representation of the ``dict`` table. These are string tokens mapped to hints for form/type/ROA and a preferred name. This also acts as a general dictionary and can be used to expand dicts to standard types. The dicts might not be unique but should be unique across dict_name, dict_type_id and dict_standard_name. This allows for dict expansion.

1. ``dict_id`` (``INTEGER``) - Auto-incremented index.
2. ``name`` (``VARCHAR(255)``) - The name of the dict type. This is indexed.
3. ``standard_name`` (``VARCHAR(255)``) - A standardised name for the dict. This is indexed.
4. ``sub_class_id`` (``INTEGER``) - The foreign key link to the dict ``sub_class_lkp`` table. Foreign key to ``sub_class_lkp.sub_class_id``.
5. ``is_regexp`` (``BOOLEAN``) - Is the ``name`` a regular expression. This is indexed.
6. ``is_checked`` (``BOOLEAN``) - Has the ``name`` been manually checked. This is indexed.
7. ``is_auto_checked`` (``BOOLEAN``) - Has the ``name`` been computationally checked. This is indexed.

``dict_base_form`` table
------------------------

A representation of the ``dict_base_form`` table. Links between dict entries and base form hints.

1. ``dict_base_form_id`` (``INTEGER``) - Auto-incremented index.
2. ``dict_id`` (``INTEGER``) - The foreign key link back to the ``dict`` table. Foreign key to ``dict.dict_id``.
3. ``base_form_id`` (``INTEGER``) - The foreign key link back to the ``base_form_lkp`` table. Foreign key to ``base_form_lkp.base_form_id``.

``dict_base_type`` table
------------------------

A representation of the ``dict_base_type`` table. Links between dict entries and base type hints.

1. ``dict_base_type_id`` (``INTEGER``) - Auto-incremented index.
2. ``dict_id`` (``INTEGER``) - The foreign key link back to the ``dict`` table. Foreign key to ``dict.dict_id``.
3. ``base_type_id`` (``INTEGER``) - The foreign key link back to the ``base_type_lkp`` table. Foreign key to ``base_type_lkp.base_type_id``.

``dict_route_admin`` table
--------------------------

A representation of the ``base_form_lkp`` table. Links between dict entries and route of administration hints.

1. ``dict_route_admin_id`` (``INTEGER``) - Auto-incremented index.
2. ``dict_id`` (``INTEGER``) - The foreign key link back to the ``dict`` table. Foreign key to ``dict.dict_id``.
3. ``route_admin_id`` (``INTEGER``) - The foreign key link back to the ``route_admin_lkp`` table. Foreign key to ``route_admin_lkp.route_admin_id``.

``dmd_comb_prod_lkp`` table
---------------------------

A representation of the ``dmd_comb_prod_lkp`` table. This is a lookup table of indicators if a prescription is a combination product (composed of multiple things).

1. ``comb_prod_id`` (``INTEGER``) - Auto-incremented primary key.
2. ``comb_prod_desc`` (``VARCHAR(255)``) - The combination product description.

``dmd_dose_form_lkp`` table
---------------------------

A representation of the ``dmd_dose_form_lkp`` table. This is the dose form indicator lookup table from the DM+D.

1. ``dose_form_id`` (``INTEGER``) - Auto-incremented primary key.
2. ``dose_form_desc`` (``VARCHAR(20)``) - The dose form description.

``dmd_form_lkp`` table
----------------------

A representation of the ``dmd_form_lkp`` table, this is an analogue of the ``lkp_form`` table in the DM+D database.

1. ``form_id`` (``BIGINT``) - SnomedCT concept ID primary key.
2. ``form_id_date`` (``DATE``) - Date the code is applicable from.
3. ``form_id_prev`` (``BIGINT``) - Previous form code, upto 18 digits.
4. ``form_desc`` (``VARCHAR(60)``) - The form code description.

``dmd_route_lkp`` table
-----------------------

A representation of the ``dmd_route_lkp`` table. This is an analogue of the ``lkp_route`` table in the DM+D.

1. ``route_id`` (``BIGINT``) - SnomedCT concept ID primary key.
2. ``route_id_date`` (``DATE``) - The date the code is applicable from.
3. ``route_id_prev`` (``BIGINT``) - The previous route of administration code (max 18 digits).
4. ``route_desc`` (``VARCHAR(60)``) - The route of administration description.

``dmd_ingredient_lkp`` table
----------------------------

A representation of the ``dmd_ingredient_lkp`` table, this is an analogue of the ``ingredient`` table in the DM+D database.

1. ``ingredient_id`` (``BIGINT``) - SnomedCT concept ID primary key.
2. ``ingredient_id_date`` (``DATE``) - The date the ingredient substance identifier became valid.
3. ``ingredient_id_prev`` (``BIGINT``) - Previous ingredient substance identifier (SNOMED Code). Up to a maximum of 18 integers.
4. ``ingredient_invalid`` (``BOOLEAN``) - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
5. ``ingredient_name`` (``VARCHAR(255)``) - The ingredient substance name.

``dmd_prescription`` table
--------------------------

A representation of a prescription that is taken directly from the DM+D.

1. ``vmp_id`` (``BIGINT``) - SnomedCT concept ID primary key.
2. ``vtm_id`` (``BIGINT``) - foreign key to the VTM table. Foreign key to ``dmd_pres_moiety.vtm_id``.
3. ``n_vtms`` (``INTEGER``) - The number VTM entries.
4. ``dose_form_id`` (``INTEGER``) - The dose form of the prescription. Foreign key to ``dmd_dose_form_lkp.dose_form_id``.
5. ``comb_prod_id`` (``INTEGER``) - The indicator for the combination product type. Foreign key to ``dmd_comb_prod_lkp.comb_prod_id``.
6. ``dose_form_size`` (``FLOAT``) - Unit dose form size - a numerical value relating to size of entity. This will only be present if the unit dose form attribute is discrete. Up to a maximum of 10 digits and 3 decimal places.
7. ``dose_form_meas_id`` (``BIGINT``) - Unit dose form units - unit of measure code relating to the size. This will only be present if the unit dose form attribute is discrete. Narrative can be located in lookup file under tag <UNIT_OF_MEASURE> Up to a maximum of 18 digits. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.
8. ``unit_dose_meas_id`` (``BIGINT``) - Unit dose unit of measure - unit of measure code relating to a description of the entity that can be handled. This will only be present if the Unit dose form attribute is discrete. Narrative can be located in lookup file under tag <UNIT_OF_MEASURE>. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.

``pres_str_to_dmd`` table
-------------------------

A representation of the ``pres_str_to_dmd`` table. This maps prescription strings to DM+D prescription definitions.

1. ``pres_mapping_id`` (``INTEGER``) - The primary key.
2. ``pres_str_id`` (``INTEGER``) - The link to the prescription string table. Foreign key to ``pres_string.pres_str_id``.
3. ``vmp_id`` (``BIGINT``) - The link to the DMD prescription table. Foreign key to ``dmd_prescription.vmp_id``.

``dmd_pres_ing`` table
----------------------

A representation of the ``dmd_pres_ing`` table this is an analogue of the ``virt_prod_ing`` table in the DM+D database.

1. ``pres_ing_id`` (``INTEGER``) - The primary key.
2. ``vmp_id`` (``BIGINT``) - The foreign key link to the ``dmd_prescription`` table. Foreign key to ``dmd_prescription.vmp_id``.
3. ``ingredient_id`` (``BIGINT``) - The foreign key link to the ``dmd_ingredient_lkp`` table. Foreign key to ``dmd_ingredient_lkp.ingredient_id``.
4. ``stren_value_num`` (``FLOAT``) - Strength value numerator - value of numerator element of strength up to a maximum of 10 digits and 3 decimal places.
5. ``stren_value_num_unit_meas_id`` (``BIGINT``) - Strength value numerator unit - The foreign key link to the dmd unit of measure table. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.
6. ``stren_value_den`` (``FLOAT``) - Strength value denominator - value of denominator element of strength. Up to a maximum of 10 digits and 3 decimal places.
7. ``stren_value_den_unit_meas_id`` (``BIGINT``) - Strength value denominator unit - The foreign key link to the dmd unit of measure table. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.

``dmd_unit_measure_lkp`` table
------------------------------

A representation of the ``dmd_unit_measure_lkp`` table. This is an analogue of the ``lkp_unit_measure`` table in the DM+D database.

1. ``unit_meas_id`` (``BIGINT``) - The primary key, whilst this is set as an auto-increment but is sourced from the XML file. Apparently it is a SNOMED code up to 18 digits long.
2. ``unit_meas_id_date`` (``DATE``) - The date the code is applicable from.
3. ``unit_meas_id_prev`` (``BIGINT``) - Previous code, up to 18 digits.
4. ``unit_meas_desc`` (``VARCHAR(255)``) - The unit of measure description.

``dmd_pres_moiety`` table
-------------------------

A representation of the ``dmd_pres_moiety`` table this is an adaptation of the ``virt_ther_moit`` table in the DM+D database.

1. ``vtm_id`` (``BIGINT``) - Virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
2. ``vtm_invalid`` (``BOOLEAN``) - Invalidity flag. If set to 1 indicates this is an invalid entry in file. 1 integer only.
3. ``vtm_name`` (``VARCHAR(255)``) - Virtual therapeutic moiety name.
4. ``vtm_abbrev`` (``VARCHAR(60)``) - Virtual therapeutic moiety abbreviated name.
5. ``vtm_id_prev`` (``BIGINT``) - Previous virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of 18 integers.
6. ``vtm_id_date`` (``DATE``) - The date the virtual therapeutic moiety identifier became valid.

``dmd_pres_route`` table
------------------------

A representation of the ``dmd_pres_ing`` table this is an analogue of the ``virt_prod_ing`` table in the DM+D database.

1. ``pres_route_id`` (``INTEGER``) - The primary key.
2. ``vmp_id`` (``BIGINT``) - The foreign key link to the ``dmd_prescription`` table. Foreign key to ``dmd_prescription.vmp_id``.
3. ``route_id`` (``BIGINT``) - The foreign key link to the ``dmd_route_lkp`` table. Foreign key to ``dmd_route_lkp.route_id``.

``dmd_pres_form`` table
-----------------------

A representation of the ``dmd_pres_ing`` table this is an analogue of the ``virt_prod_ing`` table in the DM+D database.

1. ``pres_form_id`` (``INTEGER``) - Auto-incremented primary key.
2. ``vmp_id`` (``BIGINT``) - The foreign key link to the ``dmd_prescription`` table. Foreign key to ``dmd_prescription.vmp_id``.
3. ``form_id`` (``BIGINT``) - The foreign key link to the ``dmd_form_lkp`` table. Foreign key to ``dmd_form_lkp.form_id``.

``external_code_type_lkp`` table
--------------------------------

A representation of the ``external_code_type_lkp`` table.

1. ``external_code_type_id`` (``INTEGER``) - Auto-incremented primary key.
2. ``external_code_name`` (``VARCHAR(255)``) - The external code type name.

``dmd_external_code`` table
---------------------------

A representation of the ``dmd_external_code`` table this is uses data from the ``virt_med_prod_map`` table in the DM+D database.

1. ``external_code_id`` (``INTEGER``) - The primary key.
2. ``pres_str_id`` (``INTEGER``) - The foreign key link to the ``pres_string`` string table. Foreign key to ``pres_string.pres_str_id``.
3. ``source_version_id`` (``INTEGER``) - Foreign key link to the ``source_version`` table. Foreign key to ``source_version.source_version_id``.
4. ``external_code_type_id`` (``INTEGER``) - Foreign key link to the ``external_code_type_lkp`` table. Foreign key to ``external_code_type_lkp.external_code_type_id``.
5. ``code_id`` (``VARCHAR(255)``) - The external code ID.

``dmd_ddd`` table
-----------------

A representation of the ``dmd_external_code`` table this is uses data from the ``virt_med_prod_map`` table in the DM+D database.

1. ``ddd_id`` (``INTEGER``) - The primary key.
2. ``vmp_id`` (``BIGINT``) - The foreign key link to the ``dmd_prescription`` table. Foreign key to ``dmd_prescription.vmp_id``.
3. ``ddd`` (``FLOAT``) - The defined daily dose value.
4. ``ddd_unit_id`` (``INTEGER``) - Foreign key link to the ``dmd_unit_measure_lkp`` table. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.

