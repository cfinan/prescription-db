=======================================
Prescription mapping examples and notes
=======================================

Here I have placed some example code on building the resource from scratch and some initial exploration of the data.

The contents are listed in roughly chronological order so will make more sense if followed sequentially.

The various data exploration notebooks can be found in ``./resources/examples``. However, if any analyses is useful to build something into the database, it will be scripted and documented in :ref:`scripts <scripts>`. I will indicate this in the respective analysis.

.. toctree::
   :maxdepth: 4

   examples/database_build
   examples/overlaps
   examples/pres_type
   examples/string_match
