=========================
The prescription database
=========================

This details the building of the prescription database that is used as a development resource for the mapping, validation and annotation of NHS primary care prescription data.

The database aims to incorporate prescription data available in all DM+D versions back to 2014 (the first availability) and those observed in NHS primary care EHR data. primarily, the data from CPRD Gold, CPRD Aurum and UK Biobank GP data.

The resources from the primary care data will attempt to be validated against the DM+D and for those that can't be found. Annotations from SnomedCT and direct parsing of prescription strings will be employed for validation.

In addition, the validated prescriptions will be annotated against ChEMBL and Pubchem to link them to target data in the case of drugs, potential off target data derived from drug activity assays and indication/side effect data that is derived from the BNF, ChEMBL and First DataBank.

As with the DM+D the prescription database is mapped to SQLAlchemy ORM classes and all the scrpts will use SQLAlchemy. However, pretty much all development has been against SQLite and will need to be tested against other database backends.

There are some utility scripts available in ``./resources/bin`` that can be used in addition to the Python endpoints available in the package.

Initialising the database
-------------------------

The database can be created and initialised with the dictionaries available within the package using a bash script:

.. code-block::

   .resources/bin/init-pres-db.sh /data/dmd/pres-str.db

The loading of the prescription strings from pre-built :ref:`DM+D SQlite databases <building_dmd>` was completed as below:

.. code-block::

   ./pres-db-bulk-add-dmd.sh /data/dmd/pres-str.db /data/dmd/dmd_dbs/

Here the directory ``/data/dmd/dmd_dbs/`` contains the DM+D databases in year sub-directories i.e. ``2014``. The databases are loaded in reverse age order.

The various EHR data was present in several text files and loaded with a dedicated script. This allows for the incorporation of prescription strings, DM+D codes, external codes, for example BNF chapters and for counts of the number of observations for any prescription string. Only the prescription string column is required.

The UK Biobank prescription data was extracted in summary form from the ``gp_scripts.txt`` file with the following command:

.. code-block::

   cut -f 5,6,7 gp_scripts.txt | body sort -t$'\t' -T ~/Scratch/tmp/ | body uniq -c | perl -ne "s/^\s+//;s/\s/\t/;print $_"| awk 'BEGIN{FS="\t";OFS="\t"}{if (NR == 1) {print "count",$0} else {print $0}}' | gzip > ~/ukbb_prescription_counts.txt.gz

To load this data, which includes counts of prescription string occurrence the following command was used:

.. code-block:: console

   dmd-pres-db-file -vv --pres-str "drug_name" --dmd-code "dmd_code" --other-codes "bnf_code" --code-types "bnf_mixed"  --count "count" /data/dmd/ehr_drugs/ukbb_prescription_counts.txt.gz "uk_biobank" "20220901" /data/dmd/pres-str.db 
   === dmd-pres-db-file (dmd v0.2.1a0) ===
   [info] 09:50:43 > code_types length: 1
   [info] 09:50:43 > config value: 
   [info] 09:50:43 > config_section value: pres-db
   [info] 09:50:43 > count value: count
   [info] 09:50:43 > data_source value: uk_biobank
   [info] 09:50:43 > delimiter value: 	
   [info] 09:50:43 > dmd_code value: dmd_code
   [info] 09:50:43 > infile value: /data/dmd/ehr_drugs/ukbb_prescription_counts.txt.gz
   [info] 09:50:43 > other_codes length: 1
   [info] 09:50:43 > pres_db_url value: /data/dmd/pres-str.db
   [info] 09:50:43 > pres_str value: drug_name
   [info] 09:50:43 > source_version value: 20220901
   [info] 09:50:43 > tmp value: None
   [info] 09:50:43 > verbose value: 2
   [info] 09:50:43 > ensuring all pres-db tables are in place
   [info] reading input file: 99080 rows [4:57:56,  5.54 rows/s]
   [info] 14:48:40 > #prescription strings added: 50188
   [info] 14:48:42 > *** END ***

.. code-block:: console

   $ dmd-pres-db-file -vv --delimiter "," --pres-str "termfromemis" --dmd-code "prodcodeid" --other-codes "bnf" --code-types "bnf_mixed"  --count "n" /data/dmd/ehr_drugs/allprodcodes_count_bnf.csv.gz "cprd_aurum" "20190101" /data/dmd/pres-str.db 
   === dmd-pres-db-file (dmd v0.2.1a0) ===
   [info] 15:11:05 > code_types length: 1
   [info] 15:11:05 > config value: /data/dmd/ehr_drugs/allprodcodes_count_bnf.csv.gz
   [info] 15:11:05 > config_section value: pres-db
   [info] 15:11:05 > count value: n
   [info] 15:11:05 > data_source value: cprd_aurum
   [info] 15:11:05 > delimiter value: ,
   [info] 15:11:05 > dmd_code value: prodcodeid
   [info] 15:11:05 > infile value: /data/dmd/ehr_drugs/allprodcodes_count_bnf.csv.gz
   [info] 15:11:05 > other_codes length: 1
   [info] 15:11:05 > pres_db_url value: /data/dmd/pres-str.db
   [info] 15:11:05 > pres_str value: termfromemis
   [info] 15:11:05 > source_version value: 20190101
   [info] 15:11:05 > tmp value: None
   [info] 15:11:05 > verbose value: 2
   [info] 15:11:05 > ensuring all pres-db tables are in place
   [info] reading input file: 59567 rows [42:45, 23.22 rows/s]
   [info] 15:53:51 > #prescription strings added: 10816
   [info] 15:53:52 > *** END ***

