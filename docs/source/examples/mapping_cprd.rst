==========================
Mapping CPRD prescriptions
==========================

This is a complete run through of putting together the various databases and mapping/parsing `CPRD <https://www.cprd.com/>`_ prescription data. It assumes that you have the dmd package installed and you are running in a Linux system or something like `windows subsystem for linux <https://www.microsoft.com/en-gb/p/windows-subsystem-for-linux-preview/9p9tqf7mrm4r#activetab=pivot:overviewtab>`_.

Please note, this has been superseded by the prescription database. As the task of handling the mapping and annotation of prescription strings is sufficiently complex a development database was utilised for this purpose. Nevertheless, many of the component parts listed below are recylcled into the prescription string database. Please see the :ref:`prescription database <pres_database>` documentation for more details.

Getting the source data
=======================

This walk through uses a data file that was made available after someone issued a freedom of information request for the `mappings <https://www.whatdotheyknow.com/request/gemscript_drug_code_to_snomed_dm>`_ between gemscript codes (I think these are used by CPRD) and the DM+D. The data file was downloaded with the command below, this should output a file called ``dmd_mappings.txt``:

.. code-block:: console

   $ wget -O"dmd_mappings.txt" https://www.whatdotheyknow.com/request/680018/response/1609899/attach/2/gemscript%20dmd%202020%2007.txt?cookie_passthrough=1

Getting the DM+D
================

The `DM+D <https://www.nhsbsa.nhs.uk/pharmacies-gp-practices-and-appliance-contractors/dictionary-medicines-and-devices-dmd>`_ database can be downloaded from the NHS TRUD, You will have to register first and suscribe to the resource. You should also download the supplementary file as this has mappings between the DM+D and `ATC <https://www.who.int/tools/atc-ddd-toolkit/atc-classification>`_ codes.

However, given that the DM+D is updated weekly and has changed over time it is useful to have multiple copies of the DM+D. These are available as an archive but you have to contact NHS digital directly to issue a support request. This walk through assumes that all archived versions of the DM+D are available (along with the bonus files) and are arranged in a directory structure of ``./dmd_archive/<YYYY>/``. Although, if you only have a single copy of the DM+D you can still build a database (and warehouse database) from it and use it for mapping, you will just need to adapt the steps in this section for a single input file.

.. _building_dmd:

Build the DM+D database
-----------------------

The DM+D XML files can be built into a relational database using the ``dmd-build`` :ref:`script <dmd_build>`. This is quite easy to use. If you do not have access to any proper relational database servers then it is easiest to build into an `SQLite <https://sqlite.org/index.html>`_ backend, where the database is located in a single file. ``dmd-build`` uses `SQLAlchemy <https://www.sqlalchemy.org/>`_ so can be used to build against a variety of relational database backends. However, currently it has only been tested against SQLite and MySQL/`MariaDB <https://mariadb.org/>`_. In this walk through we will use SQLite.

Because we will be building an SQLite database for each DM+D release, we will put it in a small bash script that will accept the input year directory and the output year directory, additionally it will only build the database if a database file does not exist.

.. code-block:: bash

   #!/bin/bash
   # A script to build a year's worth of DM+D databases
   # will only build if they do not exist already
   indir="$1"
   outdir="$2"
   if [[ ! -d $outdir ]] || [[ ! -d $indir ]]; then
     echo "can't find input and/or output directory" 1>&2
     exit 1
   fi

   echo $indir
   echo $outdir
   mkdir -p $outdir
   set -e
   for i in "$indir"/nhsbsa_dmd_*.zip; do
     echo "$i"
     bonus_file=$(echo "$i" | sed 's/nhsbsa_dmd_/nhsbsa_dmdbonus_/')
     echo "$bonus_file"
     if [[ ! -e "$bonus_file" ]]; then
       echo "can't find bonus file: $bonus_file" 1>&2
       exit 1
     fi

     outdb="${outdir}"/$(basename "$i" | sed 's/.zip$/.db/')
     if [[ ! -e "$outdb" ]]; then
       echo "$outdb"
       dmd-build -v -B "$bonus_file" "$i" "$outdb"
     fi
   done

This will take a while to run, after which, we can use our databases to build a warehouse database that tracks changes in codes, names and ingredients over time, this will be the input for the mapping of the CPRD file.

Build the DM+D warehouse
------------------------

The DM+D warehouse can be built with the ``dmd-warehouse-build`` :ref:`script <dmd_wh>`. The DM+D warehouse build script is designed to either add a single database or you can provide a list of databases to add, in this case if a database is already present in the warehouse it will be skipped. The only prerequisite is that databases are added in their version order, this is because currently the warehouse  does not store everything only changes in the DM+D and the version where something changed. I am not completely happy with the warehouse schema so it may change in future.

The easiest approach is to generate a list of database connections. As we are using SQLite, this will be a list of paths to the individual files. Below is a short bash script to output a list that is sorted on version order from lowest to highest. ``dmd-warehouse-build`` does not require them to be sorted as it will organise that internally but it is nicer to have them that way. This script is also at ``./resources/bin/get_db_list.sh``.

.. code-block:: bash

   #!/bin/bash
   # Outputs a list of the DM+D databases sorted on version number, lowest to
   # highest. This assumes that the databases are in directories by year, and
   # have a .db extension and the original base file name. so
   # <ROOT>/YYYY/nhsbsa_dmd_*.db
   set -e
   db_root="$1"
   db_root=$(readlink -f "$db_root")

   # A temp script to build the warehouse database
   versions=(
       $(
           ls "$db_root"/*/*.db |
               awk '{file=$1; sub(/.*\/nhsbsa_dmd_[0-9]+\.[0-9]+\.[0-9]+_/, "", $1); sub(/\.db$/, "", $1); print $1, file}' |
               sort -k1n,1n |
               cut -f2 -d' '
       )
   )

   for i in "${versions[@]}"; do
       echo "$i"
   done

So it can be run as ``./get_db_list.sh /data/dmd/dmd_dbs/ > /data/dmd/dmd_dbs/dbs.list``. The ``dbs.list`` file can then be used as input to ``dmd-warehouse-build``, this is illustrated below.

.. code-block:: console

   $ dmd-warehouse-build -v list dbs.list dmd_warehouse.db

For full documentation of ``dmd-warehouse-build`` see the script :ref:`documentation page <dmd_wh>`.

Initial mapping
===============

With everything in place we can now attempt some mapping/validation of our CPRD file. The CPRD file that we are using contains a unique prescription entry within each row and has the following columns:

1. ``dmdcode`` - The DM+D code for the prescription entry. This might be a virtual medicinal product entry or an actual medicinal product entry.
2. ``gemscriptcode`` - I am not 100% sure about this, it could be used by CPRD.
3. ``productname`` - The prescription description string
4. ``drugsubstance`` - drugs that have been extracted somehow by CPRD (maybe by previous mapping against the DM+D), in the cases where > 1 drug is present in the prescription string then each drug name is separated by ``/``.
5. ``strength`` - The strength/units of each of the drugs, in the case where > 1 drug is in the prescription string then these are separated by a ``+``. In the case of multiple entries it is assumed that each strentch unit position corresponds to a ``drugsubstance`` position, although it is not known for sure.
6. ``formulation`` - The formulation, so is the drug a tablet or capsule etc...
7. ``route`` - The route of administration, so is it orally administered etc...

So this has a fare bit of information and has kind of been mapped already. Although there is a lot of missing data (particularly in the non-drug prescriptions) and some of the ``drugsubstance`` / ``strength`` columns are not optimal - ideally the drug and the concentrations should be together. So we can use the mapper to validate against all the known DM+D entries that go back to 2014 and re-extract the ingredients for those entries that do not map we can attempt ingredient lookups to ID the a current DM+D code that matches the ingredients and their strengths. It must be said that other prescription data, for example from the UK BioBank, does not have this amount of information.

The prescription mapper was run as in the command below. By default, it uses a cache of the warehouse database, this increases speed at the expense of higher memory usage. If you are not in need of faster processing time pass the ``--no-cache`` option. For all of the options for ``dmd-prescription-mapper`` see the :ref:`documentation page <prescription parser>`.

.. code-block:: console

   $ dmd-prescription-mapper -v -i ~/dmd_mappings.txt -o "/data/dmd/cprd_mapping" -O '<ALL>' -p "cprd_validation_" -D "dmdcode" -P "productname" -I "drugsubstance" -S "strength" "/data/dmd/dmd_warehouse.db"

The output from the mapper can be seen below (as we had the ``-v`` option on). We can see that using the cache it processed the file in just under an hour. For an explanation of the output see the :ref:`documentation page <mapper output>` .

.. code-block:: console

   = dmd-prescription-mapper (dmd v0.1.0a1) =
   [info] bnf_col value: None
   [info] config_file value: /home/rmjdcfi/.db_conn.cnf
   [info] delimiter value: 	
   [info] dmd_code_col value: dmdcode
   [info] formulation_col value: None
   [info] infile value: /home/rmjdcfi/dmd_mappings.txt
   [info] ing_del value: /
   [info] ing_str_col value: drugsubstance
   [info] no_cache value: False
   [info] outcols value: all
   [info] outdir value: /data/dmd/cprd_cache_mapping
   [info] prefix value: cprd_validation_
   [info] pres_str_col value: productname
   [info] roi_col value: None
   [info] stren_del value: +
   [info] stren_str_col value: strength
   [info] tmp value: None
   [info] verbose value: True
   [info] whurl value: /data/dmd/dmd_warehouse.db
   [info] caching names: 100% 473397/473397 [00:09<00:00, 49522.60 rows/s] 
   [info] caching ingredients: 100% 109446/109446 [00:03<00:00, 36481.84 rows/s]
   [info] caching IDs: 100% 172045/172045 [00:04<00:00, 35688.81 rows/s]
   [info] Processing...: 81367 rows [1:03:44, 21.27 rows/s]
   [info] all_missing: 180
   [info] bad_dmd: 7429
   [info] defined: 81367
   [info] no_map: 16851
   [info] mapped: 64336
   [info] amp_id_mapped: 0
   [info] vmp_id_mapped: 34831
   [info] amp_name_mapped: 0
   [info] vmp_name_mapped: 60560
   [info] ingredients_mapped: 30290
   [info] mapped: 79.24%
   [info] *** END ***

The mapper produces a file called ``/data/dmd/cprd_mapping/cprd_validation_summary.txt``. In addition to all the columns that were present in the input file, it also contains columns describing the mapping. For an explanation of the output see the :ref:`documentation page <mapper summary file>` .

So currently the mapper does not use the formulation or route of administration to refine any ingredient mappings, although this is a feature that is planned for the future. Overall we managed to validate 79.24% of the rows in the file, which is not too bad. However, there are 16851 entries that can't be validated against the DM+D in anyway. We still want the information from those so we will have to try something else.

Prescription string parsing
===========================

From the mapping above there are 16851 entries in the file that we can't locate. Some of these have some extracted ingredients/units in the file (and this should also be used in addition to parsing), while many do not. Therefore, we will attempt to extract information directly from the prescription string.

This essentially means extracting chunks and tokens directly from the prescription string and attaching a label to them. There are several ways this could be done and I have attempted to use neural networks to perform named entity recognition with some success. I will hopefully document that approach properly in the future. However, given the length of the prescription strings and their semi-structured nature. Using pre-built dictionaries and simple heuristics also offers a solution. This is the approach that is outlined below.

The dmd package contains some dictionaries of drugs, units, dose forms and rout of administration data, suppliers, ingredient terms. There have been extracted from a variety of sources. There are also some manual entries and some "static transformations" - which are lookup tables in situations where the prescription string is so badly formatted that computational approaches alone are not useful.

In addition to the unit dictionaries, there are also, is also a specific unit parser that attempts to recognise some of the common ways that units are portrayed in the prescription strings. Some small scale validation of a few hundred tagged prescription strings (that could not be located in the DM+D) suggests that the units overall ~78% of tags are correctly assigned and ~33% of prescription strings are perfectly parsed. Units seem to be well captured. I will eventually perform some proper validation on this and document the metrics properly.

Currently the prescription string parser exists as a standalone application but eventually it will be added to the mapper as well, as ingredient/unit extraction information can be matched with parsed and mapped entries to potentially map more prescriptions. Failing that, the extracted ingredients can be used as is.

The ``dmd-prescription-parser`` is :ref:`documented here <prescription parser>` and the processed used in this walk through is outlined below.

Extraction of unmapped rows
---------------------------

Initially, while the parser is being developed we will do the extraction on the unmapped subset of the file generated about ``cprd_validation_summary.txt``. This equates to the rows where the ``map_info`` column is 0.


.. code-block:: console

   $ header cprd_validation_summary.txt
   1 row_idx
   2 dmdcode
   3 gemscriptcode
   4 productname
   5 drugsubstance
   6 strength
   7 formulation
   8 route
   9 match_code
   10 match_vmp
   11 match_name
   12 match_info
   13 decoded_match_info
   14 ingredient_overlap
   15 ingredients
   16 other_ids

So we will extract anything in column 12 that has the value of 0 (``NO_MAP``).

.. code-block:: console

   $ awk 'BEGIN{FS="\t";OFS="\t"}{if (NR > 1) {if ($12 == 0) {print $0}} else {print $0}}' cprd_validation_summary_cache.txt > cprd_unmapped.txt

Running the parser
------------------

The chunk parser was then run over the un-mapped rows to ID and tag chunks in the prescription strings.

.. code-block:: console

   $ dmd-prescription-parser -v -i cprd_unmapped.txt -o cprd_unmapped.parsed.txt -P"productname"

For validation any untagged tokens were extracted, so that additional drugs and information can be extracted from them.

.. code-block:: console

   $ grep-column -i cprd_unmapped.parsed.txt -c"chunk_tags" -- "OTHER" | body sort -u -t$'\t' -k22,22 | cut -f1-24 > cprd_untagged.txt

The ``cprd_untagged.txt`` file is sorted on the ``chunk_value`` column. An explanation of the output file columns can be found in the :ref:`documentation <parser_output>`.

Outstanding tasks
=================

Below are some outstanding tasks that are needed for mapping/parsing.

Major tasks
-----------

* Parse out units and strengths in the CPRD file that do not match the DM+D - there is still a lot of info here. About 5048 rows where there are some ingredients (and possibly units), with 10177 where there is nothing. I think CPRD must have had access to old DM+D databases. The parser will need to check that the unit concentrations are valid i.e. are they within a max dose of any other drugs with that ingredient in the current DM+D, or FDA max dose, if not the how far out are they.
* Move the chunking parser to direct meaning, i.e. associating drugs/ingredients with units. Deriving ROA and dose form. Then ad-hoc specific extractors for non-drug components, i.e. garments, bandages, syringe types, catheters etc...
* Mapping of drug tradenames to their ingredient lists.
* Mapping to chemical structures
* Mapping to targets

Minor tasks
-----------

* Dictionary of homeopathic remedies.
* Unusual capturing of qualitative units ``IBUPROFEN S/R 300 MG CAP`` captures ``' S'``.
* Capture organism names, this is relevant for vaccinations, might need a separate vaccination parser.
* Specific parser for vitamins, these are often highly abbreviated, i.e ``ZINC & B6 TAB``.
* fix range unit recognition of this ``18 10-18ch`` in ``EMS pvc m Nelaton catheter T1010-18 10-18Ch (SSL International Plc)``
* Some leading ``.`` are not being captured in percent units i.e. ``.05 %`` is only capturing ``05 %`` but not in all cases!?
* Identify multipliers, i.e. ``6x`` in ``Magnesium phosphoricum 6x Tablet``, and more complex ones like ``Ethinylestradiol with levonorgestrel - triphasic 6x30+50mcg; 5x40+75mcg; 10x30+125mcg Tablet`` - This may require adding space around the multiplier value or a custom unit parser. Also stuff like this ``PLASTER DOUBLE SIDED 5''X5``.
* Implement manual mappings or really badly formatting strings, stuff like this ``FERROUS FUMAR.330MG/FOLIC ACID350MCG S/R 330 MG CAP``.
* Implement age capturing units, i.e. ``Elasticated Stockinette tops age 2-5y``
* Remove leading/lagging whitescpace from captured numerics.
* Capture missing drugs.
