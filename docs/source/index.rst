.. prescription-db documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prescription database
=====================

`prescription-db <https://gitlab.com/cfinan/prescription-db>`_ is a package to aggregate all the prescriptions observed in the DM+D and in primary care EHR resources into a developmental relational database, so the ingredients can be mapped, indications assigned and target/off target annotations can be added.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   setup

.. toctree::
   :maxdepth: 2
   :caption: Examples and exploratory analyses

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
