===============
Database schema
===============

The prescription database is really a development platform to annotate the prescription strings used in the NHS EHR and the DM+D. Therefore, different parts have been ingested from various resources using differing levels of normalisation.

Below is a diagram of the currently implemented schema with the ``pres_str_id`` links between the tables shown in green.

.. image:: ./_static/pres-db.png
   :width: 700
   :alt: prescription-db schema
   :align: center

.. include:: ./data_dict/db-tables.rst
