====================
Command-line scripts
====================

All of the build tasks are available as scripts that can be run on the command line. Please see examples for a full run through of the build process. Many of the analytical tasks have also been scripted even if they were first developed in notebooks. This offers reproducible way repeating the various analytical tasks.

There are two sources of scripts, the python scripts that are installed when the package is installed and a set of associated bash scripts that wrap some of the python endpoints to bulk load data. These are not essential but can be useful.

.. _scripts:

.. toctree::
   :maxdepth: 4

   scripts/python_scripts
   scripts/resources_scripts
