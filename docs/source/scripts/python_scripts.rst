====================
Command line scripts
====================

There are several command line scripts that are installed with the package. These are detailed below.

Building the prescription database
==================================

The prescription string database is a development database to aggregate all of the prescription strings in the DM+D and various EHR resources such as CPRD and UK BioBank. The prescription strings are then annotated with their ingredients, either directly from the DM+D or by parsing and cross referencing against SnomedCT. The eventual aim is to have prescription strings mapped to ingredients and standardised concentrations.

``pres-db-dict``
----------------

.. argparse::
   :module: prescription_db.add_dicts
   :func: _init_cmd_args
   :prog: pres-db-dict

.. _pres_db_dmd:

``pres-db-dmd``
---------------

.. argparse::
   :module: prescription_db.add_dmd
   :func: _init_cmd_args
   :prog: pres-db-dmd

.. _pres_db_file:

``pres-db-file``
----------------

.. argparse::
   :module: prescription_db.add_file
   :func: _init_cmd_args
   :prog: pres-db-file

``pres-db-snomed``
------------------

.. argparse::
   :module: prescription_db.add_snomed
   :func: _init_cmd_args
   :prog: pres-db-snomed

Example
-------

To load SnomedCT relationships that are up to 3 degrees of separation from the root source relationships for the prescription string. Also, drop any existing prescription string data. This is also using SQLite databases for both the prescription strings and SnomedCT, which is recommended as this has only been tested on SQLite:

.. code-block:: console

   $ pres-db-snomed --distance 3 --drop -vv --pres-db-url /data/dmd/pres-str.db --snomed-db-url /data/snomedct/snomed-ct-36-munge.db

The ouptut will be similar to this:

.. code-block:: console

   === dmd-pres-db-snomed (dmd v0.2.1a0) ===
   [info] 10:14:27 > config value: /home/rmjdcfi/.db.cnf
   [info] 10:14:27 > distance value: 3
   [info] 10:14:27 > drop value: True
   [info] 10:14:27 > pres_config_section value: pres-db
   [info] 10:14:27 > pres_db_url value: /data/dmd/pres-str.db
   [info] 10:14:27 > snomed_config_section value: pres-db
   [info] 10:14:27 > snomed_db_url value: /data/snomedct/snomed-ct-36-munge.db
   [info] 10:14:27 > tmpdir value: None
   [info] 10:14:27 > verbose value: 2
   [info] 10:14:27 > dropping previous snomed tables
   [info] 10:14:28 > ensuring all pres-db tables are in place
   [info] 10:14:28 > need to process #prescriptions: 296539
   [info] processing prescriptions: 10%|+++        | 29653/296539 [1:54:01<10:30:00 6.01 prescriptions/s]

``pres-db-stats``
-----------------

.. _pres_db_stats:

.. argparse::
   :module: prescription_db.stats
   :func: _init_cmd_args
   :prog: pres-db-stats
