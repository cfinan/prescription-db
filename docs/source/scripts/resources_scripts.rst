===========================
``./resources/bin`` scripts
===========================

In addition to the Python command-line scripts that are installed when the package is installed. There are also some bash administrative scripts located in ``./resources/bin``. Please note these will not be installed when you install via clone & pip or a conda install. If using conda you will have to clone the repo and add the ``./resources/bin`` directory to your PATH.

These scripts will require two bash libraries to be in your PATH.

1. ``shflags`` - `Used <https://github.com/kward/shflags>`_ to manage bash command line arguments.

2. ``bash-helpers`` - `This <https://gitlab.com/cfinan/bash-helpers>`_ wraps some handle bash functions.

``init-pres-db.sh``
-------------------

.. _init_db:

Initialise a copy of the prescription database and load all of the dictionaries within this package into it. This uses relative paths and is designed to be run from the ``./resources/bin`` directory. It also has only been tested against SQLite.

Example command
~~~~~~~~~~~~~~~

.. code-block::

   ./init-pres-db.sh /data/pres-db/pres-db.db

``pres-db-bulk-add-dmd.sh``
---------------------------

.. _dmd_load_db:

Load all available DM+D data into the prescription database. The DM+D versions should be SQLite databases built with the `dmd package <https://cfinan.gitlab.io/dmd/index.html>`_.

I often arrange the DM+D databases by year in sub-directories below the root DM+D directory that is supplied to this script.

Example command
~~~~~~~~~~~~~~~

.. code-block::

   ./pres-db-bulk-add-dmd.sh /data/pres-db/pres-db.db /data/dmd/dmd_dbs/

Usage
-----

.. code-block::

   ./pres-db-bulk-add-dmd.sh --help
   === pres-db-bulk-add-dmd ===
   Bulk load all the DM+D databases into the prescription string database. The
   <pres-str-db> should be the SQLite DM+D database and the <dmd-dir> should
   be the root of the directory that contains DM+D SQLite databases. These
   should be built with dmd-build. The databases are loaded from newest to
   oldest.

   USAGE: ./pres-db-bulk-add-dmd.sh [flags] <pres-str-db> <dmd-dir>
   flags:
     -h,--help:  show this help (default: false)

``pres-db-bulk-add-file.sh``
----------------------------

.. _file_load_db:

Load all available EHR summary data into the prescription database. This is designed to load specific summary files that can be downloaded from `here <https://filedn.eu/lIeh2yR6LSw58g5HQMThnmR/pres_db_downloads/ehr_prescriptions.tar>`_.


Example command
~~~~~~~~~~~~~~~

.. code-block::

   ./pres-db-bulk-add-file.sh /data/pres-db/pres-db.db ./ehr_prescriptions/

Usage
~~~~~

.. code-block::

    $ ./pres-db-bulk-add-file.sh --help
    === pres-db-bulk-add-file ===
    Bulk load the available EHR prescription summary data into the
    prescription string database. The files should have the preset names:

    cprd_gold_gemscript_dmd_2020_07.txt.gz
    cprd_aurum.txt.gz
    ukbb_prescription_counts.txt.gz

    With defined columns that can be seen in the source code of the script.
    This is not really designed as a generalisable script but more of a
    convienience script for loading the data in the prescription string
    characterisation project. The <pres-str-db> should be the SQLite DM+D
    database and the <file-dir> should be the root of the directory that
    contains the files listed above.

    USAGE: ./pres-db-bulk-add-file.sh [flags] <pres-str-db> <file-dir>
    flags:
      -h,--help:  show this help (default: false)


``pres-db-bulk-add-dmd.sh``
---------------------------

.. _dmd_load_db:

Load all available DM+D data into the prescription database. The DM+D versions should be SQLite databases built with the `dmd package <https://cfinan.gitlab.io/dmd/index.html>`_.

I often arrange the DM+D databases by year in sub-directories below the root DM+D directory that is supplied to this script.

Example command
~~~~~~~~~~~~~~~

.. code-block::

   ./pres-db-bulk-add-dmd.sh /data/pres-db/pres-db.db /data/dmd/dmd_dbs/

Usage
~~~~~

.. code-block::

   ./pres-db-bulk-add-dmd.sh --help
   === pres-db-bulk-add-dmd ===
   Bulk load all the DM+D databases into the prescription string database. The
   <pres-str-db> should be the SQLite DM+D database and the <dmd-dir> should
   be the root of the directory that contains DM+D SQLite databases. These
   should be built with dmd-build. The databases are loaded from newest to
   oldest.

   USAGE: ./pres-db-bulk-add-dmd.sh [flags] <pres-str-db> <dmd-dir>
   flags:
     -h,--help:  show this help (default: false)

``pres-db-bulk-add-file.sh``
----------------------------

.. _full_load_db:

Perform a full initialisation of all data into the prescription database. This uses the scripts above and a copy of the SnomedCT database (I use the munged version) build with the `snomed package <https://cfinan.gitlab.io/snomed-ct/>`_.


Example command
~~~~~~~~~~~~~~~

.. code-block::

   ./pres-db-full-build.sh /data/pres-db/pres-db.db ./ehr_prescriptions/ /data/snomed/snomed-ct.db

Usage
~~~~~

   $ ./pres-db-full-build.sh --help
   === pres-db-bulk-add-dmd ===
   Perform a full end-to-end build of the prescription string database, this will:

   1. Import the DM+D versions
   2. Insert all of the EHR files
   3. Add the SnomedCT annotations

   This could take a while to run, I would allow 24-36 hours.

   USAGE: ./pres-db-full-build.sh [flags] <pres-str-db> <dmd-dbs> <file-dir> <snomed-ct-db>

   The <snomed-ct-db> can be a path to an SQLite database or an intry in a
   config file.
   flags:
     -h,--help:  show this help (default: false)
