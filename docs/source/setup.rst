====================
Setup and background
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   schema
   basic_stats
