"""Add dictionary lists to the prescription database. The dictionary files must have the
 columns: entry - the dictionary lookup name or regex, class - the class for the dictionary entry, sub_class -
 the sub-class of the dictionary entry, is_regex is the dictionary entry a regular expression (0/1),
 standard_entry - the standardised version of entry,  base_form - (solid/liquid/
aerosol/absense/unknown), roa_hint - the route of administration that the dictionary entry
 hints at, i.e. oral, base_type_hint - the type that the dictionary entry hints at, i.e.
 drug, device.

Note that is roa, base_type, base_form, class/subclass are not one of the
 categories in the database, then KeyErrors will be raised. Refer to the
 `base_form_lkp`, `base_type_lkp`, `route_admin_lkp`, `class_lkp` and
 `sub_class_lkp` tables for allowed lists.

Dictionary files are expected to be tab-delimited and '' and . are treated as
 missing values.
"""

from prescription_db import (
    __version__,
    __name__ as pkg_name,
    orm as porm,
    common
)
from sqlalchemy_config import config as cfg
from pyaddons import utils, log
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from tqdm import tqdm
import argparse
import os
import sys
import csv
import pprint as pp


_PROG_NAME = 'dmd-pres-db-dict'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""
DELIMITER = "\t"
"""The delimiter of the token file (`str`).
"""
MISSING = ['.', '', None]
"""Missing values in the token file (`list` of `str` or `NoneType`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_dict``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    prog_verbose = log.progress_verbose(verbose=args.verbose)

    # Get a sessionmaker to create sessions to interact with the database
    sm = cfg.get_sessionmaker(
        args.config_section, common._DEFAULT_PREFIX, url_arg=args.dburl,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    try:
        # Make sure the database exists (or does not exist)
        cfg.create_db(sm, exists=True)
    except FileNotFoundError:
        # Not there so create
        cfg.create_db(sm, exists=False)

    # Get the session to query/load
    session = sm()

    try:
        # Add the dict file
        add_dict_file(session, args.indata, verbose=prog_verbose)
        # Create a temp directory that we will blitz if we error out
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        # TODO: remove if not used
        session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_dict_file(session, dict_file, verbose=False):
    """Add a dict file to the dicts table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    dict_file : `str`
        The path to the dict file.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.
    """
    common.create_tables(session)
    roa_map, bf_map, bt_map, cl_map, scl_map = common.create_lookups(session)

    open_method = utils.get_open_method(dict_file)
    with open_method(dict_file, 'rt') as incsv:
        reader = csv.DictReader(incsv, delimiter=DELIMITER)

        tqdm_kwargs = dict(
            desc="[info] checking entries...",
            unit=" rows",
            disable=not verbose
        )
        idx = 0
        commit_at = 1000
        for row in tqdm(reader, **tqdm_kwargs):
            row['entry'] = row['entry'].strip()
            if row['entry'].startswith('#'):
                continue
            if row['sub_class'] is None or row['sub_class'] == '':
                row['sub_class'] = 'none'

            try:
                # Does the dict already exist (by token name/sub-class)
                have_token = entry_exists(session, row['entry'],
                                          row['sub_class'])
            except MultipleResultsFound:
                # This should not happen
                pp.pprint(row)
                raise

            # If we do not have the token, then we will add it
            if have_token is False:
                try:
                    # Set the is regex to a boolean
                    bool(int(row['is_regex']))
                except Exception:
                    pp.pprint(row)
                    raise
                try:
                    add_entry(
                        session,
                        row['entry'],
                        row['class'],
                        row['sub_class'],
                        roa=row['roa_hint'],
                        base_type=row['base_type_hint'],
                        base_form=row['base_form'],
                        is_regex=bool(int(row['is_regex'])),
                        standard_entry=row['standard_entry'],
                        roa_map=roa_map,
                        bf_map=bf_map,
                        bt_map=bt_map,
                        sc_map=scl_map)
                except KeyError:
                    pp.pprint(row)
                    raise
            # Commit periodically
            idx += 1
            if idx == commit_at:
                session.commit()
                idx = 0
        session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def entry_exists(session, entry, entry_sub_class):
    """Test to see if the token exists in the token table. This is
    case-sensitive depending on the database distribution.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    token : `str`
        The entry name/regexp
    token_sub_class : `str`
        The sub-class of the entry.

    Returns
    -------
    entry_exists : `bool`
        True if the entry exists, False if not.
    """
    q = session.query(
        porm.Dict
    ).join(
        porm.SubClassLkp,
        porm.Dict.sub_class_id == porm.SubClassLkp.sub_class_id
    ).filter(
        and_(
            porm.Dict.name == entry,
            porm.SubClassLkp.sub_class_name == entry_sub_class
        )
    )

    try:
        q.one()
    except NoResultFound:
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_entry(session, entry_name, entry_class, entry_sub_class, roa=None,
              base_type=None, base_form=None, is_regex=False,
              standard_entry=None, roa_map=None, bf_map=None, bt_map=None,
              sc_map=None):
    """Add an dictionary entry to the dictionary table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    entry_name : `str`
        The dictionary entry name/regexp
    entry_class : `str`
        The dictionary entry class.
    entry_sub_class : `str`
        The sub-class of the dictionary entry.
    roa : `str`, optional, default: `NoneType`
        Any route of administration hint for the dictionary entry. If more than one they
        should be | separated strings.
    base_type : `str`, optional, default: `NoneType`
        Any base type hint for the dictionary entry. If more than one they should be |
        separated strings.
    base_form : `str`, optional, default: `NoneType`
        Any base form hint for the dictionary entry. If more than one they should be |
        separated strings.
    is_regex : `bool`, optional, default: `NoneType`
        Is the entry_name a regular expression.
    standard_entry : `str`, optional, default: `NoneType`
        A standardised version of the entry_name.
    roa_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, route of administration names (keys),
        their IDs in the lookup table. If not supplied then there are queried
        from the database.
    bf_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, base form names names (keys), their IDs
        in the lookup table. If not supplied then there are queried from the
        database.
    bt_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, base type names names (keys), their IDs
        in the lookup table. If not supplied then there are queried from the
        database.
    sc_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, dict-class, subclass tuples (keys), the
        dict sub-class IDs in the lookup table. If not supplied then there are
        queried from the database.
    """
    if base_form in MISSING:
        base_form = 'unknown'
    if base_type in MISSING:
        base_type = 'unknown'
    if roa in MISSING:
        roa = 'unknown'
    if standard_entry in MISSING:
        standard_entry = None

    entry_sub_class_id = None
    entry_base_form_id = []
    entry_base_type_id = []
    entry_roa_id = []
    try:
        if sc_map is None:
            q = session.query(porm.SubClassLkp).\
                filter(
                    and_(
                        porm.SubClassLkp.sub_class_name ==
                        entry_sub_class,
                        porm.ClassLkp.class_name == entry_class
                    )
                )
            res = q.one()
            entry_sub_class_id = res.token_sub_class_id
        else:
            entry_sub_class_id = \
                sc_map[(entry_class, entry_sub_class)]
    except (KeyError, NoResultFound) as e:
        raise KeyError(
            "unknown dictionary entry class/sub-class: "
            f"{entry_class}/{entry_sub_class}"
        ) from e

    # Get the IDs for the base forms of the entry being added
    entry_base_form_id = _get_ids(
        session, base_form, porm.BaseFormLkp,
        'base_form_name', 'base_form_id',
        map=bf_map,
        type="base_form"
    )
    # Get the IDs for the base types of the entry being added
    entry_base_type_id = _get_ids(
        session, base_type, porm.BaseTypeLkp,
        'base_type_name', 'base_type_id',
        map=bt_map,
        type="base_type"
    )
    # Get the IDs for the route of administration of the entry being added
    entry_roa_id = _get_ids(
        session, roa, porm.RouteAdminLkp,
        'route_admin_name', 'route_admin_id',
        map=roa_map,
        type="ROA"
    )

    # Now add the tokens and types, forms, ROA
    entry = porm.Dict(
        name=entry_name.strip(),
        sub_class_id=entry_sub_class_id,
        is_regexp=is_regex,
        standard_name=standard_entry,
        is_checked=False,
        is_auto_checked=False,
    )
    session.add(entry)
    for i in entry_base_form_id:
        session.add(porm.DictBaseForm(
            dict_link=entry,
            base_form_id=i
        ))
    for i in entry_base_type_id:
        session.add(porm.DictBaseType(
            dict_link=entry,
            base_type_id=i
        ))
    for i in entry_roa_id:
        session.add(porm.DictRouteAdmin(
            dict_link=entry,
            route_admin_id=i
        ))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_ids(session, search, orm_class, name_field, id_field, map=None,
             type="entity"):
    """Helper class to generalise the retrieval if IDs for different names
    where a mapping dictionary is optionally available.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    search : `str`
        The name to search for. This can be a pipe-separated string of multiple
        names. Which is processed into a list.
    orm_class : `str`
        The class of the table being queries.
    name_field : `str`
        The name of the field containing the names in the lookup table.
    id_field : `str`
        The name of the field containing the IDs in the lookup table.
    map : `dict`, optional, default: `NoneType`
        An optional mapping dictionary to use, mapping between names->IDs.
    type : `str`, optional, default: `entity`
        An optional string to use in any KeyError messages, if the IDs can't be
        found.

    Returns
    -------
    ids : `list` of `int`
       The IDs that map to the names in `search`.

    Raises
    ------
    KeyError
        If the IDs can't be retrieved for the search names.
    """
    names = [i.strip() for i in search.split("|")]
    ids = []
    for i in names:
        try:
            if map is not None:
                ids.append(map[i])
            else:
                q = session.query(orm_class).\
                    filter(getattr(orm_class, name_field) == i)
                res = q.one()
                ids.append(getattr(res, id_field))
        except (KeyError, NoResultFound) as e:
            raise KeyError(f"unknown, {type}: {i}") from e
    return ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'indata',
        type=str,
        help="A file with the dictionary input"
    )
    parser.add_argument(
        'dburl',
        nargs='?',
        type=str,
        help="An SQLAlchemy connection URL or filename if using SQLite. If you"
        " do not want to put full connection parameters on the cmd-line use "
        "the config file to supply the parameters"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
