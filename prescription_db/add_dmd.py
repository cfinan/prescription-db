"""A a version of the DM+D database to the prescription database. Currently
this only accepts DM+D databases in SQLite format but might be adapted in
future.
"""
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    orm as porm,
    common
)
from dmd import (
    common as dcommon,
    orm as dorm
)
from snomed_ct import common as scommon
from sqlalchemy_config import config as cfg
from pyaddons import log
from sqlalchemy import and_, union, literal
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from sqlalchemy.exc import IntegrityError
from tqdm import tqdm
import hashlib
import argparse
import re
import os
import sys
# import pprint as pp


_PROG_NAME = 'dmd-pres-db-dmd'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""
DELIMITER = "\t"
"""The delimiter of the token file (`str`).
"""
MISSING = ['.', '', None, 'NA', 'n/a', 'N/A']
"""Missing values in the token file (`list` of `str` or `NoneType`).
"""
ALIAS_EXT_CODE_TYPE = 'ext_code_type'
"""SQLAlchemy column query alias for the external code type name (`str`)
"""
ALIAS_EXT_CODE_ID = 'ext_code_id'
"""SQLAlchemy column query alias for the external code ID name (`str`)
"""
ALIAS_PRES_STR = 'pres_str'
ALIAS_CODE = 'code'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_dmd``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    pres_sm = cfg.get_sessionmaker(
        args.config_section, dcommon._DEFAULT_PREFIX, url_arg=args.pres_db_url,
        config_arg=args.config, config_env=None,
        config_default=dcommon._DEFAULT_CONFIG
    )

    # Get the session to query/load
    pres_session = pres_sm()

    # Get a sessionmaker to create sessions to interact with the database
    dmd_sm = cfg.get_sessionmaker(
        args.config_section, dcommon._DEFAULT_PREFIX, url_arg=args.indmd,
        config_arg=args.config, config_env=None,
        config_default=dcommon._DEFAULT_CONFIG
    )

    # Get the session to query/load
    dmd_session = dmd_sm()

    try:
        # Add the dict file
        add_dmd(pres_session, dmd_session, verbose=args.verbose)
        # Create a temp directory that we will blitz if we error out
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        pres_session.close()
        dmd_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'indmd',
        type=str,
        help="The path to the DM+D SQLite database to import"
    )
    parser.add_argument(
        'pres_db_url',
        nargs='?',
        type=str,
        help="The prescription database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_dmd(pres_session, dmd_session, verbose=False):
    """Add a DM+D database version to the prescription database.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.
    verbose : `bool` or `int`, optional, default: `False`
        Show progress of the DMD parsing. Set to True or 1 for logging and 2
        for progress monitoring.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    logger.info("ensuring all pres-db tables are in place")
    common.create_tables(pres_session)
    nadded = 0
    # Now update the dictionaries in the prescription database with entries
    # from the DM+D
    logger.info("update suppliers dictionary...")
    nadded = update_dict_suppliers(
        pres_session, dmd_session, verbose=prog_verbose
    )
    logger.info(f"added suppliers: {nadded}")

    logger.info("update drug dictionary...")
    nadded = update_dict_drugs(
        pres_session, dmd_session, verbose=prog_verbose
    )
    logger.info(f"added drugs: {nadded}")

    logger.info("update unit dictionary...")
    nadded = update_dict_units(pres_session, dmd_session, verbose=prog_verbose)
    logger.info(f"added units: {nadded}")

    # Make sure the DM+D source is included
    logger.info("checking DM+D version...")
    version = add_source(pres_session, dmd_session)
    logger.info(f"DM+D version: {version.version_name}")

    # First add any prescriptions that are not present
    logger.info("adding prescriptions...")
    nadded = add_prescriptions(
        version, pres_session, dmd_session, verbose=prog_verbose
    )
    logger.info(f"added #prescriptions: {nadded}")

    # Add the strings
    logger.info("adding all strings...")
    pres_str_cache = None
    nadded, pres_str_cache = add_strings(version, pres_session, dmd_session,
                                         verbose=verbose)
    logger.info(f"added #strings: {nadded}")

    # Add the codes
    logger.info("adding all codes...")
    nadded, code_cache = add_codes(version, pres_session, dmd_session,
                                   verbose=verbose,
                                   pres_str_cache=pres_str_cache)
    logger.info(f"added #codes: {nadded}")

    # Add the occurrences
    logger.info("adding all occurrences...")
    nadded, occurs_cache = add_occurrences(
        version, pres_session, dmd_session, pres_str_cache=pres_str_cache,
        occurs_cache=None, verbose=verbose
    )
    logger.info(f"added #occurrences: {nadded}")

    # Add the external codes
    logger.info("adding all external codes...")
    nadded, ext_code_cache = add_external_codes(
        version, pres_session, dmd_session, pres_str_cache=pres_str_cache,
        ext_code_cache=None, verbose=verbose
    )
    logger.info(f"added #external codes: {nadded}")

    # Add the links between prescription info and prescription strings
    logger.info("adding all prescription links...")
    nadded, pres_link_cache = add_prescription_link(
        pres_session, dmd_session, pres_str_cache=pres_str_cache,
        pres_link_cache=None, verbose=verbose
    )
    logger.info(f"added #prescription links: {nadded}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def hashstr(string):
    """Create an SHA1 hash (hex) of a prescription string.

    Parameters
    ----------
    string : `str`
        The prescription string to hash.

    Returns
    -------
    sha1_hash : `str`
        A 40 character hexadecimal hash of the prescription string.
    """
    return hashlib.sha1(string.encode('utf-8')).hexdigest()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_strings(session):
    """Create an in memory cache/mapping between prescription strings (sha1
    hashes) and prescription string IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.

    Returns
    -------
    str_cache : `dict`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`).
    """
    q = session.query(
        porm.PresString.pres_str_name,
        porm.PresString.pres_str_id
    )

    string_cache = {}
    for row in q:
        string_cache[hashstr(row.pres_str_name)] = row.pres_str_id
    return string_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_prescriptions(session):
    """Cache the VMP IDs that for the unique ID for each DMD prescription info.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.

    Returns
    -------
    pres_cache : `set` of `int`
        A cache of VMP IDs that have been loaded into the dmd_prescriptions
        table.
    """
    q = session.query(
        porm.DmdPrescription.vmp_id
    )

    pres_cache = set()
    for row in q:
        pres_cache.add(row.vmp_id)
    return pres_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_occurrence(session, data_source_id=None):
    """Cache all of the prescription string/occurrence data
    (as data_source_id).

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    data_source_id : `int`, optional, default: `NoneType`
        Only cache occurrence data from a specific data source ID. If not
        supplied then all occurrence data is cached.

    Returns
    -------
    occurs_cache : `set` of (`int`, `int`)
        A cache of (pres_str_id, data_source_id).

    Notes
    -----
    Note that in the occurrences data the source_version_id is stored but the
    cache caches at the level of data_source_id. This is so we only add in new
    data from the data source and not all the individual source data.
    """
    q = session.query(
        porm.PresStringOccurs.pres_str_id,
        porm.SourceVersion.data_source_id,
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.PresStringOccurs.source_version_id
    )

    if data_source_id is not None:
        q = q.filter(
            porm.SourceVersion.data_source_id == data_source_id
        )

    occurs_cache = set()
    for row in q:
        occurs_cache.add(
            (row.pres_str_id, row.data_source_id)
        )
    return occurs_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_external_codes(session, data_source_id=None):
    """Cache all of the external code data along with the data source it was
    imported from.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    data_source_id : `int`, optional, default: `NoneType`
        Only cache external code data from a specific data source ID. If not
        supplied then all external code data is cached.

    Returns
    -------
    ext_code_cache : `set` of (`int`, `int`, `str`, `int`)
        A cache of (pres_str_id, external_code_type_id, external_code_id,
        data_source_id).

    Notes
    -----
    Note that in the external code data the source_version_id is stored but the
    cache caches at the level of data_source_id. This is so we only add in new
    data from the data source and not all the individual source data.
    """
    q = session.query(
        porm.DmdExternalCode.pres_str_id,
        porm.DmdExternalCode.external_code_type_id,
        porm.DmdExternalCode.code_id,
        porm.SourceVersion.data_source_id,
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.DmdExternalCode.source_version_id
    )

    if data_source_id is not None:
        q = q.filter(
            porm.SourceVersion.data_source_id == data_source_id
        )

    exc_cache = set()
    for row in q:
        exc_cache.add(
            (
                row.pres_str_id, row.external_code_type_id,
                row.code_id, row.data_source_id
            )
        )
    return exc_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_pres_link(session):
    """Cache the prescription to string link table (``pres_str_to_dmd``).

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.

    Returns
    -------
    pres_link_cache : `set` of (`int`, `int`)
        A cache of (pres_str_id, vmp_id).
    """
    q = session.query(
        porm.PresStringDmd.pres_str_id,
        porm.PresStringDmd.vmp_id
    )

    cache = set()
    for row in q:
        cache.add((row.pres_str_id, row.vmp_id))
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_codes(session, data_source_id=None):
    """Cache the DM+D codes for each prescription string.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    data_source_id : `int`, optional, default: `NoneType`
        Only cache DMD code data from a specific data source ID. If not
        supplied then all DMD code data is cached.

    Returns
    -------
    dmd_code_cache : `set` of (`int`, `int`, `int`)
        A cache of (pres_str_id, pres_str_code (dmd code), data_source_id).

    Notes
    -----
    Note that in the pres_str_code data the source_version_id is stored but the
    cache caches at the level of data_source_id. This is so we only add in new
    data from the data source and not all the individual source data.
    """
    q = session.query(
        porm.PresStringCode.pres_str_id,
        porm.PresStringCode.pres_str_code,
        porm.SourceVersion.data_source_id,
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.PresStringCode.source_version_id
    )

    if data_source_id is not None:
        q = q.filter(
            porm.SourceVersion.data_source_id == data_source_id
        )

    code_cache = set()
    for row in q:
        code_cache.add(
            (row.pres_str_id, row.pres_str_code, row.data_source_id)
        )
    return code_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_prescriptions(version, pres_session, dmd_session, verbose=False,
                      pres_cache=None):
    """Parse all of the prescriptions from the DM+D into the prescriptions
    table.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring. Set to 2 to turn on progress monitors.
    pres_cache : `set` of `int`, optional, default: `NoneType`
        A cache of VMP IDs that have been loaded into the dmd_prescriptions
        table. If not supplied then it is queried from the database.

    Returns
    -------
    nadded : `int`
        The number of new prescriptions that have been added from the DM+D
        source database.
    """
    if pres_cache is None:
        pres_cache = cache_prescriptions(pres_session)

    q = dmd_session.query(
        dorm.VirtMedProd
    )

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc="[info] checking prescriptions",
        unit=" prescriptions",
        total=total,
        leave=False,
        disable=not verbose
    )

    # Count how many are added and commit every 1000
    idx = 0
    nadded = 0
    commit_at = 10000
    for row in tqdm(q, **tqdm_kwargs):
        if row.vmp_id not in pres_cache:
            add_prescription(pres_session, row)
            nadded += 1
            idx += 1

        if idx >= commit_at:
            pres_session.commit()
            idx = 0
    pres_session.commit()
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_strings(version, pres_session, dmd_session, verbose=False):
    """Parse VMPs into the prescription string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    pres_str_cache : `dict`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). This is updated for new
        additions after calling this function.
    """
    total_added = 0
    # Use all the virtual medicinal products
    q = dmd_session.query(dorm.VirtMedProd.vmp_name.label(ALIAS_PRES_STR))
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] checking VMP strings", is_amp=False
    )
    total_added += nadded

    # Abbreviated VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(ALIAS_PRES_STR)
    ).filter(
        dorm.VirtMedProd.vmp_name_abbrev != None
    )
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] adding abbreviated VMPs", is_amp=False
    )
    total_added += nadded

    # Previous VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(ALIAS_PRES_STR)
    ).filter(
        dorm.VirtMedProd.vmp_name_prev != None
    )
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] adding previous VMPs", is_amp=False
    )
    total_added += nadded

    # AMP descriptions
    q = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(ALIAS_PRES_STR)
    ).filter(
        dorm.ActMedProd.amp_desc != None
    )
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] adding AMP descriptions", is_amp=True
    )
    total_added += nadded

    # AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name.label(ALIAS_PRES_STR)
    ).filter(
        dorm.ActMedProd.amp_name != None
    )
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] adding AMP names", is_amp=True
    )
    total_added += nadded

    # Previous AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(ALIAS_PRES_STR)
    ).filter(
        and_(
            dorm.ActMedProd.amp_name_prev != None,
            dorm.ActMedProd.amp_name_prev != dorm.ActMedProd.amp_name
        )
    )
    nadded, pres_str_cache = _test_strings(
        version, pres_session, q, verbose=verbose, is_dmd=True,
        msg="[info] adding previous AMP names", is_amp=True
    )
    total_added += nadded

    return total_added, pres_str_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_codes(version, pres_session, dmd_session, pres_str_cache=None,
              code_cache=None, verbose=False):
    """Parse DMD codes (snomed) into the prescription string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        called from the database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    code_cache : `set` of (`int`, `int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, pres_str_code (dmd code), data_source_id). If
        not supplied then it is called from the database.

    Returns
    -------
    nadded : `int`
        The number of new codes that have been added from the DM+D source
        database.
    code_cache : `set` of (`int`, `int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, pres_str_code (dmd code), data_source_id).
        This is updated with new codes that have been added.
    """
    total_added = 0
    str_alias = 'pres_str'
    code_alias = 'code'

    # VMPs have two code columns Now add the VMP codes
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias),
        dorm.VirtMedProd.vmp_id.label(code_alias)
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias),
        dorm.VirtMedProd.vmp_id_prev.label(code_alias)
    ).filter(
        dorm.VirtMedProd.vmp_id_prev != None
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q1.union_all(q2), verbose=verbose,
        msg="[info] checking VMP name codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    # Abbreviated VMPs
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
        dorm.VirtMedProd.vmp_id.label(code_alias)
    ).filter(
        dorm.VirtMedProd.vmp_name_abbrev != None,
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
        dorm.VirtMedProd.vmp_id_prev.label(code_alias)
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_abbrev != None,
            dorm.VirtMedProd.vmp_id_prev != None
        )
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q1.union_all(q2), verbose=verbose,
        msg="[info] checking VMP abbrev. codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    # Previous VMPs
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias),
        dorm.VirtMedProd.vmp_id.label(code_alias)
    ).filter(
        dorm.VirtMedProd.vmp_name_prev != None,
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias),
        dorm.VirtMedProd.vmp_id_prev.label(code_alias)
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_prev != None,
            dorm.VirtMedProd.vmp_id_prev != None
        )
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q1.union_all(q2), verbose=verbose,
        msg="[info] checking VMP prev. codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    # AMP descriptions
    q = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(str_alias),
        dorm.ActMedProd.amp_id.label(code_alias)
    ).filter(
        dorm.ActMedProd.amp_desc != None
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP desc. codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    # AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name.label(str_alias),
        dorm.ActMedProd.amp_id.label(code_alias)
    ).filter(
        dorm.ActMedProd.amp_name != None
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP name codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    # Previous AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(str_alias),
        dorm.ActMedProd.amp_id.label(code_alias)
    ).filter(
        dorm.ActMedProd.amp_name_prev != None
    )
    nadded, code_cache = _test_codes(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP prev. name codes", is_dmd=True,
        pres_str_cache=pres_str_cache, code_cache=code_cache
    )
    total_added += nadded

    return total_added, code_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_occurrences(version, pres_session, dmd_session, pres_str_cache=None,
                    occurs_cache=None, verbose=False):
    """Parse the source occurrences of specific strings into the prescription
    string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        called from the database.
    occurs_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, data_source_id). If not supplied then it is
        called from the database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    occurs_cache : `set` of (`int`, `int`)
        A cache of (pres_str_id, data_source_id). This is updated with any new
        additions.
    """
    total_added = 0
    str_alias = 'pres_str'

    # VMPs have two code columns Now add the VMP codes
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias)
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking VMP name occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    # Abbreviated VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
    ).filter(
        dorm.VirtMedProd.vmp_name_abbrev != None,
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking VMP name abbrev. occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    # Previous VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias)
    ).filter(
        dorm.VirtMedProd.vmp_name_prev != None,
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking VMP prev. name occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    # AMP descriptions
    q = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(str_alias)
    ).filter(
        dorm.ActMedProd.amp_desc != None
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP desc occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    # AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name.label(str_alias)
    ).filter(
        dorm.ActMedProd.amp_name != None
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP name occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    # Previous AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(str_alias)
    ).filter(
        dorm.ActMedProd.amp_name_prev != None
    )
    nadded, occurs_cache = _test_occurrences(
        version, pres_session, q, verbose=verbose,
        msg="[info] checking AMP prev. name occurs",
        pres_str_cache=pres_str_cache, occurs_cache=occurs_cache
    )
    total_added += nadded

    return total_added, occurs_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_external_codes(version, pres_session, dmd_session, pres_str_cache=None,
                       ext_code_cache=None, verbose=False):
    """Parse the prescription string external codes into the prescription
    string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        called from the database.
    ext_code_cache : `set` of (`int`, `int`, `str`, `int`), optional, default:\
    `NoneType`
        A cache of (pres_str_id, external_code_type_id, external_code_id,
        data_source_id). If not supplied then it is queried from the database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    ext_code_cache : `set` of (`int`, `int`, `str`, `int`)
        A cache of (pres_str_id, external_code_type_id, external_code_id,
        data_source_id). This has any new codes added to it.
    """
    total_added = 0
    str_alias = 'pres_str'

    ext_code_map = {
        common.EXT_CODE_BNF: add_external_code_type(
            pres_session, common.EXT_CODE_BNF
        ).external_code_type_id,
        common.EXT_CODE_ATC: add_external_code_type(
            pres_session, common.EXT_CODE_ATC
        ).external_code_type_id
    }

    # VMP names
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking VMP name external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    # Abbreviated VMPs
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_abbrev != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_abbrev != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking abbreviated VMP name external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    # Previous VMPs
    q1 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_prev != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.VirtMedProd.vmp_name_prev != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking previous VMP name external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    # AMP descriptions
    q1 = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_desc != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_desc != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking AMP descriptions external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    # AMP names
    q1 = dmd_session.query(
        dorm.ActMedProd.amp_name.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_name != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.ActMedProd.amp_name.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_name != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking AMP name external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    # Previous AMP names
    q1 = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(str_alias),
        dorm.VirtMedProdMap.bnf.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_BNF).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_name_prev != None,
            dorm.VirtMedProdMap.bnf != None
        )
    )
    q2 = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(str_alias),
        dorm.VirtMedProdMap.atc.label(ALIAS_EXT_CODE_ID),
        literal(common.EXT_CODE_ATC).label(ALIAS_EXT_CODE_TYPE),
    ).join(
        dorm.VirtMedProd,
        dorm.ActMedProd.vmp_id == dorm.VirtMedProd.vmp_id
    ).join(
        dorm.VirtMedProdMap,
        dorm.VirtMedProd.vmp_id == dorm.VirtMedProdMap.vmp_id
    ).filter(
        and_(
            dorm.ActMedProd.amp_name_prev != None,
            dorm.VirtMedProdMap.atc != None,
            dorm.VirtMedProdMap.atc != "n/a"
        )
    )
    nadded, occurs_cache = _test_ext_codes(
        version, pres_session, q1.union(q2), ext_code_map, verbose=verbose,
        msg="[info] checking previous AMP name external codes",
        pres_str_cache=pres_str_cache, ext_code_cache=ext_code_cache
    )
    total_added += nadded

    return total_added, ext_code_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_prescription_link(pres_session, dmd_session, verbose=False,
                          pres_str_cache=None, pres_link_cache=None):
    """Parse VMPs into the prescription string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    dmd_session : `sqlalchemy.Session`
        A session object bound to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        called from the database.
    pres_link_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, vmp_id). If not supplied then it is queried
        from the database.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    pres_link_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, vmp_id). This has any new entries added.
    """
    total_added = 0
    str_alias = 'pres_str'

    # VMP names
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name.label(str_alias),
        dorm.VirtMedProd.vmp_id
    ).filter(dorm.VirtMedProd.vmp_name != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking VMP name prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    # Abbreviated VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_abbrev.label(str_alias),
        dorm.VirtMedProd.vmp_id
    ).filter(dorm.VirtMedProd.vmp_name_abbrev != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking abbreviated VMP name prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    # Previous VMPs
    q = dmd_session.query(
        dorm.VirtMedProd.vmp_name_prev.label(str_alias),
        dorm.VirtMedProd.vmp_id
    ).filter(dorm.VirtMedProd.vmp_name_prev != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking previous VMP name prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    # AMP descriptions
    q = dmd_session.query(
        dorm.ActMedProd.amp_desc.label(str_alias),
        dorm.ActMedProd.vmp_id
    ).filter(dorm.ActMedProd.amp_desc != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking AMP descriptions prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    # AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name.label(str_alias),
        dorm.ActMedProd.vmp_id
    ).filter(dorm.ActMedProd.amp_name != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking AMP name prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    # Previous AMP names
    q = dmd_session.query(
        dorm.ActMedProd.amp_name_prev.label(str_alias),
        dorm.ActMedProd.vmp_id
    ).filter(dorm.ActMedProd.amp_name_prev != None)
    nadded, pres_link_cache = _test_prescription_link(
        pres_session, q, verbose=verbose,
        msg="[info] checking previous AMP name prescription links",
        pres_str_cache=pres_str_cache, pres_link_cache=pres_link_cache
    )
    total_added += nadded

    return total_added, pres_link_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_strings(version, pres_session, q,  verbose=False, is_dmd=False,
                  is_amp=False, msg="[info] checking strings",
                  pres_str_cache=None):
    """Parse prescription strings into the prescription string database.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `sqlalchemy.Query`
        A query to extract the strings. The string column must be available as
        an alias ``pres_str``.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    is_dmd : `bool`, optional, default: `False`
        Is the prescription string derived from a DM+D source.
    is_amp : `bool`, optional, default: `False`
        Is the prescription string derived from an actual medicinal product.
    msg : `str`, optional, default: `[info] checking strings`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    pres_str_cache : `dict`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). This is updated for new
        additions after calling this function.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    if pres_str_cache is None:
        pres_str_cache = cache_strings(pres_session)

    logger.info(f"the string cache contains #string: {len(pres_str_cache)}")

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        string = row.pres_str

        if string in MISSING:
            continue

        str_hash = hashstr(row.pres_str)
        if str_hash not in pres_str_cache:
            is_truncated = False
            if re.search(r'...$', row.pres_str):
                is_truncated = True

            str_entry = porm.PresString(
                pres_str_name=string,
                is_amp=is_amp,
                is_dmd=is_dmd,
                is_truncated=is_truncated,
                source_version=version
            )
            # session.add(str_entry)
            row_buffer.append(str_entry)
            pres_str_cache[str_hash] = None
            nadded += 1
            idx += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            for i in row_buffer:
                pres_str_cache[hashstr(i.pres_str_name)] = i.pres_str_id
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        for i in row_buffer:
            pres_str_cache[hashstr(i.pres_str_name)] = i.pres_str_id
        row_buffer = []
        idx = 0
    return nadded, pres_str_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_codes(version, pres_session, q, verbose=False,
                msg="[info] checking codes", is_dmd=False,
                pres_str_cache=None, code_cache=None):
    """Query DMD codes and add them to the database of they are not in the
    cache.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `sqlalchemy.Query`
        A query to extract the codes. The string column must be available as
        an alias ``pres_str`` and the code column ``code``.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    is_dmd : `bool`, optional, default: `False`
        Is the prescription string code derived from the DM+D.
    msg : `str`, optional, default: `[info] checking codes`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    code_cache : `set` of (`int`, `int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, pres_str_code (dmd code), data_source_id). If
        not supplied then it is called from the database.

    Returns
    -------
    nadded : `int`
        The number of new VMPs that have been added from the DM+D source
        database.
    code_cache : `set` of (`int`, `int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, pres_str_code (dmd code), data_source_id).
        This is updated with new codes that have been added.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()
    try:
        int(total)
    except Exception:
        total = None

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    data_source_id = version.data_source_id

    if pres_str_cache is None:
        pres_str_cache = cache_strings(pres_session)
    if code_cache is None:
        code_cache = cache_codes(pres_session, data_source_id=data_source_id)

    logger.info(f"the code cache contains #code/versions: {len(code_cache)}")

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        if row.pres_str in MISSING or row.code in MISSING:
            continue

        str_id = pres_str_cache[hashstr(row.pres_str)]
        code_id = row.code
        key = (str_id, code_id, data_source_id)
        if key not in code_cache:
            code_parse = scommon.SnomedCodeParser.parse(code_id)
            code_entry = porm.PresStringCode(
                pres_str_code=code_id,
                pres_str_id=str_id,
                source_version=version,
                is_dmd=is_dmd,
                is_verhoeff_valid=code_parse.verhoeff_valid,
                check_digit=code_parse.check_digit,
                partition_id=code_parse.partition_id,
                has_namespace=code_parse.has_namespace,
                code_type=code_parse.code_type,
                is_partition_valid=code_parse.partition_valid,
                namespace_id=code_parse.namespace_id,
                is_namespace_valid=code_parse.namespace_valid,
                organisation=code_parse.organisation
            )
            row_buffer.append(code_entry)
            code_cache.add(key)
            nadded += 1
            idx += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
        idx = 0
    return nadded, code_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_occurrences(version, pres_session, q, verbose=False,
                      msg="[info] checking occurrences",
                      pres_str_cache=None, occurs_cache=None):
    """Query DMD strings and add their presence to the occurrences table if
    they are not in the cache.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `sqlalchemy.Query`
        A query to extract the codes. The string column must be available as
        an alias ``pres_str``.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    msg : `str`, optional, default: `[info] checking occurrences`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    occurs_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, data_source_id). If not supplied then it is
        called from the database.

    Returns
    -------
    nadded : `int`
        The number of new occurrences that have been added from the DM+D source
        database.
    occurs_cache : `set` of (`int`, `int`)
        A cache of (pres_str_id, data_source_id). This is updated with any new
        additions.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    data_source_id = version.data_source_id

    if pres_str_cache is None:
        pres_str_cache = cache_strings(pres_session)
    if occurs_cache is None:
        occurs_cache = cache_occurrence(
            pres_session, data_source_id=data_source_id
        )

    logger.info(
        f"the occurrence cache contains #code/versions: {len(occurs_cache)}"
    )

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        if row.pres_str in MISSING:
            continue

        str_id = pres_str_cache[hashstr(row.pres_str)]
        key = (str_id, data_source_id)
        if key not in occurs_cache:
            occurs_entry = porm.PresStringOccurs(
                pres_str_id=str_id,
                source_version=version,
            )
            row_buffer.append(occurs_entry)
            occurs_cache.add(key)
            nadded += 1
            idx += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
        idx = 0
    return nadded, occurs_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_ext_codes(version, pres_session, q, ext_code_map, verbose=False,
                    msg="[info] checking external codes",
                    pres_str_cache=None, ext_code_cache=None):
    """Query external codes for the DMD strings and add them to the external
    codes table if they are not present in the cache.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `sqlalchemy.Query`
        A query to extract the codes. The string column must be available as
        an alias ``pres_str`` and the external code column must be
        ``ext_code_id``.
    ext_code_map : `dict`
        Mappings between external code names and their IDs.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    msg : `str`, optional, default: `[info] checking external codes`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    ext_code_cache : `set` of (`int`, `int`, `str`, `int`), optional, default:\
    `NoneType`
        A cache of (pres_str_id, external_code_type_id, external_code_id,
        data_source_id). If not supplied then it is queried from the database.

    Returns
    -------
    nadded : `int`
        The number of new occurrences that have been added from the DM+D source
        database.
    ext_code_cache : `set` of (`int`, `int`, `str`, `int`)
        A cache of (pres_str_id, external_code_type_id, external_code_id,
        data_source_id). This has any new codes added to it.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    data_source_id = version.data_source_id

    if pres_str_cache is None:
        pres_str_cache = cache_strings(pres_session)
    if ext_code_cache is None:
        ext_code_cache = cache_external_codes(
            pres_session, data_source_id=data_source_id
        )

    logger.info(
        f"the external code cache contains #codes: {len(ext_code_cache)}"
    )

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        if row.pres_str in MISSING or row.ext_code_id in MISSING:
            continue

        str_id = pres_str_cache[hashstr(row.pres_str)]
        ext_code_id = str(row.ext_code_id)
        ext_code_type_id = ext_code_map[row.ext_code_type]
        key = (
            str_id, ext_code_type_id,
            ext_code_id, data_source_id
        )
        if key not in ext_code_cache:
            ext_code_entry = porm.DmdExternalCode(
                pres_str_id=str_id,
                source_version=version,
                external_code_type_id=ext_code_type_id,
                code_id=ext_code_id
            )
            row_buffer.append(ext_code_entry)
            ext_code_cache.add(key)
            nadded += 1
            idx += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
        idx = 0
    return nadded, ext_code_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_prescription_link(pres_session, q, verbose=False,
                            msg="[info] checking prescription link",
                            pres_str_cache=None, pres_link_cache=None):
    """Test is a link has been created between the prescription string ID and
    the prescribing VMP ID and add a link if not.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `sqlalchemy.Query`
        A query to extract the codes. The string column must be available as
        an alias ``pres_str``.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    msg : `str`, optional, default: `[info] checking prescription link`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    pres_link_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, vmp_id). If not supplied then it is queried
        from the database.

    Returns
    -------
    nadded : `int`
        The number of new occurrences that have been added from the DM+D source
        database.
    pres_link_cache : `set` of (`int`, `int`), optional, default: `NoneType`
        A cache of (pres_str_id, vmp_id). This has any new entries added.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    if pres_str_cache is None:
        pres_str_cache = cache_strings(pres_session)
    if pres_link_cache is None:
        pres_link_cache = cache_pres_link(pres_session)

    logger.info(
        f"the prescription link cache contains #codes: {len(pres_link_cache)}"
    )

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        str_id = pres_str_cache[hashstr(row.pres_str)]
        key = (str_id, row.vmp_id)
        if key not in pres_link_cache:
            ext_code_entry = porm.PresStringDmd(
                pres_str_id=str_id,
                vmp_id=row.vmp_id
            )
            row_buffer.append(ext_code_entry)
            pres_link_cache.add(key)
            nadded += 1
            idx += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
        idx = 0
    return nadded, pres_link_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_prescription(session, vmp_row):
    """Generic function to add prescription strings to the database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    vmp_row : `dmd.orm.VirtMedProd`
        The virtual medicinal product row that is associated with the string.

    Returns
    -------
    added : `bool`
        Was the prescription string added to the database.
    """
    # Add the dose form entry
    dose_form_entry = add_dose_form(session, vmp_row)

    # Now add the prescription information if it is not there
    comb_prod_entry = add_comb_prod(session, vmp_row)

    # Make sure the units are in place
    dose_form_unit = vmp_row.unit_meas
    dose_form_unit_entry = add_unit(session, dose_form_unit)
    dose_form_unit_dose = vmp_row.unit_meas_ud
    dose_form_unit_dose_entry = add_unit(session, dose_form_unit_dose)

    try:
        # Ensure the VTM is in place
        vtm_entry = add_vtm(session, vmp_row.virt_ther_moiet)

        # This will raise the AttributeError if there is no VTM
        vtms = _split_vtm(vmp_row.virt_ther_moiet.vtm_name)
        nvtms = len(vtms)
    except AttributeError:
        if vmp_row.virt_ther_moiet is not None:
            raise
        nvtms = 0

    # Finally add the prescription
    pres_entry = porm.DmdPrescription(
        dose_form_size=vmp_row.unit_dose_form_size,
        vmp_id=vmp_row.vmp_id,
        comb_prod=comb_prod_entry,
        dose_form=dose_form_entry,
        dose_form_unit_meas=dose_form_unit_entry,
        unit_dose_unit_meas=dose_form_unit_dose_entry,
        dmd_pres_moiety=vtm_entry,
        n_vtms=nvtms
    )
    session.add(pres_entry)

    for i in vmp_row.virt_prod_ing:
        num_unit_entry = add_unit(session, i.stren_value_num_unit_meas)
        den_unit_entry = add_unit(session, i.stren_value_den_unit_meas)
        ingredient_entry = add_ingredient(session, i.ingredient)

        pres_ing_entry = porm.DmdPresIngredient(
            dmd_pres=pres_entry,
            ingredient=ingredient_entry,
            stren_value_num_unit_meas=num_unit_entry,
            stren_value_den_unit_meas=den_unit_entry,
            stren_value_num=i.stren_value_num,
            stren_value_den=i.stren_value_den
        )
        session.add(pres_ing_entry)

    for i in vmp_row.drug_form:
        form_entry = add_form(session, i.form)
        pres_form_entry = porm.DmdPresForm(
            form=form_entry,
            dmd_pres=pres_entry
        )
        session.add(pres_form_entry)

    for i in vmp_row.drug_route:
        route_entry = add_route(session, i.route)
        pres_route_entry = porm.DmdPresRoute(
            route=route_entry,
            dmd_pres=pres_entry
        )
        session.add(pres_route_entry)

    add_ddd(session, vmp_row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_ddd(session, row):
    """Make sure any defined daily dosages are in the database and associated
    with prescriptions.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    vmp_row : `dmd.orm.VirtMedProd`
        The virtual medicinal product row that is associated with the string.
    """
    for m in row.virt_med_prod_map:
        # print(m)
        if m.ddd is None:
            continue

        unit_entry = add_unit(
            session, m.unit_meas
        )

        session.add(
            porm.DmdDefinedDailyDose(
                vmp_id=row.vmp_id,
                ddd=m.ddd,
                ddd_units=unit_entry
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_external_code_type(session, code_type_name):
    """Add the external code type.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    code_type_name : `str`
        The name for the external code type.

    Returns
    -------
    code_type_entry : `dmd.pres_db.orm.ExternalCodeTypeLkp`
        The code type object.
    """
    try:
        return session.query(
            porm.ExternalCodeTypeLkp
        ).filter(
            porm.ExternalCodeTypeLkp.external_code_name == code_type_name,
        ).one()
    except NoResultFound:
        entry = porm.ExternalCodeTypeLkp(
            external_code_name=code_type_name,
        )
        session.add(entry)
        session.commit()
        return entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _split_vtm(text):
    """Split a virtual therapeutic moiety string into in's components. The
    components are separated with a <space>+<space>.

    Parameters
    ----------
    text : `str`
        The VTM string to split.

    Returns
    -------
    vtms : `list` of `str`
        One or more individual VTM strings.
    """
    return re.split(r'\s\+\s', text)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_vtm(session, row):
    """Add a virtual therapeutic moiety string to the VTM table if it is not
    already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    row : `dmd.orm.VirtTherMoiet`
        A DM+D virtual therapeutic moiety object.

    Returns
    -------
    vtm_row : `dmd.pres_db.orm.DmdPresTherMoiety`
        An virtual therapeutic moiety row that has either been added to the
        database or retrieved from it.

    Notes
    -----
    The presence of existing rows is determined by a vtm_id with
    existing VTMs. The name is not checked as it may vary between
    different DM+D versions. This should not matter too much as the IDs
    are SnomedIDs so should be stable.
    """
    if row is None:
        return row

    try:
        return session.query(
            porm.DmdPresTherMoiety
        ).filter(
            porm.DmdPresTherMoiety.vtm_id == row.vtm_id,
        ).one()
    except NoResultFound:
        vtm_entry = porm.DmdPresTherMoiety(
            vtm_name=row.vtm_name,
            vtm_id=row.vtm_id,
            vtm_abbrev=row.vtm_abbrev,
            vtm_id_date=row.vtm_id_date,
            vtm_id_prev=row.vtm_id_prev,
            vtm_invalid=bool(row.vtm_invalid)
        )
        session.add(vtm_entry)
        return vtm_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_form(session, row):
    """Add a prescription form to the dmd forms lookup table table if it is not
    already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    row : `dmd.orm.LkpForm`
        A DM+D form row object.

    Returns
    -------
    form_row : `dmd.pres_db.orm.DmdFormLkp`
        A form row that has either been added to the database or retrieved from
        it.

    Notes
    -----
    The presence of existing rows is determined by a form description match.
    """
    try:
        return session.query(
            porm.DmdFormLkp
        ).filter(
            and_(
                porm.DmdFormLkp.form_id == row.form_cd
            )
        ).one()
    except NoResultFound:
        form_entry = porm.DmdFormLkp(
            form_desc=row.form_desc,
            form_id=row.form_cd,
            form_id_date=row.form_cd_date,
            form_id_prev=row.form_cd_prev,
        )
        session.add(form_entry)
        return form_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_route(session, row):
    """Add a prescription ROA to the dmd route lookup table table if it is not
    already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    row : `dmd.orm.LkpRoute`
        A DM+D route row object.

    Returns
    -------
    route_row : `dmd.pres_db.orm.DmdRouteLkp`
        A ROA row that has either been added to the database or retrieved from
        it.

    Notes
    -----
    The presence of existing rows is determined by a route description match.
    """
    try:
        return session.query(
            porm.DmdRouteLkp
        ).filter(
            and_(
                porm.DmdRouteLkp.route_id == row.route_cd
            )
        ).one()
    except NoResultFound:
        route_entry = porm.DmdRouteLkp(
            route_desc=row.route_desc,
            route_id=row.route_cd,
            route_id_date=row.route_cd_date,
            route_id_prev=row.route_cd_prev,
        )
        session.add(route_entry)
        return route_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_ingredient(session, row):
    """Add ingredient lookups if not already present in the database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    row : `dmd.orm.Ingredient`
        A DM+D ingredient object.

    Returns
    -------
    ingredient_row : `dmd.pres_db.orm.DmdIngredientLkp`
        An ingredient row that has either been added to the database or
        retrieved from it.

    Notes
    -----
    The presence of existing rows is determined by an ingredient_id with
    existing ingredients. The name is not checked as it may vary between
    different DM+D versions. This should not matter too much as the IDs
    are SnomedIDs so should be stable.
    """
    if row is None:
        return row

    try:
        return session.query(
            porm.DmdIngredientLkp
        ).filter(
            porm.DmdIngredientLkp.ingredient_id == row.ingredient_id
        ).one()
    except NoResultFound:
        ingredient_entry = porm.DmdIngredientLkp(
            ingredient_name=row.ingredient_name,
            ingredient_id=row.ingredient_id,
            ingredient_id_date=row.ingredient_id_date,
            ingredient_id_prev=row.ingredient_id_prev,
            ingredient_invalid=bool(row.ingredient_invalid)
        )
        session.add(ingredient_entry)
        return ingredient_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_unit(session, unit_obj):
    """Add a unit of measure to the dmd units lookup table if it is not
    already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    unit_obj : `dmd.orm.LkpUnitMeasure` or `NoneType`
        A DM+D unit measure row object.

    Returns
    -------
    unit_row : `dmd.pres_db.orm.DmdUnitMeasureLkp`
        A unit of measure row that has either been added to the database or
        retrieved from it. If `unit_obj` not defined hen `NoneType` is
        returned.

    Notes
    -----
    The presence of existing rows is determined by a unit of measure
    description ID match. Matching on descriptions can cause issues when the
    description is updated but refers to the same ID.
    """
    if unit_obj is None:
        return unit_obj

    try:
        return session.query(
            porm.DmdUnitMeasureLkp
        ).filter(
            porm.DmdUnitMeasureLkp.unit_meas_id == unit_obj.unit_meas_cd
        ).one()
    except NoResultFound:
        unit_entry = porm.DmdUnitMeasureLkp(
            unit_meas_id=unit_obj.unit_meas_cd,
            unit_meas_id_date=unit_obj.unit_meas_cd_date,
            unit_meas_id_prev=unit_obj.unit_meas_cd_prev,
            unit_meas_desc=unit_obj.unit_meas_desc
        )
        session.add(unit_entry)
        # print("Adding units...")
        # print(unit_entry)
        return unit_entry
    except IntegrityError:
        print(unit_obj)
        raise


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_dose_form(session, vmp_row):
    """Add a prescription dose for indicator to the dose form table if it is
    not already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    vmp_row : `dmd.orm.VirtMedProd`
        A DM+D virtual medicinal product row object.

    Returns
    -------
    dose_form_row : `dmd.pres_db.orm.DmdDoseFormLkp`
        A dose form indicator row that has either been added to the database or
        retrieved from it. If the vmp does not have a dose form defined, then a
        custom "none" value is added. Note these are not present in the DM+D
        but are useful to have.

    Notes
    -----
    The presence of existing rows is determined by a dose form description
    match.
    """
    try:
        dose_form = vmp_row.dose_form_ind.dose_form_ind_desc
    except AttributeError:
        dose_form = common.NO_VALUE

    try:
        return session.query(
            porm.DmdDoseFormLkp
        ).filter(
            porm.DmdDoseFormLkp.dose_form_desc == dose_form
        ).one()
    except NoResultFound:
        dose_form_entry = porm.DmdDoseFormLkp(
            dose_form_desc=dose_form
        )
        session.add(dose_form_entry)
        return dose_form_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_comb_prod(session, vmp_row):
    """Add a prescription combination product indicator to the combination
    product lookup table if it is not already present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    vmp_row : `dmd.orm.VirtMedProd`
        A DM+D virtual medicinal product row object.

    Returns
    -------
    dose_form_row : `dmd.pres_db.orm.DmdCombProdLkp`
        A combination product indicator row that has either been added to the
        database or retrieved from it. If the vmp does not have a combination
        product indicator defined, then a custom "none" value is added. Note
        these are not present in the DM+D but are useful to have.

    Notes
    -----
    The presence of existing rows is determined by a combination product
    description match. Also, note that AMPs will end up with the VMP
    combination product indicator due to the way the code is structured.
    """
    try:
        comb_prod = vmp_row.comb_prod_ind.comb_prod_ind_desc
    except AttributeError:
        comb_prod = common.NO_VALUE

    try:
        return session.query(
            porm.DmdCombProdLkp
        ).filter(
            porm.DmdCombProdLkp.comb_prod_desc == comb_prod
        ).one()
    except NoResultFound:
        comb_prod_entry = porm.DmdCombProdLkp(
            comb_prod_desc=comb_prod
        )
        session.add(comb_prod_entry)
        return comb_prod_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_source(pres_session, dmd_session):
    """Add the DM+D source to the prescription string sources and version
    table.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.

    Returns
    -------
    version : `dmd.pres_db.orm.SourceVersion`
        The source version for the DM+D being added.
    """
    try:
        # First make sure the DM+D is registered as a version in the database
        data_source = pres_session.query(
            porm.DataSource
        ).filter(
            porm.DataSource.data_source_name == common.DMD_DATA_SOURCE
        ).one()
    except NoResultFound:
        # No, so add it
        data_source = porm.DataSource(
            data_source_name=common.DMD_DATA_SOURCE,
            data_source_pretty=common.DMD_PRETTY_NAME
        )
        pres_session.add(data_source)
        pres_session.commit()

    # Now build the DMD version string
    dmd_version = dmd_session.query(
        dorm.FileInfo
    ).filter(
        dorm.FileInfo.file_type == 'zip'
    ).one()

    # Add the year to the month version
    dmd_version = f"{dmd_version.file_date.year}.{dmd_version.file_version}"

    try:
        # See if it already exists, if so then return for use
        version = pres_session.query(
            porm.SourceVersion
        ).filter(
            and_(
                porm.SourceVersion.data_source_id ==
                data_source.data_source_id,
                porm.SourceVersion.version_name == dmd_version
            )
        ).one()
        return version
    except NoResultFound:
        # No, so add and return
        version = porm.SourceVersion(
            data_source_id=data_source.data_source_id,
            version_name=dmd_version
        )
        pres_session.add(version)
        pres_session.commit()
        return version


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_dict_suppliers(pres_session, dmd_session, verbose=False):
    """Update the suppliers dictionary with the current DM+D.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.

    Returns
    -------
    nadded : `int`
        The number of suppliers that have been added from the DM+D database.
    """
    suppliers_added = 0
    q = dmd_session.query(dorm.LkpSupplier)
    total = q.count()

    tqdm_kwargs = dict(
        desc="[info] updating supplier dict",
        unit=" suppliers",
        leave=False,
        total=total,
        disable=not verbose
    )

    # Get the supplier sub-class ID to use in any updates
    sc_row = pres_session.query(porm.SubClassLkp).\
        join(porm.ClassLkp).\
        filter(
            and_(
                porm.SubClassLkp.sub_class_name == common.NO_VALUE,
                porm.ClassLkp.class_name == common.CLASS_SUPPLIER
            )
        ).one()
    scid = sc_row.sub_class_id

    for row in tqdm(q, **tqdm_kwargs):
        sq = pres_session.query(porm.Dict).\
            join(porm.SubClassLkp).\
            join(porm.ClassLkp).\
            filter(
                and_(
                    porm.ClassLkp.class_name == common.CLASS_SUPPLIER,
                    porm.Dict.name == row.supplier_desc
                )
            )
        n = sq.count()
        if n == 0:
            suppliers_added += 1
            pres_session.add(
                porm.Dict(
                    name=row.supplier_desc,
                    sub_class_id=scid,
                    is_regexp=False
                )
            )
    pres_session.commit()
    return suppliers_added


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_dict_drugs(pres_session, dmd_session, verbose=False):
    """Update the drugs dictionary with the current DM+D.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.

    Returns
    -------
    nadded : `int`
        The number of drugs that have been added from the DM+D database.

    Notes
    -----
    Drugs are added indirectly as anything that has an entry in the therapeutic
    moiety table. Both the therapeutic moieties are added as well as the
    ingredients that mat them. Therapeutic moieties are split first.
    """
    nadded = 0

    tqdm_kwargs = dict(
        desc="[info] updating drug dict",
        unit=" drugs",
        leave=False,
        total=dmd_drug_query(dmd_session).count(),
        disable=not verbose
    )

    # Get the drug sub-class ID to use in any updates, this is class drug with
    # unknown drug sub-class
    sc_row = pres_session.query(porm.SubClassLkp).\
        join(porm.ClassLkp).\
        filter(
            and_(
                porm.SubClassLkp.sub_class_name ==
                common.SUB_CLASS_UNKNOWN_DRUG,
                porm.ClassLkp.class_name == common.CLASS_DRUG
            )
        ).one()
    addid = sc_row.sub_class_id

    for row in tqdm(dmd_drug_query(dmd_session), **tqdm_kwargs):
        vtms = _split_vtm(row.vtm_name)
        for i in vtms + [row.ingredient_name]:
            tq = pres_session.query(porm.Dict).\
                join(porm.SubClassLkp).\
                join(porm.ClassLkp).\
                filter(
                    and_(
                        porm.ClassLkp.class_name == common.CLASS_DRUG,
                        porm.Dict.name.like(i)
                    )
                )
            n = tq.count()
            if n == 0:
                nadded += 1
                pres_session.add(
                    porm.Dict(
                        name=i,
                        sub_class_id=addid,
                        is_regexp=False
                    )
                )
    pres_session.commit()
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_dict_salts(pres_session, dmd_session, verbose=False):
    """Update the drugs-salt dictionary with the current DM+D. Note, this is
    not called anymore as there are lots of false positive salts.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.

    Returns
    -------
    nadded : `int`
        The number of salts that have been added from the DM+D database.

    Notes
    -----
    Salts are found indirectly by getting drug names, which are also ID'd
    indirectly (see `update_dict_drugs`) then removing the therapeutic moiety
    from the drug (ingredient) name, what is left is assumed to be a salt.
    """
    nadded = 0

    tqdm_kwargs = dict(
        desc="[info] updating salt dict",
        unit=" salts",
        leave=False,
        total=dmd_drug_query(dmd_session).count(),
        disable=not verbose
    )

    # Get the drug sub-class ID to use in any updates, this is class drug with
    # unknown drug sub-class
    sc_row = pres_session.query(porm.SubClassLkp).\
        join(porm.ClassLkp).\
        filter(
            and_(
                porm.SubClassLkp.sub_class_name ==
                common.SUB_CLASS_DRUG_SALT,
                porm.ClassLkp.class_name == common.CLASS_DRUG
            )
        ).one()
    addid = sc_row.sub_class_id
    print(addid)
    all_salts = set()
    for row in tqdm(dmd_drug_query(dmd_session), **tqdm_kwargs):
        vtms = _split_vtm(row.vtm_name)
        # print(vtms, row.ingredient_name)
        for i in vtms:
            match_name = re.escape(i)
            if re.match(r'^{0}'.format(match_name), row.ingredient_name):
                salt = re.sub(
                    r'^{0}'.format(match_name),
                    '',
                    row.ingredient_name).strip()
                if len(salt) > 0:
                    # print(row.ingredient_name, ":", salt)
                    all_salts.add(salt)
    for i in sorted(all_salts):
        print(i)
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_dict_units(pres_session, dmd_session, verbose=False):
    """Update the units dictionary with the current DM+D units.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    dmd_session : `sqlalchemy.Session`
        The session object attached to the DM+D database.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.

    Returns
    -------
    nadded : `int`
        The number of units that have been added from the DM+D database.
    """
    nadded = 0

    q = dmd_session.query(dorm.LkpUnitMeasure.unit_meas_desc)

    tqdm_kwargs = dict(
        desc="[info] updating units dict",
        unit=" units",
        leave=False,
        total=q.count(),
        disable=not verbose
    )

    # Get the drug sub-class ID to use in any updates, this is class drug with
    # unknown drug sub-class
    sc_row = pres_session.query(porm.SubClassLkp).\
        join(porm.ClassLkp).\
        filter(
            and_(
                porm.SubClassLkp.sub_class_name ==
                common.SUB_CLASS_NO_REVERSE,
                porm.ClassLkp.class_name == common.CLASS_UNIT
            )
        ).one()
    addid = sc_row.sub_class_id
    for row in tqdm(q, **tqdm_kwargs):
        tq = pres_session.query(porm.Dict).\
            join(porm.SubClassLkp).\
            join(porm.ClassLkp).\
            filter(
                and_(
                    porm.ClassLkp.class_name == common.CLASS_UNIT,
                    porm.Dict.name == row.unit_meas_desc
                )
            )
        n = tq.count()
        if n == 0:
            nadded += 1
            pres_session.add(
                porm.Dict(
                    name=row.unit_meas_desc,
                    sub_class_id=addid,
                    is_regexp=False
                )
            )
    pres_session.commit()
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dmd_drug_query(session):
    """Get an SQLAlchemy query to ID drugs in the DM+D.

    Returns
    -------
    query : `sqlalchemy.Query`
        A query object to derive the drug information.
    """
    q = session.query(
        dorm.VirtTherMoiet.vtm_name,
        dorm.Ingredient.ingredient_name
    ).join(
        dorm.VirtMedProd,
        dorm.VirtMedProd.vtm_id == dorm.VirtTherMoiet.vtm_id
    ).join(
        dorm.VirtProdIng,
        dorm.VirtMedProd.vmp_id == dorm.VirtProdIng.vmp_id
    ).join(
        dorm.Ingredient,
        dorm.VirtProdIng.vmp_ingredient_id == dorm.Ingredient.ingredient_id
    ).group_by(
        dorm.VirtTherMoiet.vtm_name,
        dorm.Ingredient.ingredient_name
    )
    return q


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def entry_exists(session, entry, entry_sub_class):
    """Test to see if the token exists in the token table. This is
    case-sensitive depending on the database distribution.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    token : `str`
        The entry name/regexp
    token_sub_class : `str`
        The sub-class of the entry.

    Returns
    -------
    entry_exists : `bool`
        True if the entry exists, False if not.
    """
    q = session.query(porm.Dict).\
        filter(
            and_(
                porm.Dict.name == entry,
                porm.SubClassLkp.sub_class_name == entry_sub_class
            )
        )
    try:
        q.one()
    except NoResultFound:
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_entry(session, entry_name, entry_class, entry_sub_class, roa=None,
              base_type=None, base_form=None, is_regex=False,
              standard_entry=None, roa_map=None, bf_map=None, bt_map=None,
              sc_map=None):
    """Add an dictionary entry to the dictionary table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    entry_name : `str`
        The dictionary entry name/regexp
    entry_class : `str`
        The dictionary entry class.
    entry_sub_class : `str`
        The sub-class of the dictionary entry.
    roa : `str`, optional, default: `NoneType`
        Any route of administration hint for the dictionary entry. If more than
        one they should be | separated strings.
    base_type : `str`, optional, default: `NoneType`
        Any base type hint for the dictionary entry. If more than one they
        should be | separated strings.
    base_form : `str`, optional, default: `NoneType`
        Any base form hint for the dictionary entry. If more than one they
        should be | separated strings.
    is_regex : `bool`, optional, default: `NoneType`
        Is the entry_name a regular expression.
    standard_entry : `str`, optional, default: `NoneType`
        A standardised version of the entry_name.
    roa_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, route of administration names (keys),
        their IDs in the lookup table. If not supplied then there are queried
        from the database.
    bf_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, base form names names (keys), their IDs
        in the lookup table. If not supplied then there are queried from the
        database.
    bt_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, base type names names (keys), their IDs
        in the lookup table. If not supplied then there are queried from the
        database.
    sc_map : `dict`, optional, default: `NoneType`
        Pre-queried mappings between, dict-class, subclass tuples (keys), the
        dict sub-class IDs in the lookup table. If not supplied then there are
        queried from the database.
    """
    if base_form in MISSING:
        base_form = 'unknown'
    if base_type in MISSING:
        base_type = 'unknown'
    if roa in MISSING:
        roa = 'unknown'
    if standard_entry in MISSING:
        standard_entry = None

    entry_sub_class_id = None
    entry_base_form_id = []
    entry_base_type_id = []
    entry_roa_id = []
    try:
        if sc_map is None:
            q = session.query(porm.SubClassLkp).\
                filter(
                    and_(
                        porm.SubClassLkp.sub_class_name ==
                        entry_sub_class,
                        porm.ClassLkp.class_name == entry_class
                    )
                )
            res = q.one()
            entry_sub_class_id = res.token_sub_class_id
        else:
            entry_sub_class_id = \
                sc_map[(entry_class, entry_sub_class)]
    except (KeyError, NoResultFound) as e:
        raise KeyError(
            "unknown dictionary entry class/sub-class: "
            f"{entry_class}/{entry_sub_class}"
        ) from e

    # Get the IDs for the base forms of the entry being added
    entry_base_form_id = _get_ids(
        session, base_form, porm.BaseFormLkp,
        'base_form_name', 'base_form_id',
        map=bf_map,
        type="base_form"
    )
    # Get the IDs for the base types of the entry being added
    entry_base_type_id = _get_ids(
        session, base_type, porm.BaseTypeLkp,
        'base_type_name', 'base_type_id',
        map=bt_map,
        type="base_type"
    )
    # Get the IDs for the route of administration of the entry being added
    entry_roa_id = _get_ids(
        session, roa, porm.RouteAdminLkp,
        'route_admin_name', 'route_admin_id',
        map=roa_map,
        type="ROA"
    )

    # Now add the tokens and types, forms, ROA
    entry = porm.Dict(
        name=entry_name.strip(),
        sub_class_id=entry_sub_class_id,
        is_regexp=is_regex,
        standard_name=standard_entry,
        is_checked=False,
        is_auto_checked=False,
    )
    session.add(entry)
    for i in entry_base_form_id:
        session.add(porm.DictBaseForm(
            dict_link=entry,
            base_form_id=i
        ))
    for i in entry_base_type_id:
        session.add(porm.DictBaseType(
            dict_link=entry,
            base_type_id=i
        ))
    for i in entry_roa_id:
        session.add(porm.DictRouteAdmin(
            dict_link=entry,
            route_admin_id=i
        ))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_ids(session, search, orm_class, name_field, id_field, map=None,
             type="entity"):
    """Helper class to generalise the retrieval if IDs for different names
    where a mapping dictionary is optionally available.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object to issue queries and add new rows to.
    search : `str`
        The name to search for. This can be a pipe-separated string of multiple
        names. Which is processed into a list.
    orm_class : `str`
        The class of the table being queries.
    name_field : `str`
        The name of the field containing the names in the lookup table.
    id_field : `str`
        The name of the field containing the IDs in the lookup table.
    map : `dict`, optional, default: `NoneType`
        An optional mapping dictionary to use, mapping between names->IDs.
    type : `str`, optional, default: `entity`
        An optional string to use in any KeyError messages, if the IDs can't be
        found.

    Returns
    -------
    ids : `list` of `int`
       The IDs that map to the names in `search`.

    Raises
    ------
    KeyError
        If the IDs can't be retrieved for the search names.
    """
    names = [i.strip() for i in search.split("|")]
    ids = []
    for i in names:
        try:
            if map is not None:
                ids.append(map[i])
            else:
                q = session.query(orm_class).\
                    filter(getattr(orm_class, name_field) == i)
                res = q.one()
                ids.append(getattr(res, id_field))
        except (KeyError, NoResultFound) as e:
            raise KeyError(f"unknown, {type}: {i}") from e
    return ids


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
