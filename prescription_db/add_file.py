"""Load a file of prescription strings from a non-DM+D source into the
 prescription string database. These may be summies of prescription string
 occurrences in IPD datasets.
"""
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    common as common,
    orm as porm,
    add_dmd,
)
from sqlalchemy_config import config as cfg
from pyaddons import log, utils
from pyaddons.flat_files import header
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import and_
from tqdm import tqdm
from collections import namedtuple
import argparse
import csv
import re
import os
import sys
# import pprint as pp


_PROG_NAME = 'pres-db-file'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""
DELIMITER = "\t"
"""The delimiter of the token file (`str`).
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringReader(object):
    """A reader class for prescription string data. This is used to output
    objects that can be used in the DMD import functions.

    Parameters
    ----------
    inpath : `str`
        The path to the input file, can be compressed.
    pres_str_col : `str`
        The name of the prescription string column in the input file.
    delimiter : `str`, optional, default: `NoneType`
        The delimiter of the input file. If not supplied then the input file
        will be sniffed by CSV to detect the dialect.
    code_cols : `dict`, optional, default, `NoneType`
        Mappings between external code type names and the column names in the
        input file. The code type names should be lowercase and have no spaces.
    dmd_code_col : `str`, optional, default: `NoneType`
        The name for the DM+D code column in the input file.
    count_col : `str`, optional, default, `NoneType`
        The name of any EHR count columns in the input file.
    metadata_cols : `dict`, optional, default: `NoneType`
        Any additional metadata columns that appear in the input file. The dict
        keys should be column names of the metadata columns in the input file
        and the values should be the metadata types you want them to be known
        as in the database. The values should be lowercase with no spaces.
    """
    COUNT_STR_COL = "pres_str_count"
    """The name for the mapped prescription string counts column read in from
    the input file (`str`)
    """
    METADATA_COL = "metadata"
    """The name for the metadata field in the input row object (`str`).
    """
    EMPTY_ROW = {
        add_dmd.ALIAS_PRES_STR: None,
        add_dmd.ALIAS_CODE: None,
        add_dmd.ALIAS_EXT_CODE_ID: None,
        add_dmd.ALIAS_EXT_CODE_TYPE: None,
        COUNT_STR_COL: None,
        METADATA_COL: None
    }
    """The skeleton of an empty row that will be filled for each input row
    (`dict`)
    """
    FileRow = namedtuple(
        "FileRow", list(EMPTY_ROW.keys())
    )
    """A namedtuple definition for an input row (`namedtuple`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, pres_str_col, delimiter=None, code_cols=None,
                 dmd_code_col=None, count_col=None, metadata_cols=None):
        self.infile_path = infile
        self.count_col = count_col
        self.code_cols = _process_code_cols(code_cols)
        self.colmap = dict()
        self.colmap[add_dmd.ALIAS_PRES_STR] = pres_str_col
        if count_col is not None:
            self.colmap[self.COUNT_STR_COL] = count_col

        if dmd_code_col is not None:
            self.colmap[add_dmd.ALIAS_CODE] = dmd_code_col

        self.open_method = utils.get_open_method(self.infile_path)
        self.header_row, dialect, has_header, skip_rows = \
            header.detect_header(
                self.infile_path, open_method=self.open_method
            )

        self.metadata_map = None
        if metadata_cols is not None:
            self.metadata_map = _process_metadata_cols(
                metadata_cols, self.header_row
            )

        self.csv_kwargs = dict(delimiter=delimiter)
        if delimiter is not None:
            self.csv_kwargs = dict(dialect=dialect)

        self.code_col_name = None
        if code_cols is not None:
            self.set_code_col(list(code_cols.keys())[0])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        row = next(self.reader)
        # pp.pprint(row)
        kwargs = {
            **self.EMPTY_ROW,
            **dict([(i, row[j].strip()) for i, j in self.colmap.items()])
        }
        # pp.pprint(kwargs)
        if kwargs[add_dmd.ALIAS_CODE] is not None:
            try:
                kwargs[add_dmd.ALIAS_CODE] = int(kwargs[add_dmd.ALIAS_CODE])
            except (ValueError, TypeError):
                kwargs[add_dmd.ALIAS_CODE] = None

        if self.code_col_name is not None:
            kwargs[add_dmd.ALIAS_EXT_CODE_ID] = \
                row[self.ext_code_file_col].strip()
            kwargs[add_dmd.ALIAS_EXT_CODE_TYPE] = \
                self.code_col_name

        if self.count_col is not None:
            kwargs[self.COUNT_STR_COL] = int(kwargs[self.COUNT_STR_COL])

        metadata = dict()
        if self.metadata_map is not None:
            for s, t in self.metadata_map.items():
                metadata[t] = row[s].strip()
        kwargs[self.METADATA_COL] = metadata
        return self.FileRow(**kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the input file. This will initialise a `csv.DictReader`.
        """
        self.infile_obj = self.open_method(self.infile_path, 'rt')
        header.move_to_header(self.infile_obj)
        self.reader = csv.DictReader(
            self.infile_obj, **self.csv_kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the input file.
        """
        try:
            self.infile_obj.close()
        except AttributeError as e:
            raise IOError("can't close file, is it open?") from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def count(self):
        """Provide a count of the rows in the input file. This is a method that
        is only present for interface purposes.

        Returns
        -------
        nothing : `NoneType`
            A null value.
        """
        return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_code_col(self, code_col_name):
        """In the cases where > 1 external code column is available set the one
        that will be available in the input row.

        Parameters
        ----------
        code_col_name : `str`
            The external code column that will be available in the input row.

        Notes
        -----
        This odd arrangement is because we are using the external code function
        of the `dmd.pres_db.add_dmd` module, that only operates in a single
        field. So if multiple external row columns are available, then the file
        will have to be traversed multiple times setting the correct column
        each time.
        """
        try:
            self.ext_code_file_col = self.code_cols[code_col_name]
        except KeyError as e:
            raise KeyError(f"unknown code column: {code_col_name}") from e

        self.code_col_name = code_col_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_file.add_file``.
    """
    # Initialise and parse the command line arguments
    arg_parser = _init_cmd_args()
    args = _parse_cmd_args(arg_parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    pres_sm = cfg.get_sessionmaker(
        args.config_section, common._DEFAULT_PREFIX,
        url_arg=args.pres_db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    pres_session = pres_sm()

    try:
        other_len = len(args.other_codes)

        if other_len != len(args.code_types):
            raise AttributeError("codes not equal length")

        code_cols = dict(
            [(i, j) for i, j in zip(args.code_types, args.other_codes)]
        )
    except (TypeError, AttributeError) as e:
        if args.code_types is not None:
            raise ValueError(
                "other_codes and code_types must be the same length"
            ) from e

    try:
        meta_cols = None
        # Parse any metadata columns/types
        meta_len = len(args.metadata_cols)

        if meta_len != len(args.metadata_types):
            raise AttributeError("metadata columns/types not equal length")

        meta_cols = dict(
            [(i, j) for i, j in zip(args.metadata_cols, args.metadata_types)]
        )
    except (TypeError, AttributeError) as e:
        if args.metadata_types is not None:
            raise ValueError(
                "metadata columns/types not equal length"
            ) from e

    try:
        # Add the dict file
        add_file(pres_session, args.data_source, args.source_version,
                 args.infile, args.pres_str, dmd_code_col=args.dmd_code,
                 count_col=args.count, code_cols=code_cols,
                 verbose=args.verbose, delimiter=args.delimiter,
                 pretty_data_source=args.pretty_data_source,
                 metadata_cols=meta_cols)
        # Create a temp directory that we will blitz if we error out
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        pres_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    arg_parser = argparse.ArgumentParser(
        description=_DESC
    )

    arg_parser.add_argument(
        'infile',
        type=str,
        help="The path to prescription string file import"
    )
    arg_parser.add_argument(
        'data_source',
        type=str,
        help="The name of the data source of the input file."
    )
    arg_parser.add_argument(
        'source_version',
        type=str,
        help="The data source version of the input file."
    )
    arg_parser.add_argument(
        'pres_db_url',
        nargs='?',
        type=str,
        help="The prescription database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    arg_parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    arg_parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    arg_parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    arg_parser.add_argument(
        '-v', '--verbose', action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    arg_parser.add_argument(
        '-d', '--delimiter', type=str, default="\t",
        help="The delimiter of the input prescription string file"
    )
    arg_parser.add_argument(
        '-P', '--pretty-data-source', type=str,
        help="A human friendly name for the data source."
    )
    arg_parser.add_argument(
        '-D', '--dmd-code', type=str,
        help="The name of the DM+D code column if available"
    )
    arg_parser.add_argument(
        '--count', type=str,
        help="The name of any source counts for the prescription string "
        "occurrence."
    )
    arg_parser.add_argument(
        '-C', '--other-codes', type=str, nargs="+",
        help="The name of any other code type columns if available, each code"
        " column name should be listed here, with the code types listed "
        "under --code-types"
    )
    arg_parser.add_argument(
        '-Y', '--code-types', type=str, nargs="+",
        help="The code types for the code columns, there should be the same"
        " number of code types as --other-code columns. Code types should"
        " have no spaces and be lowercase"
    )
    arg_parser.add_argument(
        '--metadata-cols', type=str, nargs="+",
        help="The name of any other columns that contain useful metadata."
        " These are imported as is along with their type, which should be"
        " defined in --metadata-types. There should be the same number of"
        " --metadata-types as columns"
    )
    arg_parser.add_argument(
        '--metadata-types', type=str, nargs="+",
        help="The types of metadata columns. These are strings and should be"
        " lowercase with no spaces. They should define the type of the "
        "columns given in --metadata-cols"
    )
    arg_parser.add_argument(
        '-S', '--pres-str', type=str, default="prescription",
        help="The name of the prescription string column, this must be"
        " defined."
    )
    return arg_parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(arg_parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = arg_parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.realpath(os.path.expanduser(args.config))
    args.infile = os.path.realpath(os.path.expanduser(args.infile))
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_file(session, data_source, source_version, infile, pres_str_col,
             dmd_code_col=None, count_col=None, code_cols=None, verbose=False,
             delimiter=None, pretty_data_source=None, metadata_cols=None):
    """Add a prescription file to the prescription string database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    data_source : `str`
        The data source for the input file.
    source_version : `str`
        The version number/ID for the input file.
    infile : `str`
        The path to the input file. Can be compressed.
    pres_str_col : `str`
        The name of the prescription string column.
    dmd_code_col : `str`, optional, default: `NoneType`
        The name for any DM+D code columns.
    count_col : `str`, optional, default: `NoneType`
        Any counts of the number of times a prescription string occurs in the
        data source.
    code_cols : `dict`, optional, default: `NoneType`
        Any other code columns, the dict keys are the code column names and the
        values are the code_type as they should be known in the database.
        Code types should be lowercase with no spaces.
    verbose : `bool`, optional, default: `False`
        Show progress of the input file parsing.
    delimiter : `str`, optional, default: `NoneType`
        The delimiter of the input file. If not supplied then the dialect is
        sniffed with CSV.
    pretty_data_source : `str`, optional, default: `NoneType`
        A pretty human readable name for the data source that can be used in
        plots etc... If not supplied then the `data_source` argument is used.
    metadata_cols : `dict`, optional, default: `NoneType`
        Any additional metadata columns that appear in the input file. The dict
        keys should be column names of the metadata columns in the input file
        and the values should be the metadata types you want them to be known
        as in the database. The values should be lowercase with no spaces.

    Notes
    -----
    The import will involve several traversals of the input file, gathering
    specific data each time.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    logger.info("ensuring all pres-db tables are in place")
    common.create_tables(session)
    nadded = 0
    # Add/get the source and source version
    version = add_source(session, data_source, source_version,
                         pretty_data_source=pretty_data_source)

    args = (infile, pres_str_col)
    kwargs = dict(
        delimiter=delimiter, code_cols=code_cols, dmd_code_col=dmd_code_col,
        count_col=count_col, metadata_cols=metadata_cols
    )
    pres_str_cache = None
    with PresStringReader(*args, **kwargs) as reader:
        logger.info("adding all strings...")
        nadded, pres_str_cache = add_dmd._test_strings(
            version, session, reader, verbose=verbose, is_amp=False,
            msg="[info] checking file strings", pres_str_cache=None,
            is_dmd=False
        )
        logger.info(f"added #strings: {nadded}")

    if dmd_code_col is not None:
        with PresStringReader(*args, **kwargs) as reader:
            logger.info("adding all codes...")
            nadded, code_cache = add_dmd._test_codes(
                version, session, reader, verbose=verbose, is_dmd=False,
                msg="[info] checking file codes",
                pres_str_cache=pres_str_cache,
                code_cache=None
            )
            logger.info(f"added #codes: {nadded}")

    # Add the occurrence data
    with PresStringReader(*args, **kwargs) as reader:
        logger.info("adding occurrences...")
        nadded, occurs_cache = add_dmd._test_occurrences(
            version, session, reader, verbose=verbose,
            msg="[info] checking file occurrences",
            pres_str_cache=pres_str_cache, occurs_cache=None
        )
        logger.info(f"added #occurrences: {nadded}")

    if code_cols is not None:
        for i in code_cols.keys():
            code_map = {
                i: add_dmd.add_external_code_type(
                    session, i
                ).external_code_type_id
            }
            with PresStringReader(*args, **kwargs) as reader:
                logger.info(f"adding external code {i}...")
                nadded, ext_code_cache = add_dmd._test_ext_codes(
                    version, session, reader, code_map, verbose=verbose,
                    msg=f"[info] checking file {i} external codes",
                    pres_str_cache=pres_str_cache, ext_code_cache=None
                )
                logger.info(f"added #external codes: {nadded}")

    if count_col is not None:
        with PresStringReader(*args, **kwargs) as reader:
            logger.info("adding counts...")
            nadded, count_cache = _test_counts(
                version, session, reader, verbose=verbose,
                msg="[info] checking file counts",
                pres_str_cache=pres_str_cache, count_cache=None
            )
            logger.info(f"added #counts: {nadded}")

    if metadata_cols is not None:
        with PresStringReader(*args, **kwargs) as reader:
            logger.info("adding metadata...")
            nadded, metadata_cache = _test_metadata(
                version, session, reader, verbose=verbose,
                msg="[info] checking file metadata cols",
                pres_str_cache=pres_str_cache, metadata_cache=None)
            logger.info(f"added #metadata: {nadded}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_counts(session, data_source_id=None):
    """Cache all of the prescription string counts.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    data_source_id : `int`, optional, default: `NoneType`
        Only cache count data from a specific data source ID. If not
        supplied then all count data is cached.

    Returns
    -------
    cache : `dict`
        A cache of counts. The keys are (pres_str_id, data_source_id) and the
        values are counts `int`.

    Notes
    -----
    Note that in the count data the source_version_id is stored but the
    cache caches at the level of data_source_id. This is so we only add in new
    data from the data source and not all the individual source data.
    """
    q = session.query(
        porm.PresStringCount.pres_str_id,
        porm.PresStringCount.pres_str_count,
        porm.SourceVersion.data_source_id,
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.PresStringCount.source_version_id
    )

    if data_source_id is not None:
        q = q.filter(porm.SourceVersion.data_source_id == data_source_id)

    cache = dict()
    for row in q:
        cache[(row.pres_str_id, row.data_source_id)] = row.pres_str_count
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_metadata(session, data_source_id=None):
    """Cache all of the metadata values.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    data_source_id : `int`, optional, default: `NoneType`
        Only cache metadata values from a specific data source ID. If not
        supplied then all metadata is cached.

    Returns
    -------
    metadata_cache : `set` of (`int`, `int`, `int`, `str`)
        A cache of (pres_str_id, metadata_type_id, data_source_id,
        metadata_hash).

    Notes
    -----
    Note that in the metadata the source_version_id is stored but the
    cache caches at the level of data_source_id. This is so we only add in new
    data from the metadata source and not all the individual source data.
    """
    q = session.query(
        porm.PresStringMetadata.pres_str_id,
        porm.PresStringMetadata.pres_str_meta_type_id,
        porm.PresStringMetadata.pres_str_meta_value,
        porm.SourceVersion.data_source_id,
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.PresStringMetadata.source_version_id
    )

    if data_source_id is not None:
        q = q.filter(porm.SourceVersion.data_source_id == data_source_id)

    cache = set()
    for row in q:
        cache.add((
            row.pres_str_id, row.pres_str_meta_type_id,
            row.data_source_id, add_dmd.hashstr(row.pres_str_meta_value)
        ))
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_counts(version, pres_session, q, verbose=False,
                 msg="[info] checking counts", pres_str_cache=None,
                 count_cache=None):
    """Add any count data in the import file. Counts are the number of
    occurrences for each prescription string.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `dmd.pres_db.add_file.PresStringReader`
        A reader to provise input rows. The count data should be under the
        ``count`` field in each input row.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    msg : `str`, optional, default: `[info] checking counts`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    count_cache : `dict`, optional, default: `NoneType`
        A cache of counts. The keys are (pres_str_id, data_source_id) and the
        values are counts `int`. If not supplied then it is queried from the
        database.

    Returns
    -------
    nadded : `int`
        The number of new occurrences that have been added from the DM+D source
        database.
    count_cache : `dict`
        A cache of counts. The keys are (pres_str_id, data_source_id) and the
        values are counts `int`.

    Notes
    -----
    Please not that re-importing the same file will update the counts so, this
    should be avoided.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    data_source_id = version.data_source_id

    if pres_str_cache is None:
        pres_str_cache = add_dmd.cache_strings(pres_session)
    if count_cache is None:
        count_cache = cache_counts(
            pres_session, data_source_id=data_source_id
        )

    logger.info(
        f"the count cache contains #entries: {len(count_cache)}"
    )

    # Count how many are added and commit every 1000
    idx = 0
    commit_at = 5000
    row_buffer = []
    update_rows = dict()
    for row in tqdm(q, **tqdm_kwargs):
        if row.pres_str in add_dmd.MISSING or \
           row.pres_str_count in add_dmd.MISSING:
            continue

        str_id = pres_str_cache[add_dmd.hashstr(row.pres_str)]
        key = (str_id, data_source_id)

        if key not in count_cache:
            # Definitely add
            count_entry = porm.PresStringCount(
                pres_str_id=str_id,
                source_version=version,
                pres_str_count=row.pres_str_count
            )
            row_buffer.append(count_entry)
            count_cache[key] = row.pres_str_count
            nadded += 1
            idx += 1
        else:
            cache_value = count_cache[key]
            if row.pres_str_count != cache_value:
                try:
                    # If it is already in the updated then the cache has
                    # already been included
                    update_rows[key] += row.pres_str_count
                except KeyError:
                    # If not then include the cache count
                    update_rows[key] = (row.pres_str_count + cache_value)

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
            idx = 0
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
        idx = 0

    tqdm_kwargs['desc'] = '[info] preforming count updates'
    # Now perform any updates
    for k, co in tqdm(update_rows.items(), **tqdm_kwargs):
        str_id, ds_id = k
        to_update = pres_session.query(
            porm.PresStringCount
        ).join(
            porm.SourceVersion,
            porm.SourceVersion.source_version_id ==
            porm.PresStringCount.source_version_id
        ).filter(
            and_(
                porm.PresStringCount.pres_str_id == str_id,
                porm.SourceVersion.data_source_id == ds_id
            )
        ).one()
        to_update.pres_str_count = co
        pres_session.add(to_update)
    pres_session.commit()
    return nadded, count_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_metadata(version, pres_session, q, verbose=False,
                   msg="[info] checking metadata cols",
                   pres_str_cache=None, metadata_cache=None):
    """Add any metadata values in the import file. Metadata are any useful
    additional info about the prescription string.

    Parameters
    ----------
    version : `dmd.pres_db.orm.SourceVersion`
        The version of the DM+D that is being added.
    pres_session : `sqlalchemy.Session`
        A session object bound to the prescription string database.
    q : `dmd.pres_db.add_file.PresStringReader`
        A reader to provise input rows. The count data should be under the
        ``count`` field in each input row.
    verbose : `bool`, optional, default: `False`
        Turn on logging/progress monitoring. Set to `2` to turn on progress
        monitoring.
    msg : `str`, optional, default: `[info] checking metadata cols`
        A customisable progress message.
    pres_str_cache : `dict`, optional, default: `NoneType`
        A cache/mapping between prescription string hashes (`str`) and
        prescription string database ids (`int`). If not supplied then it is
        queried from the database.
    metadata_cache : `set` of (`int`, `int`, `int`, `str`), optional,\
    default: `NoneType`
        A cache of (pres_str_id, metadata_type_id, data_source_id,
        metadata_hash). If not supplied then it is queried from the
        database.

    Returns
    -------
    nadded : `int`
        The number of new entries that have been added from the DM+D source
        database.
    metadata_cache : `set` of (`int`, `int`, `int`, `str`)
        A cache of (pres_str_id, metadata_type_id, data_source_id,
        metadata_hash). New entries will be added.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Track how many new VMPs have been added
    nadded = 0

    # Get the count for the progress monitor
    total = q.count()

    tqdm_kwargs = dict(
        desc=msg,
        unit=" rows",
        total=total,
        leave=False,
        disable=not prog_verbose
    )

    data_source_id = version.data_source_id

    if pres_str_cache is None:
        pres_str_cache = add_dmd.cache_strings(pres_session)
    if metadata_cache is None:
        metadata_cache = cache_metadata(
            pres_session, data_source_id=data_source_id
        )

    logger.info(
        f"the metadata cache contains #entries: {len(metadata_cache)}"
    )

    # Generate metadata type IDs
    metadata_id_map = dict()
    for i in q.metadata_map.values():
        metadata_id_map[i] = add_metadata_type(
            pres_session, i
        ).pres_str_meta_type_id

    # Count how many are added and commit every 1000
    commit_at = 5000
    row_buffer = []
    for row in tqdm(q, **tqdm_kwargs):
        if row.pres_str in add_dmd.MISSING:
            continue

        str_id = pres_str_cache[add_dmd.hashstr(row.pres_str)]
        for t, v in row.metadata.items():
            if v not in add_dmd.MISSING:
                vhash = add_dmd.hashstr(v)
                tid = metadata_id_map[t]
                key = (str_id, tid, data_source_id, vhash)

                if key not in metadata_cache:
                    entry = porm.PresStringMetadata(
                        pres_str_id=str_id,
                        pres_str_meta_type_id=tid,
                        source_version=version,
                        pres_str_meta_value=v
                    )
                    row_buffer.append(entry)
                    metadata_cache.add(key)
                    nadded += 1

        if len(row_buffer) >= commit_at:
            pres_session.add_all(row_buffer)
            pres_session.flush()
            pres_session.commit()
            row_buffer = []
    # Final commit
    if len(row_buffer) > 0:
        pres_session.add_all(row_buffer)
        pres_session.flush()
        pres_session.commit()
        row_buffer = []
    return nadded, metadata_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_code_cols(code_cols):
    """Make sure the external code column types are lowercase and have no
    spaces.

    Parameters
    ----------
    code_cols : `dict`
        Mappings between external code type names and the column names in the
        input file. The code type names should be lowercase and have no spaces.

    Returns
    -------
    proc_code_cols : `dict`
        Valid mappings between external code type names and the column names in
        the input file. The code type names will be lowercase and have no
        spaces.
    """
    try:
        colmap = dict(
            [(re.sub(r'\s+', '_', i.lower()), j) for i, j in code_cols.items()]
        )

        if len(colmap) != len(code_cols):
            raise ValueError("processed code columns are not unique")

        return colmap
    except (TypeError):
        return dict()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_metadata_cols(meta_cols, header_row):
    """Make sure the metadata type columns are lowercase and have no
    spaces. Additionally, make sure their source columns are in the header.

    Parameters
    ----------
    meta_cols : `dict`
        Mappings between metadata column names in the source file and their
        type in the database. The types names should be lowercase and have no
        spaces.

    Returns
    -------
    colmap : `dict`
        Valid mappings between source metadata column names and their metadata
        type names in the database. If no metadata is defined this will be an
        empty dict.
    """
    for s, t in meta_cols.items():
        if s not in header_row:
            raise KeyError(f"metadata column not in file header: {s}")

    try:
        colmap = dict(
            [(i, re.sub(r'\s+', '_', j.lower())) for i, j in meta_cols.items()]
        )

        if len(set(colmap.values())) != len(meta_cols):
            raise ValueError("processed metadata columns are not unique")

        return colmap
    except (TypeError):
        return dict()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_metadata_type(pres_session, metadata_type_name):
    """Add/Get the metadata type object that corresponds to the metadata type
    name.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    metadata_type_name : `str`
        The name of the type of metadata.

    Returns
    -------
    version : `dmd.pres_db.orm.PresStringMetadataType`
        The metadata type object that corresponds to the name.
    """
    try:
        # Does it already exist?
        return pres_session.query(
            porm.PresStringMetadataType
        ).filter(
            porm.PresStringMetadataType.pres_str_meta_type_name ==
            metadata_type_name
        ).one()
    except NoResultFound:
        # No, so add it
        meta_type_entry = porm.PresStringMetadataType(
            pres_str_meta_type_name=metadata_type_name
        )
        pres_session.add(meta_type_entry)
        pres_session.commit()
        return meta_type_entry


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_source(pres_session, data_source_name, source_version_name,
               pretty_data_source=None):
    """Add the input file source to the prescription string sources and version
    table.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    data_source_name : `str`
        The name of the data source that the input file represents.
    source_version_name : `str`
        The name of the data source version number that the input file
        represents.

    Returns
    -------
    version : `dmd.pres_db.orm.SourceVersion`
        The source version for the input file being processed.
    """
    # Make sure that the data source and source version names are valid
    data_source_name = re.sub(r'\s+', '_', data_source_name.strip().lower())
    source_version_name = re.sub(
        r'\s+', '_', source_version_name.strip().lower()
    )

    for i in [data_source_name, source_version_name]:
        if i in add_dmd.MISSING:
            raise ValueError("bad data_source or source_version name")
    pretty_data_source = pretty_data_source or data_source_name

    try:
        # First make sure the DM+D is registered as a version in the database
        data_source = pres_session.query(
            porm.DataSource
        ).filter(
            porm.DataSource.data_source_name == data_source_name
        ).one()
    except NoResultFound:
        # No, so add it
        data_source = porm.DataSource(
            data_source_name=data_source_name,
            data_source_pretty=pretty_data_source,
        )
        pres_session.add(data_source)
        pres_session.commit()

    try:
        # See if it already exists, if so then return for use
        version = pres_session.query(
            porm.SourceVersion
        ).filter(
            and_(
                porm.SourceVersion.data_source_id ==
                data_source.data_source_id,
                porm.SourceVersion.version_name == source_version_name
            )
        ).one()
        return version
    except NoResultFound:
        # No, so add and return
        version = porm.SourceVersion(
            data_source_id=data_source.data_source_id,
            version_name=source_version_name
        )
        pres_session.add(version)
        pres_session.commit()
        return version


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
