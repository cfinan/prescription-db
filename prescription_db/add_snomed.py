"""Add snomed CT relationship annotations to the prescription strings in the
 database.

Note, by default, this only add snomed descriptions to those prescription
 strings that do not have them already.

The snomed codes are added to the ``pres_string_snomed`` table. They are
flattened relationships and are not properly normalised (i.e. lots of string
 repetition) to enable easy querying and analyses, although, I might change
 this in future.

Note that this takes a while to run but can be interrupted and subsequent runs
 should  pickup from where it left off.
"""
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    orm as porm,
    common
)
from snomed_ct import query, orm as sorm
from sqlalchemy_config import config as cfg
from pyaddons import log
from tqdm import tqdm
import argparse
import os
import sys
# import pprint as pp


_PROG_NAME = 'pres-db-snomed'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_snomed.add_snomed``.
    """
    # Initialise and parse the command line arguments
    arg_parser = _init_cmd_args()
    args = _parse_cmd_args(arg_parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    pres_sm = cfg.get_sessionmaker(
        args.pres_config_section,
        common._DEFAULT_PREFIX,
        url_arg=args.pres_db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    pres_session = pres_sm()

    # Connect to the snomed database
    snomed_sm = cfg.get_sessionmaker(
        args.snomed_config_section,
        common._DEFAULT_PREFIX,
        url_arg=args.snomed_db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    snomed_session = snomed_sm()

    try:
        # Add the snomed relationships
        add_snomed(pres_session, snomed_session, verbose=args.verbose,
                   drop=args.drop, distance=args.distance)
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        pres_session.close()
        snomed_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    arg_parser = argparse.ArgumentParser(
        description=_DESC
    )

    arg_parser.add_argument(
        '--pres-db-url',
        type=str,
        help="The prescription database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    arg_parser.add_argument(
        '--snomed-db-url',
        type=str,
        help="The snomed database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    arg_parser.add_argument(
        '-v', '--verbose', action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    arg_parser.add_argument(
        '-D', '--drop', action="store_true",
        help="Drop any existing snomed tables rather than updating missing"
        " prescription strings"
    )
    arg_parser.add_argument(
        '-d', '--distance', type=int, default=2,
        help="Query the relationships up to this distance from the product."
    )
    arg_parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    arg_parser.add_argument(
        '-T', '--tmpdir', type=str,
        help="The location of the temp directory. Relationships are written"
        " to temp before being imported. If not supplied then the default"
        "system temp location is used."
    )
    arg_parser.add_argument(
        '-p', '--pres-config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name for the prescription DB in the config file"
    )
    arg_parser.add_argument(
        '-s', '--snomed-config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name for the snomed DB in the config file"
    )
    return arg_parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(arg_parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = arg_parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.realpath(os.path.expanduser(args.config))
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_snomed(pres_session, snomed_session, verbose=False, distance=3,
               drop=False):
    """Add snomed relationships to the prescription string database.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    snomed_session : `sqlalchemy.Session`
        The session object attached to the snomed database.
    verbose : `bool`, optional, default: `False`
        Show progress of the input file parsing.
    distance : `int`, optional, default: `3`
        Query snomed relationships that are up to this many nodes away from the
        source prescription string.
    drop : `bool`, optional, default: `False`
        Drop any existing snomed tables before re-querying.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    if drop is True:
        try:
            logger.info("dropping previous snomed tables")
            porm.PresStringSnomed.__table__.drop(pres_session.get_bind())

            logger.info("ensuring all pres-db tables are in place")
            common.create_tables(pres_session)
        except Exception:
            logger.info("ensuring all pres-db tables are in place")
            common.create_tables(pres_session)

            # The exception probably means the table is not there so we see if
            # there are rows in it, is so we error
            nrows = pres_session.query(porm.PresStringSnomed).limit(10).count()
            if nrows > 0:
                raise ValueError("snomed table still has data in it")

    nadded = 0
    nmissing = missing_id_query(pres_session).count()
    logger.info(f"need to process #prescriptions: {nmissing}")

    tqdm_kwargs = dict(
        total=nmissing,
        desc="[info] processing prescriptions",
        unit=" prescriptions",
        disable=not prog_verbose
    )

    row_buffer = []
    for row in tqdm(missing_id_query(pres_session),
                    **tqdm_kwargs):
        trees, code_match = get_relationships(
            snomed_session, row, distance=distance
        )
        unique_rels = set([])
        for rels in trees:
            rel_type = 'destination'
            for idx, i in enumerate(rels):
                # print(i)
                unique_rels.add((
                    ('pres_str_id', row.pres_str_id),
                    ('rel_distance', idx),
                    ('concept_id', i[rel_type]['concept_id']),
                    ('concept_term', i[rel_type]['term']),
                    ('type_id', i['rel_type']['concept_id']),
                    ('type_term', i['rel_type']['term']),
                    ('char_type_id', i['char_type']['concept_id']),
                    ('char_type_term', i['char_type']['term']),
                    ('modifier_id', i['modifier']['concept_id']),
                    ('modifier_term', i['modifier']['term']),
                    ('code_match', code_match)
                ))
                rel_type = 'source'

        seen_ids = set()
        for i in sorted(unique_rels, key=lambda x: x[1][1]):
            # If the concept Id has already been seen then skip adding it
            if i[3][1] in seen_ids:
                seen_ids.add(i[3][1])
                continue
            row_buffer.append(
                porm.PresStringSnomed(**dict([(j, k) for j, k in i]))
            )
            # Add the concept to the seen IDs
            seen_ids.add(i[2][1])

        if len(row_buffer) >= 10000:
            pres_session.bulk_save_objects(row_buffer)
            pres_session.commit()
            row_buffer = []

        nadded += bool(len(trees))

    if len(row_buffer) >= 0:
        pres_session.bulk_save_objects(row_buffer)
        pres_session.commit()
        row_buffer = []
    logger.info(f"#prescription strings added: {nadded}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_relationships(session, row, distance=3):
    """Query the relationships for a prescription string row.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object attached to the snomed database.
    row : `dmd.pres_db.orm.PresString`
        A prescription string object.
    distance : `int`, optional, default: `3`
        Query snomed relationships that are up to this many nodes away from the
        source prescription string.

    Returns
    -------
    relationships : `list` of `list` of `dict`
        The relationships for the prescription string. Each sub-list represents
        a linear route through the tree.
    code_match : `bool`
        By default, the prescription string is searched against the description
        table to get snomed concept codes applicable for it. However, if no
        match is found then the prescription string codes are searched it see
        if they are available and the first matching code is used. If this is
        True, then codes were used to derive the relationships, if False, then
        strings were used.
    """
    code_match = False
    # print(row.pres_str_name)
    descs = session.query(sorm.SctDescription).filter(
        sorm.SctDescription.term == row.pres_str_name
    ).all()

    # If no description match is found
    if len(descs) == 0:
        # Get the codes that align with the prescription string
        # If they are from the DM+D then search for a code match in Snomed
        # descriptions.
        codes = row.pres_str_code
        for c in codes:
            if c.is_dmd is True:
                descs = session.query(sorm.SctDescription).filter(
                    sorm.SctDescription.concept_id == c.pres_str_code
                ).all()

                # We have some matches on code, so we will take the first
                # match
                if len(descs) > 0:
                    code_match = True
                    break

    if len(descs) == 0:
        return [], code_match

    codes = [i.concept_id for i in descs]
    # Get the relationship matches for the first code that is associated
    # with the relationships
    rels = query.get_source_releationships(
        session, codes[0], distance=distance
    )
    return [query.parse_relationship_tree(i) for i in rels], code_match


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def missing_id_query(session):
    """Return a query for obtaining the prescription strings that have no
    snomed data associated with them.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object attached to the prescription string database.

    Returns
    -------
    query : `sqlalchemy.Query`
        An un-executed query object.
    """
    return session.query(
        porm.PresString
    ).join(
        porm.PresStringSnomed,
        porm.PresStringSnomed.pres_str_id == porm.PresString.pres_str_id,
        isouter=True
    ).filter(
        porm.PresStringSnomed.pres_str_id == None
    ).group_by(
        porm.PresString.pres_str_id
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
