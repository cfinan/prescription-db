"""Common functions and variables for the prescription string database.
"""
from prescription_db import orm as porm
from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound
import os


try:
    _DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None


_DEFAULT_SECTION = 'pres-db'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""

_DEFAULT_PREFIX = 'db.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""

DMD_DATA_SOURCE = "DMD"
"""The name for a DM+D data source in the prescription string database (`str`)
"""
DMD_PRETTY_NAME = "DM+D"
"""The pretty formatted name for a DM+D data source (`str`)
"""

CLASS_DRUG = 'drug'
"""The class name for a drug (`str`)
"""
CLASS_ROA = 'form_roa'
"""The class name for a route of administration (`str`)
"""
CLASS_ING = 'misc_ingredients'
"""The class name for miscellaneous ingredient (`str`)
"""
CLASS_PERSON = 'person_type'
"""The class name for a person type (`str`)
"""
CLASS_SUPPLIER = 'supplier'
"""The class name for a supplier (`str`)
"""
CLASS_UNIT = 'unit'
"""The class name for a unit (`str`)
"""

SUB_CLASS_UNKNOWN_DRUG = 'unknown_drug'
"""The sub-class name for an unknown drug (`str`)
"""
SUB_CLASS_DRUG_SALT = 'salt'
"""The sub-class name for a drug salt (`str`)
"""
SUB_CLASS_REVERSE = "reverse"
"""The sub-class name for a unit that can be present in reverse order (`str`)
"""
SUB_CLASS_NO_REVERSE = "no_reverse"
"""The sub-class name for a unit that can't be present in reverse order (`str`)
"""
SUB_CLASS_QUALITATIVE = 'qualitative'
"""The sub-class name for a qualitative unit (`str`)
"""
SUB_CLASS_HOMEO = 'homeopathic'
"""The sub-class name for a homeopathic drug (`str`)
"""
SUB_CLASS_PLANT = 'plant'
"""The sub-class name for a plant derived homeopathic drug (`str`)
"""

# External code names
EXT_CODE_BNF = "bnf"
"""The name for a BNF (`str`)
"""
EXT_CODE_ATC = "atc"
"""The name for an ATC external code (`str`)
"""

NO_VALUE = 'none'
"""A general indicator that a field contains no value (`str`)
"""

BASE_FORMS = [
    'solid',
    'liquid',
    'aerosol',
    'absence',
    'unknown'
]
"""Very high level types that a prescription token can map to
(`list` of `str`).
"""


BASE_TYPES = [
    'device',
    CLASS_DRUG,
    'food',
    'garment',
    'hygene',
    'incontinence_device',
    'vaccine',
    'wound_dressing',
    'unknown'
]
"""Very high level forms that a prescription token can map to
(`list` of `str`).
"""

CLASS_TYPES = [
    CLASS_DRUG,
    CLASS_ROA,
    CLASS_ING,
    CLASS_PERSON,
    CLASS_SUPPLIER,
    CLASS_UNIT,
]
"""High level categories for tokens (`list` of `str`).
"""

SUB_CLASS_TYPES = [
    (CLASS_DRUG, NO_VALUE),
    (CLASS_DRUG, 'antibody'),
    (CLASS_DRUG, 'cell'),
    (CLASS_DRUG, 'enzyme'),
    (CLASS_DRUG, 'gene'),
    (CLASS_DRUG, SUB_CLASS_HOMEO),
    (CLASS_DRUG, 'oligonucleotide'),
    (CLASS_DRUG, 'oligosaccharide'),
    (CLASS_DRUG, SUB_CLASS_PLANT),
    (CLASS_DRUG, 'protein'),
    (CLASS_DRUG, SUB_CLASS_DRUG_SALT),
    (CLASS_DRUG, 'small_molecule'),
    (CLASS_DRUG, SUB_CLASS_UNKNOWN_DRUG),
    (CLASS_DRUG, 'vaccine'),
    (CLASS_ROA, NO_VALUE),
    (CLASS_ING, NO_VALUE),
    (CLASS_PERSON, NO_VALUE),
    (CLASS_SUPPLIER, NO_VALUE),
    (CLASS_UNIT, SUB_CLASS_NO_REVERSE),
    (CLASS_UNIT, SUB_CLASS_REVERSE),
    (CLASS_UNIT, NO_VALUE),
    (CLASS_UNIT, SUB_CLASS_QUALITATIVE),
    (CLASS_UNIT, 'ratio_unit'),
    (CLASS_UNIT, 'rate_unit'),
    (CLASS_UNIT, 'range_unit'),
    (CLASS_UNIT, 'percent_unit'),
    (CLASS_UNIT, 'combined_unit'),
    (CLASS_UNIT, 'volume_unit'),
    (CLASS_UNIT, 'area_unit'),
]
"""Lower-level categories for tokens (`list` of `tuple`).
"""

ROA = [
    "auricular",
    "body cavity",
    "buccal",
    "cutaneous",
    "dental",
    "endocervical",
    "endosinusial",
    "endotracheopulmonary",
    "epidural",
    "epilesional",
    "extraamniotic",
    "gastroenteral",
    "gingival",
    "haemodialysis",
    "haemofiltration",
    "infiltration",
    "inhalation",
    "intestinal",
    "intraamniotic",
    "intraarterial",
    "intraarticular",
    "intrabursal",
    "intracameral",
    "intracardiac",
    "intracatheter instillation",
    "intracavernous",
    "intracerebroventricular",
    "intracervical",
    "intracholangiopancreatic",
    "intracoronary",
    "intradermal",
    "intradialytic",
    "intradiscal",
    "intraepidermal",
    "intraglandular",
    "intralesional",
    "intralymphatic",
    "intramuscular",
    "intraocular",
    "intraosseous",
    "intraperitoneal",
    "intrapleural",
    "intraputaminal",
    "intrasternal",
    "intrathecal",
    "intratumoral",
    "intrauterine",
    "intravenous",
    "intraventricular cardiac",
    "intravesical",
    "intravitreal",
    "iontophoresis",
    "line lock",
    "nasal",
    "obsolete-intraventricular",
    "obsolete-oromucosal other",
    "ocular",
    "oral",
    "oromucosal",
    "periarticular",
    "peribulbar ocular",
    "perineural",
    "peritumoral",
    "rectal",
    "regional perfusion",
    "route of administration not applicable",
    "subconjunctival",
    "subcutaneous",
    "sublabial",
    "sublingual",
    "submucosal",
    "submucosal rectal",
    "subretinal",
    "transdermal",
    "urethral",
    "vaginal",
    'unknown',
    'injection',
    'limb'
]
"""Allowed route of administrations, and DM+D sources will add to these
(`list` of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("PRAGMA foreign_keys=ON")
    except Exception:
        # Probably not SQLite but do not know how to tell!
        pass
    cursor.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_tables(session):
    """Ensure all the tables are created.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.
    """
    porm.Base.metadata.create_all(
        session.get_bind(), checkfirst=True
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_lookups(session):
    """Ensure all the static lookup tables are created.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    roa_map : `dict`
        A route of administration map. The keys are roa names and the values
        are roa primary keys.
    base_form_map : `dict`
        A base form map. The keys are base form names and the values are
        base form primary keys.
    base_type_map : `dict`
        A base type map. The keys are base type names and the values are
        base type primary keys.
    class_map : `dict`
        A class map. The keys are class names and the values are class primary
        keys.
    sub_class_map : `dict`
        A sub-class map. The keys are sub-class names and the values are
        sub-class primary keys.
    """
    roa_map = create_roa_lookups(session)
    base_form_map = create_base_form_lookups(session)
    base_type_map = create_base_type_lookups(session)
    class_map = create_class_lookups(session)
    sub_class_map = create_sub_class_lookups(session)
    return roa_map, base_form_map, base_type_map, class_map, \
        sub_class_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_roa_lookups(session):
    """Ensure all the route of administration lookups are in place.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    roa_map : `dict`
        A route of administration map. The keys are roa names and the values
        are roa primary keys.
    """
    for i in ROA:
        q = session.query(porm.RouteAdminLkp.route_admin_name).\
            filter(porm.RouteAdminLkp.route_admin_name == i)

        try:
            q.one()
        except NoResultFound:
            session.add(
                porm.RouteAdminLkp(route_admin_name=i)
            )
    session.commit()
    return dict(
        [(i.route_admin_name, i.route_admin_id)
         for i in session.query(porm.RouteAdminLkp)]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_base_form_lookups(session):
    """Ensure all the base form lookups are in place.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    base_form_map : `dict`
        A base form map. The keys are base form names and the values are
        base form primary keys.
    """
    for i in BASE_FORMS:
        q = session.query(porm.BaseFormLkp.base_form_name).\
            filter(porm.BaseFormLkp.base_form_name == i)

        try:
            q.one()
        except NoResultFound:
            session.add(
                porm.BaseFormLkp(base_form_name=i)
            )
    session.commit()
    return dict(
        [(i.base_form_name, i.base_form_id)
         for i in session.query(porm.BaseFormLkp)]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_base_type_lookups(session):
    """Ensure all the base type lookups are in place.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    base_type_map : `dict`
        A base type map. The keys are base type names and the values are
        base type primary keys.
    """
    for i in BASE_TYPES:
        q = session.query(porm.BaseTypeLkp.base_type_name).\
            filter(porm.BaseTypeLkp.base_type_name == i)

        try:
            q.one()
        except NoResultFound:
            session.add(
                porm.BaseTypeLkp(base_type_name=i)
            )
    session.commit()
    return dict(
        [(i.base_type_name, i.base_type_id)
         for i in session.query(porm.BaseTypeLkp)]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_class_lookups(session):
    """Ensure all the class lookups are in place.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    class_map : `dict`
        A class map. The keys are class names and the values are class primary
        keys.
    """
    for i in CLASS_TYPES:
        q = session.query(porm.ClassLkp.class_name).\
            filter(porm.ClassLkp.class_name == i)

        try:
            q.one()
        except NoResultFound:
            session.add(
                porm.ClassLkp(class_name=i)
            )
    session.commit()
    return dict(
        [(i.class_name, i.class_id)
         for i in session.query(porm.ClassLkp)]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_sub_class_lookups(session):
    """Ensure all the sub-class lookups are in place.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.

    Returns
    -------
    sub_class_map : `dict`
        A sub-class map. The keys are sub-class names and the values are
        sub-class primary keys.
    """
    cl_map = create_class_lookups(session)
    for cl, scl in SUB_CLASS_TYPES:
        try:
            cl_id = cl_map[cl]
        except KeyError as e:
            raise KeyError(f"unknown class type: {cl}") from e

        q = session.query(
            porm.SubClassLkp.sub_class_name
        ).filter(
            and_(
                porm.SubClassLkp.sub_class_name == scl,
                porm.SubClassLkp.class_id == cl_id
            )
        )

        try:
            q.one()
        except NoResultFound:
            session.add(
                porm.SubClassLkp(
                    sub_class_name=scl,
                    class_id = cl_id
                )
            )
    session.commit()
    return dict(
        [
            (
                (i.class_type.class_name,
                 i.sub_class_name),
                i.sub_class_id
            ) for i in session.query(porm.SubClassLkp)
        ]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_missing(pres_string):
    """Does the row have complete missing data?

    Parameters
    ----------
    pres_string : `str` or `NoneType`
        The product name (or prescription string ), if ``''`` or ``NoneType``
        then this is deemed missing.

    Returns
    -------
    is_miss : `bool`
        ``True`` if yes ``False`` if no.
    """
    return pres_string in ['', None]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def splice_spaces(splice_str, start, end):
    """Replace the text between ``start``, ``end`` with whitespace.

    Parameters
    ----------
    splice_str : `str`
        The string to splice spaces into.
    start : `int`
        The start position to splice from.
    end : `int`
        The end position to splice to.
    """
    return (
        splice_str[:start] +
        " " * (end - start) +
        splice_str[end:]
    )
