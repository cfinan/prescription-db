"""The main app control.
"""
# from entity_tagger import views
# from flask import Flask, url_for
# import os
# import pprint as pp


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def create_app(name, query, labels, columns, row_annotate=True,
#                col_annotate=True, text_annotate=True):
#     """The application factory.

#     Parameters
#     ----------
#     name : `str`
#         The name of the application and project.
#     query : `entity_tagger.query.QueryInterface`
#         The query interface that handles all database interaction.
#     labels : `list` or `entity_tagger.label.Label`
#         Label objects.
#     columns : `list` of `entity_tagger.columns.Column`
#         The columns that should be displayed.
#     row_annotate : `bool, optional, default: `True`
#         Do you want to enable row level annotations.
#     col_annotate : `bool, optional, default: `True`
#         Do you want to enable column level annotations.
#     text_annotate : `bool, optional, default: `True`
#         Do you want to enable text string annotations level annotations.

#     Returns
#     -------
#     app : `flask.Flask`
#         The initialised application.
#     """
#     app = Flask(
#         name,
#         template_folder=os.path.join(
#             os.path.dirname(__file__), 'templates'
#         ),
#         root_path=os.path.join(
#             os.path.dirname(__file__)
#         )
#     )
#     query.handler.open()
#     app.query = query
#     app.columns = columns
#     app.labels = labels
#     app.row_annotate = row_annotate
#     app.col_annotate = col_annotate
#     app.text_annotate = text_annotate

#     app.add_url_rule("/", 'root', views.main)
#     app.add_url_rule("/index", 'index', views.main)
#     app.add_url_rule("/setup", 'setup', views.setup)
#     app.add_url_rule("/data", 'data', views.data, ['GET', 'POST'])
#     # app.config['EXPLAIN_TEMPLATE_LOADING'] = True

#     @app.teardown_appcontext
#     def shutdown_session(exception=None):
#         app.query.handler.close()
#     return app
