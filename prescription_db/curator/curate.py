"""Initialise a development webserver to serve up pages for manual
curation tasks.
"""
from dmd import (
    __version__,
    __name__ as pkg_name,
    common as dcommon,
    orm as dorm
)
from dmd.pres_db import (
    orm as porm,
    common
)
from dmd.pres_db.curator import (
    app as capp,
    database
)
from snomed_ct import common as scommon
from sqlalchemy_config import config as cfg
from pyaddons import log
from sqlalchemy import and_, union, literal
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from sqlalchemy.exc import IntegrityError
from tqdm import tqdm
import hashlib
import argparse
import re
import os
import sys
# import pprint as pp


_PROG_NAME = 'dmd-pres-db-curate'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_dmd``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    config_path = cfg.get_config_file(arg=args.config)
    url = cfg.get_url_from_config(
        config_path, args.pres_db, config_prefix=dcommon._DEFAULT_PREFIX
    )

    try:
        # Add the dict file
        # Create a temp directory that we will blitz if we error out
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        '--pres-db',
        type=str,
        help="The prescription database connection config file section."
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_curator(pres_db_url, verbose=False, skip_file_check=False):
    """The API entry point for manual curation tasks in the prescription string
    database.

    Parameters
    ----------
    config : `entity_tagger.config.Config`
        An entity tagging config obejct.
    verbose : `bool`, optional, default: `False`
        Issue progress and log updates.
    skip_file_check : `bool`, optional, default: `False`
        Once initialise do not recheck the input file for changes. This
        currently has no effect.
    """
    # The call to getLogger will return the same logger object if the name is
    # the same
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    session_handler = database.SessionHandler(
        (init_config.data_url, 'data')
    )
    query_int = query.QueryInterface(session_handler, LocalTable)
    print(query_int.count)
    etapp = app.create_app(
        init_config.name, query_int, init_config.labels, init_config.columns,
        row_annotate=init_config.row_annotate,
        col_annotate=init_config.col_annotate,
        text_annotate=init_config.text_annotate
    )
    etapp.run(debug=True)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_initialised(config_file):
    """Check to see if the project has been initialised.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.

    Notes
    -----
    This checks for the presence of a file called ``.init.cfg`` in the root of
    the project directory.
    """
    try:
        open(config_file.init_file).close()
        return True
    except FileNotFoundError:
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file, verbose=False):
    """Initialise a file for tagging.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.
    verbose : `bool`, optional, default: `False`
        Report initialisation progress.
    """
    if config_file.infile is None:
        raise ValueError("no input file section is defined in the config file")

    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    logger.info("checking file MD5 hash...")
    md5hash = config_file.infile.get_md5()
    logger.info(f"MD5 hash: {md5hash}")
    # print(config_file)

    # There may be columns supplied in the config file, if so we use those to
    # load the data
    if len(config_file.columns) == 0:
        logger.info("determine data types...")
        # Determine the data types in the file.
        dtypes = config_file.infile.get_dtypes(verbose=verbose)
        for i in dtypes:
            config_file.add_column(i)
    else:
        dtypes = config_file.columns

    logger.info(f"there are {config_file.infile.nrows} rows")
    # print(config_file)
    # Dynamically create an ORM table class
    LocalTable = database.get_table(
        _LOCAL_TABLE_CLASS, _LOCAL_TABLE_NAME, [i.sqlalchemy for i in dtypes]
    )
    # engine = database.get_engine(config_file.data_url)
    # print(type(engine))
    # print(LocalTable)
    load_data(config_file, LocalTable, dtypes, verbose=verbose)
    config_file.write_config(config_file.init_file)
    return config_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_data(config_file, table, dtypes, verbose=False):
    """Initialise a file for tagging.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.
    table : `LocalTable`
        A dynamically created ``LocalTable`` orm class to load the data.
    dytpes : `list` of `entity_tagger.columns.Column`
        The column definitions that were used to built the ``LocalTable``. This
        may be a subset of the columns in the file.
    verbose : `bool`, optional, default: `False`
        Report initialisation progress.
    """
    engine = database.get_engine(config_file.data_url)
    database.Base.metadata.create_all(bind=engine)
    sessionmaker = database.get_local_sessionmaker(engine)
    session = sessionmaker()

    tqdm_kwargs = dict(
        desc="[info] loading rows",
        unit=" rows",
        disable=not verbose,
        total=config_file.infile.nrows
    )
    reader = config_file.infile.read()
    header = next(reader)

    for row in tqdm(reader, **tqdm_kwargs):
        # Map the header into a dict of columns
        row = dict([(i, row[idx]) for idx, i in enumerate(header)])
        load_row = {}
        # Use the column definitions that we want to load to extract
        # the specific data to load
        for i in dtypes:
            try:
                load_row[i.name] = i.dtype(row[i.name])
            except ValueError as e:
                if i.nullable is True:
                    load_row[i.name] = None
                else:
                    raise ValueError(f"column {i.name}: {e}") from e
        session.add(table(**load_row))
    session.commit()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
