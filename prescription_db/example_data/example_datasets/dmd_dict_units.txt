entry	class	sub_class	is_regex	standard_entry	base_form	roa_hint	base_type_hint
%v	unit	no_reverse	0				
%v/v	unit	no_reverse	0				
%v/w	unit	no_reverse	0				
%w	unit	no_reverse	0				
%w/v	unit	no_reverse	0				
%w/w	unit	no_reverse	0				
16 hours	unit	no_reverse	0				
24 hours	unit	no_reverse	0				
72 hours	unit	no_reverse	0				
Cell	unit	no_reverse	0				
GBq	unit	no_reverse	0				
GBq/ml	unit	no_reverse	0				
HEP	unit	no_reverse	0				
Kallikrein inactivator unit	unit	no_reverse	0				
Kallikrein inactivator unit/ml	unit	no_reverse	0				
MBq	unit	no_reverse	0				
MBq/ml	unit	no_reverse	0				
ML	unit	no_reverse	0				
SQ-Bet	unit	no_reverse	0				
SQ-T	unit	no_reverse	0				
SQ-U	unit	no_reverse	0				
SQ-U/ml	unit	no_reverse	0				
U	unit	no_reverse	0				
actuation	unit	no_reverse	0				
ampoule	unit	no_reverse	0				
ampoule-pair	unit	no_reverse	0				
application	unit	no_reverse	0				
applicator	unit	no_reverse	0				
bag	unit	no_reverse	0				
baguette	unit	no_reverse	0				
bandage	unit	no_reverse	0				
bar	unit	no_reverse	0				
billion vector genome	unit	no_reverse	0				
billion vector genome/ml	unit	no_reverse	0				
blister	unit	no_reverse	0				
bottle	unit	no_reverse	0				
c	unit	no_reverse	0				
can	unit	no_reverse	0				
capsule	unit	no_reverse	0				
carton	unit	no_reverse	0				
cartridge	unit	no_reverse	0				
catheter	unit	no_reverse	0				
cc	unit	no_reverse	0				
cell	unit	no_reverse	0				
cell/microlitre	unit	no_reverse	0				
cell/ml	unit	no_reverse	0				
cell/square cm	unit	no_reverse	0				
ch	unit	no_reverse	0				
cigarette	unit	no_reverse	0				
cm	unit	no_reverse	0				
component	unit	no_reverse	0				
container	unit	no_reverse	0				
copies	unit	no_reverse	0				
cup	unit	no_reverse	0				
cycle	unit	no_reverse	0				
cylinder	unit	no_reverse	0				
d	unit	reverse	0				
device	unit	no_reverse	0				
disc	unit	no_reverse	0				
dose	unit	no_reverse	0				
dose step	unit	no_reverse	0				
dressing	unit	no_reverse	0				
drop	unit	no_reverse	0				
dual dose sachet	unit	no_reverse	0				
elastomeric device	unit	no_reverse	0				
elastomeric device/ml	unit	no_reverse	0				
enema	unit	no_reverse	0				
film	unit	no_reverse	0				
g	unit	no_reverse	0				
g/actuation	unit	no_reverse	0				
g/application	unit	no_reverse	0				
g/dose	unit	no_reverse	0				
g/l	unit	no_reverse	0				
g/ml	unit	no_reverse	0				
gauge	unit	reverse	0				
generator	unit	no_reverse	0				
genome	unit	no_reverse	0				
glove	unit	no_reverse	0				
gm	unit	no_reverse	0				
gms	unit	no_reverse	0				
gram	unit	no_reverse	0				
gram/gram	unit	no_reverse	0				
h	unit	no_reverse	0				
hour	unit	no_reverse	0				
hours	unit	no_reverse	0				
insert	unit	no_reverse	0				
iu	unit	no_reverse	0				
iu/g	unit	no_reverse	0				
iu/mg	unit	no_reverse	0				
iu/ml	unit	no_reverse	0				
kBq	unit	no_reverse	0				
kBq/ml	unit	no_reverse	0				
kcal	unit	no_reverse	0				
kg	unit	no_reverse	0				
kg/l	unit	no_reverse	0				
kit	unit	no_reverse	0				
l	unit	no_reverse	0				
lancet	unit	no_reverse	0				
larva	unit	no_reverse	0				
leech	unit	no_reverse	0				
litre	unit	no_reverse	0				
litres	unit	no_reverse	0				
loaf	unit	no_reverse	0				
lozenge	unit	no_reverse	0				
m	unit	no_reverse	0				
mcg	unit	no_reverse	0				
mega u	unit	no_reverse	0				
mega u/ml	unit	no_reverse	0				
mega unit	unit	no_reverse	0				
mega unit/ml	unit	no_reverse	0				
mg	unit	no_reverse	0				
mgs	unit	no_reverse	0				
mgm	unit	no_reverse	0				
mg/16 hours	unit	no_reverse	0				
mg/24 hours	unit	no_reverse	0				
mg/72 hours	unit	no_reverse	0				
mg/actuation	unit	no_reverse	0				
mg/application	unit	no_reverse	0				
mg/dose	unit	no_reverse	0				
mg/g	unit	no_reverse	0				
mg/kg	unit	no_reverse	0				
mg/l	unit	no_reverse	0				
mg/mg	unit	no_reverse	0				
mg/ml	unit	no_reverse	0				
mg/square cm	unit	no_reverse	0				
microgram	unit	no_reverse	0				
microgram/24 hours	unit	no_reverse	0				
microgram/72 hours	unit	no_reverse	0				
microgram/actuation	unit	no_reverse	0				
microgram/dose	unit	no_reverse	0				
microgram/g	unit	no_reverse	0				
microgram/hour	unit	no_reverse	0				
microgram/ml	unit	no_reverse	0				
micrograms	unit	no_reverse	0				
micrograms/square cm	unit	no_reverse	0				
microlitre	unit	no_reverse	0				
microlitre/g	unit	no_reverse	0				
microlitre/ml	unit	no_reverse	0				
micromol	unit	no_reverse	0				
micromol/ml	unit	no_reverse	0				
million	unit	no_reverse	0				
million plaque forming units	unit	no_reverse	0				
million plaque forming units/ml	unit	no_reverse	0				
million units/ml	unit	no_reverse	0				
ml	unit	no_reverse	0				
ml/gram	unit	no_reverse	0				
ml/kg	unit	no_reverse	0				
ml/l	unit	no_reverse	0				
ml/ml	unit	no_reverse	0				
mls	unit	no_reverse	0				
mm	unit	no_reverse	0				
mmhg	unit	no_reverse	0				
mmol	unit	no_reverse	0				
mmol/litre	unit	no_reverse	0				
mmol/ml	unit	no_reverse	0				
mol	unit	no_reverse	0				
mol/l	unit	no_reverse	0				
molar	unit	no_reverse	0				
month supply	unit	no_reverse	0				
multipack	unit	no_reverse	0				
nanogram	unit	no_reverse	0				
nanogram/ml	unit	no_reverse	0				
nanolitre	unit	no_reverse	0				
nanolitre/ml	unit	no_reverse	0				
nebule	unit	no_reverse	0				
needle	unit	no_reverse	0				
no value	unit	no_reverse	0				
obsolete-mM	unit	no_reverse	0				
pack	unit	no_reverse	0				
pad	unit	no_reverse	0				
pastille	unit	no_reverse	0				
patch	unit	no_reverse	0				
pessary	unit	no_reverse	0				
piece	unit	no_reverse	0				
pillule	unit	no_reverse	0				
pizza base	unit	no_reverse	0				
plaque forming units	unit	no_reverse	0				
plaque forming units/ml	unit	no_reverse	0				
plaster	unit	no_reverse	0				
ply	unit	no_reverse	0				
pot	unit	no_reverse	0				
pouch	unit	no_reverse	0				
pouches	unit	no_reverse	0				
ppm	unit	no_reverse	0				
pre-filled disposable injection	unit	no_reverse	0				
roll	unit	no_reverse	0				
sachet	unit	no_reverse	0				
scoop	unit	no_reverse	0				
spoonful	unit	no_reverse	0				
square	unit	no_reverse	0				
square cm	unit	no_reverse	0				
stocking	unit	no_reverse	0				
straw	unit	no_reverse	0				
strip	unit	no_reverse	0				
suppository	unit	no_reverse	0				
suture	unit	no_reverse	0				
swab	unit	no_reverse	0				
syringe	unit	no_reverse	0				
system	unit	no_reverse	0				
tablet	unit	no_reverse	0				
tera vector genome	unit	no_reverse	0				
tera vector genome/ml	unit	no_reverse	0				
teragenome copies	unit	no_reverse	0				
teragenome copies/ml	unit	no_reverse	0				
truss	unit	no_reverse	0				
tube	unit	no_reverse	0				
tuberculin unit	unit	no_reverse	0				
tuberculin units	unit	no_reverse	0				
tuberculin units/ML	unit	no_reverse	0				
u	unit	no_reverse	0				
unit	unit	no_reverse	0				
unit dose	unit	no_reverse	0				
unit/actuation	unit	no_reverse	0				
unit/dose	unit	no_reverse	0				
unit/drop	unit	no_reverse	0				
unit/gram	unit	no_reverse	0				
unit/mg	unit	no_reverse	0				
unit/ml	unit	no_reverse	0				
unit/square cm	unit	no_reverse	0				
units	unit	no_reverse	0				
v	unit	no_reverse	0				
vector genome	unit	no_reverse	0				
vector genome/ml	unit	no_reverse	0				
vial	unit	no_reverse	0				
w	unit	no_reverse	0				
week supply	unit	no_reverse	0				
y	unit	no_reverse	0				
