"""Provides centralised access to example data sets that can be used in tests
and also in example code and/or jupyter notebooks.

The `dmd.example_data.examples` module is very simple, this is
not really designed for editing via end users but they should call the three
public functions, `dmd.example_data.examples.get_data()`,
`dmd.example_data.examples.help()` and
`dmd.example_data.examples.list_datasets()`.

Notes
-----
Data can be "added" either through functions that generate the data on the fly
or via functions that load the data from a static file located in the
``example_data`` directory. The data files being added  should be as small as
possible (i.e. kilobyte/megabyte range). The dataset functions should be
decorated with the ``@dataset`` decorator, so the example module knows about
them. If the function is loading a dataset from a file in the package, it
should look for the path in ``_ROOT_DATASETS_DIR``.

Examples
--------
Registering a function as a dataset providing function:

>>> @dataset
>>> def dummy_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that returns a small list.
>>>
>>>     Returns
>>>     -------
>>>     data : `list`
>>>         A list of length 3 with ``['A', 'B', 'C']``
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions.
>>>     \"\"\"
>>>     return ['A', 'B', 'C']

The dataset can then be used as follows:

>>> from dmd.example_data import examples
>>> examples.get_data('dummy_data')
>>> ['A', 'B', 'C']

A dataset function that loads a dataset from file, these functions should load
 from the ``_ROOT_DATASETS_DIR``:

>>> @dataset
>>> def dummy_load_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that loads a string from a file.
>>>
>>>     Returns
>>>     -------
>>>     str_data : `str`
>>>         A string of data loaded from an example data file.
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions. The path to this dataset is
>>>     built from ``_ROOT_DATASETS_DIR``.
>>>     \"\"\"
>>>     load_path = os.path.join(_ROOT_DATASETS_DIR, "string_data.txt")
>>>     with open(load_path) as data_file:
>>>         return data_file.read().strip()

The dataset can then be used as follows:

>>> from dmd.example_data import examples
>>> examples.get_data('dummy_load_data')
>>> 'an example data string'
"""
from collections import namedtuple
import os
import re
import csv


_INTERNAL_DELIMITER = "|"
"""The internal delimiter of the input files (`str`)
"""

# The name of the example datasets directory
_EXAMPLE_DATASETS = "example_datasets"
"""The example dataset directory name (`str`)
"""

_ROOT_DATASETS_DIR = os.path.join(os.path.dirname(__file__), _EXAMPLE_DATASETS)
"""The root path to the dataset files that are available (`str`)
"""

_DATASETS = dict()
"""This will hold the registered dataset functions (`dict`)
"""


# 1. token
# 2. class
# 3. sub_class
# 4. is_regex
# 5. standard_token
# 6. base_form
# 7. roa_hint
# 8. base_type_hint
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DictEntry(object):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, token=None, token_class=None, token_sub_class=None,
                 is_regex=False, standard_token=None, base_form=None,
                 roa_hint=None, base_type_hint=None):
        self._token_regex = None
        self.token = token.strip()
        self.token_lower = self.token.lower()
        self.token_class = token_class.strip()
        self.token_sub_class = missing(token_sub_class.strip())
        self.is_regex = parse_bool(is_regex.strip())
        self.standard_token = missing(standard_token.strip())
        self.base_form = split(missing(base_form))
        self.base_type = split(missing(base_type_hint))
        self.roa = split(missing(roa_hint))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def token_regex(self):
        if self._token_regex is None:
            if self.is_regex is True:
                self._token_regex = regexp(self.token)
            else:
                self._token_regex = regexp(f"\b{re.escape(self.token)}\b")
        return self._token_regex


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dataset(func):
    """Register a dataset generating function. This function should be used as
    a decorator.

    Parameters
    ----------
    func : `function`
        The function to register as a dataset. It is registered as under the
        function name.

    Returns
    -------
    func : `function`
        The function that has been registered.

    Raises
    ------
    KeyError
        If a function of the same name has already been registered.

    Notes
    -----
    The dataset function should accept ``*args`` and ``**kwargs`` and should be
    decorated with the ``@dataset`` decorator.

    Examples
    --------
    Create a dataset function that returns a dictionary.

    >>> @dataset
    >>> def get_dict(*args, **kwargs):
    >>>     \"\"\"A dictionary to test or use as an example.
    >>>
    >>>     Returns
    >>>     -------
    >>>     test_dict : `dict`
    >>>         A small dictionary of string keys and numeric values
    >>>     \"\"\"
    >>>     return {'A': 1, 'B': 2, 'C': 3}
    >>>

    The dataset can then be used as follows:

    >>> from dmd.example_data import examples
    >>> examples.get_data('get_dict')
    >>> {'A': 1, 'B': 2, 'C': 3}

    """
    try:
        _DATASETS[func.__name__]
        raise KeyError("function already registered")
    except KeyError:
        pass

    _DATASETS[func.__name__] = func
    return func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_data(name, *args, **kwargs):
    """Central point to get the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a registered
        dataset function.
    *args
        Arguments to the data generating functions
    **kwargs
        Keyword arguments to the data generating functions

    Returns
    -------
    dataset : `Any`
        The requested datasets
    """
    try:
        return _DATASETS[name](*args, **kwargs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def list_datasets():
    """List all the registered datasets.

    Returns
    -------
    datasets : `list` of `tuple`
        The registered datasets. Element [0] for each tuple is the dataset name
        and element [1] is a short description captured from the docstring.
    """
    datasets = []
    for d in _DATASETS.keys():
        desc = re.sub(
            r'(Parameters|Returns).*$', '', _DATASETS[d].__doc__.replace(
                '\n', ' '
            )
        ).strip()
        datasets.append((d, desc))
    return datasets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def help(name):
    """Central point to get help for the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a unique key in the
        DATASETS module level dictionary.

    Returns
    -------
    help : `str`
        The docstring for the function.
    """
    docs = ["Dataset: {0}\n{1}\n\n".format(name, "-" * (len(name) + 9))]
    try:
        docs.extend(
            ["{0}\n".format(re.sub(r"^\s{4}", "", i))
             for i in _DATASETS[name].__doc__.split("\n")]
        )
        return "".join(docs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def drug_dict_attrs(*args, **kwargs):
    """The loading attributes for the drug dictionary file.

    Returns
    -------
    load_attrs : `dict`
        The keys of the ``dict`` are ``header`` - the expected header for the
        drug dictionary file, ``delimiter`` - the delimiter of the drug
        dictionary file and ``internal_delimiter`` - the delimiter of any
        flattened nested data.
    """
    return dict(
        delimiter="\t",
        fields=[('match', dummy), ('source', split), ('type', dummy),
                ('standard_name', dummy)],
        internal_delimiter=_INTERNAL_DELIMITER
    )


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @dataset
# def unit_attrs(*args, **kwargs):
#     """The loading attributes for the unit dictionary file.

#     Returns
#     -------
#     load_attrs : `dict`
#         The keys of the ``dict`` are ``fields`` - the expected header for the
#         drug dictionary file, ``delimiter`` - the delimiter of the drug
#         dictionary file.
#     """
#     return dict(
#         delimiter="\t",
#         fields=[('match', dummy), ('has_reverse', parse_bool)],
#     )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(x):
    """Just strip the input.

    Parameters
    ----------
    x : `str`
        input value to be stripped.
    """
    if missing(x) is None:
        return False
    return bool(int(x))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dummy(x):
    """Just strip the input.

    Parameters
    ----------
    x : `str`
        input value to be stripped.
    """
    return x.strip()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def split(x):
    """Parse the input into a split list.

    Parameters
    ----------
    x : `str`
        input value to be parsed, potentially containing a ``|``.
    """
    try:
        return set([i.strip() for i in x.split(_INTERNAL_DELIMITER)])
    except AttributeError:
        if x is not None:
            raise
        return set()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def regexp(x):
    """Parse the input into a regular expression.

    Parameters
    ----------
    x : `str`
        input value to be parsed
    """
    return re.compile(x.strip(), re.IGNORECASE)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def missing(x):
    """Make a value None f it is a missing value '' or '.'.

    Parameters
    ----------
    x : `str`
        input value to be checked
    """
    if x in ['', '.']:
        return None
    return x


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _load_dict_file(infile, *args, **kwargs):
    """Generic loader for a dictionary file.

    Parameters
    ----------
    infile : `str`
        The path to the input dictionary file.

    Returns
    -------
    contents : `dict`
        The contents of the file.

    Notes
    -----
    A dictionary file is uncompressed, tab delimited and has the following
    format:

    1. token
    2. class
    3. sub_class
    4. is_regex
    5. standard_token
    6. base_form
    7. roa_hint
    8. base_type_hint

    """
    contents = []
    with open(infile, 'rt') as intab:
        reader = csv.DictReader(intab, delimiter="\t")
        for row in reader:
            row['token'] = row['token'].strip()
            if row['token'].startswith('#'):
                continue
            row['token_class'] = row['class']
            row['token_sub_class'] = row['sub_class']
            del(row['class'])
            del(row['sub_class'])
            contents.append(DictEntry(**row))
    return contents


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def drugs(*args, **kwargs):
    """Read in the dictionary of drug names.

    Returns
    -------
    drug_dict : `dict`
        The current drug name dictionary where the keys are drug names
        (lowercase) and the values are a namedtuple of ``Drug``.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_drugs.txt")
    return _load_dict_file(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def units(*args, **kwargs):
    """Read in the unit set.

    Returns
    -------
    units : `set`
        The unit definitions.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_units.txt")
    return _load_dict_file(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def ingredients(*args, **kwargs):
    """Read in the ingredient set.

    Returns
    -------
    ingredients : `set`
        The ingredient set. Ingredients are from entries in the DM+D ingredient
        table, that do not overlap drugs.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_ingredients.txt")
    return _load_dict_file(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def forms(*args, **kwargs):
    """Read in the dose form definition regular expressions.

    Returns
    -------
    forms : `list` of `re.Pattern`
        The dose form definitions.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_form.txt")
    return _load_dict_file(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def salts(*args, **kwargs):
    """Read in the dictionary of drug salt names/abbreviations.

    Returns
    -------
    salts : `list` of `tuple`
        The salt regexp is at ``[0]`` and the normalised form is at ``[1]``.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_salts.txt")
    return _load_dict_file(load_path)
    # load_attrs = get_data('drug_dict_attrs')
    # salts = list()
    # with open(load_path, 'rt') as infile:
    #     reader = csv.DictReader(infile, delimiter=load_attrs['delimiter'])

    #     fields = [
    #         ('name', regexp),
    #         ('norm', dummy)
    #     ]
    #     for row in reader:
    #         salt_row = []
    #         for colname, parser in fields:
    #             try:
    #                 salt_row.append(parser(missing(row[colname])))
    #             except AttributeError:
    #                 salt_row.append(None)

    #         salts.append(tuple(salt_row))
    # return salts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def person_type(*args, **kwargs):
    """Read in the dictionary of person types.

    Returns
    -------
    person_types : `list` of `tuple`
        The person type string is at ``[0]`` and the normalised form is at
        ``[1]``.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_person_types.txt")
    return _load_dict_file(load_path)
    # load_attrs = get_data('drug_dict_attrs')
    # person_types = list()
    # with open(load_path, 'rt') as infile:
    #     reader = csv.DictReader(infile, delimiter=load_attrs['delimiter'])

    #     fields = [
    #         ('person_type', dummy),
    #         ('norm', dummy)
    #     ]
    #     for row in reader:
    #         person_type_row = []
    #         for colname, parser in fields:
    #             try:
    #                 person_type_row.append(parser(missing(row[colname])))
    #             except AttributeError:
    #                 person_type_row.append(None)

    #         person_types.append(tuple(person_type_row))
    # return person_types


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def qualitative_units(*args, **kwargs):
    """Read in the dictionary of qualitative units.

    Returns
    -------
    qualitative_units : `list` of `re.Pattern`
        The qualitative unit regexps.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_qual_units.txt")
    return _load_dict_file(load_path)
    # return _load_regexp(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def suppliers(*args, **kwargs):
    """Read in the supplier set.

    Returns
    -------
    suppliers : `set`
        The supplier set (company names).
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_suppliers.txt")
    return _load_dict_file(load_path)
    # return _load_column_into_set(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def homeopathic(*args, **kwargs):
    """Read in the dose form definition regular expressions.

    Returns
    -------
    forms : `list` of `re.Pattern`
        The dose form definitions.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_dict_hom.txt")
    return _load_dict_file(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def static_mappings(*args, **kwargs):
    """Read in the dictionary of static mapping transformations.

    Returns
    -------
    static_map : `dict`
        The keys are the original expected text and the values are the
        transformed text.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "dmd_statics.txt")
    load_attrs = get_data('drug_dict_attrs')
    static_map = dict()
    with open(load_path, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter=load_attrs['delimiter'])

        for row in reader:
            static_map[dummy(row['source'])] = dummy(row['mapping'])
    return static_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def drug_excludes(*args, **kwargs):
    """Read in the set of drug names to exclude.

    Returns
    -------
    drug_excludes : `set`
        The drug names that we do not want in the drug dictionary.
    """
    load_path = os.path.join(_ROOT_DATASETS_DIR, "drug_excludes.txt")
    return _load_column_into_set(load_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _load_column_into_set(infile, expected_header=None):
    """Helper function that will load single column files (one entry per line)
    into sets.

    Parameters
    ----------
    infile : `str`
        The file to read in.
    expected_header : `str` or `NoneType`, optional, default: `NoneType`
        Any expected header to check against what is in the file. If
        ``NoneType`` then no checks will take place.

    Returns
    -------
    indata : `set`
        A set of data contained in ``infile``.

    Notes
    -----
    All input files are expected to have a header row.
    """
    inset = set([])
    with open(infile) as infile:
        # Remove the header
        header = next(infile).strip()

        if expected_header is not None and header != expected_header:
            raise ValueError(
                "wrong header: expected '{0}' got '{1} ".format(
                    expected_header, header
                )
            )

        for row in infile:
            name = row.strip()
            if len(name) > 0:
                inset.add(row.strip())
    return inset
