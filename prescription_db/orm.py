"""ORM classes for the prescription database.
"""
from prescription_db import common
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    BigInteger,
    ForeignKey,
    Date,
    Boolean,
    Text,
    Sequence
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataSource(Base):
    """A representation of the ``data_source`` table.

    Parameters
    ----------
    data_source_id : `int`
        Auto-incremented index.
    data_source_name : `str`
        The name for the data source (length 255).
    data_source_pretty : `str`
        The pretty name for the data source, i.e. one that can be used for
        display items (length 255).
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship link to the ``source_version`` table.

    Notes
    -----
    A data source is a single source of data that can have one or more
    versions.
    """
    __tablename__ = 'data_source'
    data_source_id = Column(
        Integer, Sequence('data_source_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    data_source_name = Column(
        String(255), nullable=False,
        doc="The name for the data source."
    )
    data_source_pretty = Column(
        String(255), nullable=False,
        doc="The pretty name for the data source, i.e. one that can be used "
        "for display items."
    )

    # -------------------------------------------------------------------------#
    source_version = relationship(
        "SourceVersion",
        back_populates='data_source',
        doc="Relationship link to the ``source_version`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SourceVersion(Base):
    """A representation of the ``source_version`` table. A source version is a
    specific version of a data source.

    Parameters
    ----------
    source_version_id : `int`
        Auto-incremented index.
    data_source_id : `int`
        Foreign key link to the ``data_source`` table. Foreign key to
        ``data_source.data_source_id``.
    version_name : `str`
        The version name/ID for the data source (length 255).
    data_source : `prescription_db.orm.DataSource`, optional, \
    default: `NoneType`
        Relationship link to the data_source table.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship link to the ``pres_string`` table.
    pres_string_code : `prescription_db.orm.PresStringCode`, optional, \
    default: `NoneType`
        Relationship link to the ``pres_string_code`` table.
    pres_string_count : `prescription_db.orm.PresStringCount`, optional, \
    default: `NoneType`
        Relationship link to the ``pres_str_count`` table.
    dmd_external_code : `prescription_db.orm.DmdExternalCode`, optional, \
    default: `NoneType`
        Relationship link to the ``dmd_external_code`` table.
    pres_string_occurs : `prescription_db.orm.PresStringOccurs`, optional, \
    default: `NoneType`
        Relationship link to the ``pres_string_occurs`` table.
    pres_string_meta : `prescription_db.orm.PresStringMetadata`, optional, \
    default: `NoneType`
        Relationship link to the ``pres_string_metadata`` table.
    """
    __tablename__ = 'source_version'

    source_version_id = Column(
        Integer, Sequence('sv_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index"
    )
    data_source_id = Column(
        Integer, ForeignKey("data_source.data_source_id"),
        nullable=False,
        doc="Foreign key link to the ``data_source`` table."
    )
    version_name = Column(
        String(255), nullable=False,
        doc="The version name/ID for the data source."
    )

    # -------------------------------------------------------------------------~
    data_source = relationship(
        "DataSource",
        back_populates='source_version',
        doc="Relationship link to the data_source table."
    )
    pres_str = relationship(
        "PresString",
        back_populates='source_version',
        doc="Relationship link to the ``pres_string`` table."
    )
    pres_str_code = relationship(
        "PresStringCode",
        back_populates='source_version',
        doc="Relationship link to the ``pres_string_code`` table."
    )
    pres_str_count = relationship(
        "PresStringCount",
        back_populates='source_version',
        doc="Relationship link to the ``pres_str_count`` table."
    )
    pres_str_external_code = relationship(
        "DmdExternalCode",
        back_populates='source_version',
        doc="Relationship link to the ``dmd_external_code`` table."
    )
    pres_str_occurs = relationship(
        "PresStringOccurs",
        back_populates='source_version',
        doc="Relationship link to the ``pres_string_occurs`` table."
    )
    pres_str_meta = relationship(
        "PresStringMetadata",
        back_populates='source_version',
        doc="Relationship link to the ``pres_string_metadata`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresString(Base):
    """A representation of the ``pres_string`` table. This holds all of the raw
    prescription strings.

    Parameters
    ----------
    pres_str_id : `int`
        Auto-incremented index.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    pres_str_name : `str`
        The text for the prescription string. This is indexed (length 700).
    is_dmd : `bool`
        Is the prescription string directly from the DM+D.
    is_amp : `bool`
        Is the prescription string an AMP.
    is_truncated : `bool`
        Are thee indicators that the prescription string has been truncated?.
    pres_str_to_dmd : `prescription_db.orm.PresStringDmd`, optional, \
    default: `NoneType`
        Relationship with ``pres_str_to_dmd``.
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    pres_string_code : `prescription_db.orm.PresStringCode`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_code``.
    pres_string_occurs : `prescription_db.orm.PresStringOccurs`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_occurs``.
    pres_string_count : `prescription_db.orm.PresStringCount`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_count``.
    pres_string_tokens : `prescription_db.orm.PresStringTokens`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_tokens``.
    dmd_external_code : `prescription_db.orm.DmdExternalCode`, optional, \
    default: `NoneType`
        Relationship with ``dmd_external_code``.
    pres_string_snomed : `prescription_db.orm.PresStringSnomed`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_snomed``.
    pres_string_meta : `prescription_db.orm.PresStringMetadata`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_meta``.
    """
    __tablename__ = 'pres_string'

    pres_str_id = Column(
        Integer, Sequence('pres_string_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index"
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    pres_str_name = Column(
        String(700, collation='NOCASE'), nullable=False,
        index=True,
        doc="The text for the prescription string."
    )
    is_dmd = Column(
        Boolean, nullable=False, default=False,
        doc="Is the prescription string directly from the DM+D."
    )
    is_amp = Column(
        Boolean, nullable=False, default=False,
        doc="Is the prescription string an AMP."
    )
    is_truncated = Column(
        Boolean, nullable=False, default=False,
        doc="Are thee indicators that the prescription string has been"
        " truncated?"
    )

    # -------------------------------------------------------------------------~
    pres_str_map = relationship(
        "PresStringDmd",
        back_populates='pres_str'
    )
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str'
    )
    pres_str_code = relationship(
        "PresStringCode",
        back_populates='pres_str'
    )
    pres_str_occurs = relationship(
        "PresStringOccurs",
        back_populates='pres_str'
    )
    pres_str_count = relationship(
        "PresStringCount",
        back_populates='pres_str'
    )
    pres_str_tokens = relationship(
        "PresStringTokens",
        back_populates='pres_string'
    )
    pres_str_external_code = relationship(
        "DmdExternalCode",
        back_populates='pres_str'
    )
    pres_str_snomed = relationship(
        "PresStringSnomed",
        back_populates='pres_str'
    )
    pres_str_meta = relationship(
        "PresStringMetadata",
        back_populates='pres_str'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringSnomed(Base):
    """A representation of the ``pres_string_snomed`` table. This holds all the
    SnomedCT un-normalised relationship annotations for the prescriptions.

    Parameters
    ----------
    pres_str_snomed_id : `int`
        Auto-incremented index.
    pres_str_id : `int`
        Foreign key link to the prescription string table. Foreign key to
        ``pres_string.pres_str_id``.
    rel_distance : `int`
        The relationship distance.
    concept_id : `int`
        The snomed concept ID for the relationship component.
    concept_term : `str`
        The snomed concept term for the relationship component (length 1000).
    type_id : `int`
        The snomed concept ID for the relationship type.
    type_term : `str`
        The snomed concept term for the relationship type (length 1000).
    char_type_id : `int`
        The snomed concept ID for the relationship characteristic type.
    char_type_term : `str`
        The snomed concept term for the relationship characteristic type
        (length 1000).
    modifier_id : `int`
        The snomed concept ID for the modifier type.
    modifier_term : `str`
        The snomed concept term for the modifier type (length 1000).
    code_match : `bool`
        Has the relationship been matched on snomed concept code and not
        string.
    pres_string : `prescription_db.orm.PresString`, optional,\
    default: `NoneType`
        Relationship with ``pres_string``.
    """
    __tablename__ = 'pres_string_snomed'

    pres_str_snomed_id = Column(
        Integer, Sequence('pss_seq'), nullable=False, primary_key=True,
        autoincrement=True,
        doc="Auto-incremented index."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="Foreign key link to the prescription string table."
    )
    rel_distance = Column(
        Integer, nullable=False,
        doc="The relationship distance."
    )
    concept_id = Column(
        BigInteger, nullable=False,
        doc="The snomed concept ID for the relationship component."
    )
    concept_term = Column(
        String(1000, collation='NOCASE'), nullable=False,
        doc="The snomed concept term for the relationship component."
    )
    type_id = Column(
        BigInteger, nullable=False,
        doc="The snomed concept ID for the relationship type."
    )
    type_term = Column(
        String(1000, collation='NOCASE'), nullable=False,
        doc="The snomed concept term for the relationship type."
    )
    char_type_id = Column(
        BigInteger, nullable=False,
        doc="The snomed concept ID for the relationship "
        "characteristic type."
    )
    char_type_term = Column(
        String(1000, collation='NOCASE'), nullable=False,
        doc="The snomed concept term for the relationship "
        "characteristic type."
    )
    modifier_id = Column(
        BigInteger, nullable=False,
        doc="The snomed concept ID for the modifier type."
    )
    modifier_term = Column(
        String(1000, collation='NOCASE'), nullable=False,
        doc="The snomed concept term for the modifier type."
    )
    code_match = Column(
        Boolean, nullable=False, default=False,
        doc="Has the relationship been matched on snomed concept code "
        "and not string."
    )

    # -------------------------------------------------------------------------~
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_snomed'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringOccurs(Base):
    """Table holding a mapping between the prescription strings and the data
    source they occur in.

    Parameters
    ----------
    pres_str_occurs_id : `int`
        Auto-incremented index.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    pres_str_id : `int`
        Foreign key link to the ``pres_string`` table. Foreign key to
        ``pres_string.pres_str_id``.
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.

    Notes
    -----
    This differs from the source version in the ``pres_string`` table as this
    will have all the source versions that the string has been observed in not
    just the first one it was imported from.
    """
    __tablename__ = 'pres_string_occurs'

    pres_str_occurs_id = Column(
        Integer, Sequence('pres_string_occurs_seq'), nullable=False,
        primary_key=True,
        doc="Auto-incremented index"
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="Foreign key link to the ``pres_string`` table."
    )

    # -------------------------------------------------------------------------~
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str_occurs'
    )
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_occurs'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringCode(Base):
    """A representation of the ``pres_string_code`` table. This maps
    prescription strings back to associated DM+D (snomed) codes.

    Parameters
    ----------
    pres_str_code_id : `int`
        Auto-incremented index.
    pres_str_id : `int`
        Foreign key link to the ``pres_string`` table. Foreign key to
        ``pres_string.pres_str_id``.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    pres_str_code : `int`
        The DM+D code associated with the prescription string. This is indexed.
    is_dmd : `bool`
        Is the prescription string code directly from the DM+D as opposed to
        being imported from an EHR file.
    is_verhoeff_valid : `bool`
        Is the prescription string code valid according to the Verhoeff
        function.
    check_digit : `int`
        The check digit of the SnomedCT code.
    partition_id : `str`
        The partition ID of the SnomedCT code (length 7).
    has_namespace : `bool`
        Based on the partition ID does the SnomedCT code have a namespace ID.
    code_type : `str`
        Based on the partition ID what is the type of code (length 12).
    is_partition_valid : `bool`
        Is the partition ID valid?.
    namespace_id : `int`
        The namespace ID of the code.
    is_namespace_valid : `bool`
        Is the namespace ID valid?.
    organisation : `str`
        The organisation that the SnomedCT code is defined from (based on the
        namespace ID) (length 255).
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.
    """
    __tablename__ = 'pres_string_code'

    pres_str_code_id = Column(
        Integer, Sequence('psc_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index"
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="Foreign key link to the ``pres_string`` table."
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    pres_str_code = Column(
        BigInteger, nullable=False, index=True,
        doc="The DM+D code associated with the prescription string."
    )
    is_dmd = Column(
        Boolean, nullable=False, default=False,
        doc="Is the prescription string code directly from the DM+D as opposed"
        " to being imported from an EHR file."
    )
    is_verhoeff_valid = Column(
        Boolean, nullable=False, default=False,
        doc="Is the prescription string code valid according to the "
        "Verhoeff function."
    )
    check_digit = Column(
        Integer, nullable=False, default=-1,
        doc="The check digit of the SnomedCT code."
    )
    partition_id = Column(
        String(7), nullable=False, default="UNKNOWN",
        doc="The partition ID of the SnomedCT code."
    )
    has_namespace = Column(
        Boolean, nullable=False, default=False,
        doc="Based on the partition ID does the SnomedCT code have a "
        "namespace ID."
    )
    code_type = Column(
        String(12), nullable=False, default="UNKNOWN",
        doc="Based on the partition ID what is the type of code."
    )
    is_partition_valid = Column(
        Boolean, nullable=False, default=False,
        doc="Is the partition ID valid?"
    )
    namespace_id = Column(
        Integer, nullable=False, default=-1,
        doc="The namespace ID of the code."
    )
    is_namespace_valid = Column(
        Boolean, nullable=False, default=False,
        doc="Is the namespace ID valid?"
    )
    organisation = Column(
        String(255), nullable=False, default="UNKNOWN",
        doc="The organisation that the SnomedCT code is defined from (based on"
        " the namespace ID)."
    )

    # -------------------------------------------------------------------------#
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str_code'
    )
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_code'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringCount(Base):
    """A representation of the ``pres_string_count`` table.

    Parameters
    ----------
    pres_str_count_id : `int`
        Auto-incremented index.
    pres_str_id : `int`
        Foreign key link to the ``pres_string`` table. Foreign key to
        ``pres_string.pres_str_id``.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    pres_str_count : `int`
        A count associated with the ``pres_string`` in the EHR. This is
        indexed.
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.

    Notes
    -----
    This contains summary level counts for the number of times the prescription
    string is observed in the EHR records (``source_version_id``).
    """
    __tablename__ = 'pres_string_count'

    pres_str_count_id = Column(
        Integer, Sequence('psco_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index"
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="Foreign key link to the ``pres_string`` table."
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    pres_str_count = Column(
        BigInteger, nullable=False, index=True,
        doc="A count associated with the ``pres_string`` in the EHR."
    )

    # -------------------------------------------------------------------------#
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str_count'
    )
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_count'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringMetadataType(Base):
    """Counts of prescription strings observed in EHR records.

    Parameters
    ----------
    pres_str_meta_type_id : `int`
        Auto-incremented index.
    pres_str_meta_type_name : `str`
        The name of the prescription string metadata type (length 255).
    pres_string_meta : `prescription_db.orm.PresStringMetadata`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_meta``.
    """
    __tablename__ = 'pres_string_meta_type'

    pres_str_meta_type_id = Column(
        Integer, Sequence('pres_string_meta_type_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented index"
    )
    pres_str_meta_type_name = Column(
        String(255), nullable=False,
        doc="The name of the prescription string metadata type."
    )

    # -------------------------------------------------------------------------#
    pres_str_meta = relationship(
        "PresStringMetadata",
        back_populates='pres_str_type'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringMetadata(Base):
    """A representation of the ``pres_string_meta`` table. This contains any
    additional metadata that has been imported from a prescription string file.

    Parameters
    ----------
    pres_str_meta_id : `int`
        Auto-incremented index.
    pres_str_id : `int`
        Foreign key link to the ``pres_string`` table. Foreign key to
        ``pres_string.pres_str_id``.
    pres_str_meta_type_id : `int`
        Foreign key link to the ``pres_string`` metadata type table. Foreign
        key to ``pres_string_meta_type.pres_str_meta_type_id``.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    pres_str_meta_value : `str`
        The prescription string metadata value. This is indexed (length text).
    pres_string_meta_type : `prescription_db.orm.PresStringMetadataType`, \
    optional, default: `NoneType`
        Relationship with ``pres_string_meta_type``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    """
    __tablename__ = 'pres_string_meta'

    pres_str_meta_id = Column(
        Integer, Sequence('pres_str_meta_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented index."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="Foreign key link to the ``pres_string`` table."
    )
    pres_str_meta_type_id = Column(
        Integer, ForeignKey("pres_string_meta_type.pres_str_meta_type_id"),
        nullable=False,
        doc="Foreign key link to the ``pres_string`` metadata type table."
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    pres_str_meta_value = Column(
        Text(collation='NOCASE'), nullable=False, index=True,
        doc="The prescription string metadata value."
    )

    # -------------------------------------------------------------------------#
    pres_str_type = relationship(
        "PresStringMetadataType",
        back_populates='pres_str_meta'
    )
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_meta'
    )
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str_meta'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TokenLkp(Base):
    """A representation of the ``token_lkp`` table. This contains all of the
    tokens extracted from all of the the prescription strings.

    Parameters
    ----------
    token_id : `int`
        Auto-incremented index.
    token_value : `str`
        The prescription string token value. This is indexed (length 255).
    pres_string_tokens : `prescription_db.orm.PresStringTokens`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_tokens``.
    """
    __tablename__ = 'token_lkp'

    token_id = Column(
        Integer, Sequence('tok_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index"
    )
    token_value = Column(
        String(255), index=True, nullable=False,
        doc="The prescription string token value."
    )

    # -------------------------------------------------------------------------#
    pres_str_tokens = relationship(
        "PresStringTokens",
        back_populates='token_value'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringTokens(Base):
    """A representation of the ``pres_string_tokens`` table. This contains
    mappings of tokens to prescription strings.

    Parameters
    ----------
    pres_str_token_id : `int`
        Auto-incremented index.
    pres_str_id : `int`
        The foreign key link to the ``pres_string`` table. Foreign key to
        ``pres_string.pres_str_id``.
    token_id : `int`
        The foreign key link to the ``token_lkp`` table. Foreign key to
        ``token_lkp.token_id``.
    token_str_idx : `int`
        The rank position that the token occupies in the prescription string.
    token_start_pos : `int`
        The starting character position that the token occupies in the
        prescription string.
    token_end_pos : `int`
        The ending character position that the token occupies in the
        prescription string.
    in_bracket : `bool`
        Does the token occur within a bracketed region of the prescription
        string.
    dict_id : `int`
        The link to dictionary term that was used to extract/describe the
        token. Foreign key to ``dict.dict_id``.
    sub_class_id : `int`
        The link to sub class that the prescription string token belongs to.
        Foreign key to ``sub_class_lkp.sub_class_id``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.
    dict : `prescription_db.orm.Dict`, optional, default: `NoneType`
        Relationship with ``dict``.
    token_lkp : `prescription_db.orm.TokenLkp`, optional, default: `NoneType`
        Relationship with ``token_lkp``.
    sub_class_lkp : `prescription_db.orm.SubClassLkp`, optional, \
    default: `NoneType`
        Relationship with ``sub_class_lkp``.
    """
    __tablename__ = 'pres_string_tokens'

    pres_str_token_id = Column(
        Integer, Sequence('pst_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="The foreign key link to the ``pres_string`` table."
    )
    token_id = Column(
        Integer, ForeignKey("token_lkp.token_id"),
        nullable=False,
        doc="The foreign key link to the ``token_lkp`` table."
    )
    token_str_idx = Column(
        Integer, nullable=False,
        doc="The rank position that the token occupies in the prescription "
        "string."
    )
    token_start_pos = Column(
        Integer, nullable=False,
        doc="The starting character position that the token occupies in the"
        " prescription string."
    )
    token_end_pos = Column(
        Integer, nullable=False,
        doc="The ending character position that the token occupies in the"
        " prescription string."
    )
    in_bracket = Column(
        Boolean, nullable=False,
        doc="Does the token occur within a bracketed region of the "
        "prescription string."
    )
    tag_id = Column(
        Integer, ForeignKey("tag_lkp.tag_id"), nullable=False,
        doc="The link to the tag type of the token."
    )
    manual_tag_id = Column(
        Integer, ForeignKey("tag_lkp.tag_id"), nullable=True,
        doc="The link to the manually assigned tag type of the token."
    )

    # -------------------------------------------------------------------------
    pres_string = relationship(
        "PresString",
        back_populates='pres_str_tokens'
    )
    token_value = relationship(
        "TokenLkp",
        back_populates='pres_str_tokens'
    )
    tag = relationship(
        "TagLkp",
        foreign_keys=[tag_id],
        back_populates='pres_str_tokens'
    )
    manual_tag = relationship(
        "TagLkp",
        foreign_keys=[manual_tag_id],
        back_populates='pres_str_tokens'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TagLkp(Base):
    """A representation of the ``tag_lkp`` table. These are tags given by the
    heuristic parser.

    Parameters
    ----------
    tag_id : `int`
        Auto-incremented index.
    tag_name : `str`
        The name of the tag (length 50).
    """
    __tablename__ = 'tag_lkp'

    tag_id = Column(
        Integer, Sequence('tag_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    tag_name = Column(
        String(50), nullable=False,
        doc="The name of the tag."
    )

    # -------------------------------------------------------------------------
    pres_str_tokens = relationship(
        "PresStringTokens",
        foreign_keys=[PresStringTokens.tag_id],
        back_populates='tag'
    )
    # -------------------------------------------------------------------------
    manual_pres_str_tokens = relationship(
        "PresStringTokens",
        foreign_keys=[PresStringTokens.manual_tag_id],
        back_populates='manual_tag'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseFormLkp(Base):
    """A representation of the ``base_form_lkp`` table. These are very basic
    forms for prescription components.

    Parameters
    ----------
    base_form_id : `int`
        Auto-incremented index.
    base_form_name : `str`
        The name of the base level form (length 50).
    dict_base_form : `prescription_db.orm.DictBaseForm`, optional, \
    default: `NoneType`
        Relationship with ``dict_base_form``.
    """
    __tablename__ = 'base_form_lkp'

    base_form_id = Column(
        Integer, Sequence('base_form_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    base_form_name = Column(
        String(50), nullable=False,
        doc="The name of the base level form."
    )

    # -------------------------------------------------------------------------
    dict_base_form = relationship(
        "DictBaseForm",
        back_populates='base_form'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseTypeLkp(Base):
    """A representation of the ``base_type_lkp`` table. These are very basic
    types for prescription components.

    Parameters
    ----------
    base_type_id : `int`
        Auto-incremented index.
    base_type_name : `str`
        The name of the base type (length 50).
    dict_base_type : `prescription_db.orm.DictBaseType`, optional, \
    default: `NoneType`
        Relationship with ``dict_base_type``.
    """
    __tablename__ = 'base_type_lkp'

    base_type_id = Column(
        Integer, Sequence('base_type_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    base_type_name = Column(
        String(50), nullable=False,
        doc="The name of the base type."
    )

    # -------------------------------------------------------------------------
    dict_base_type = relationship(
        "DictBaseType",
        back_populates='base_type'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RouteAdminLkp(Base):
    """A representation of the ``route_admin_lkp`` table. These are route of
    administration indicators.

    Parameters
    ----------
    route_admin_id : `int`
        Auto-incremented index.
    route_admin_name : `str`
        The name of the route of administration (length 60).
    dict_route_admin : `prescription_db.orm.DictRouteAdmin`, optional, \
    default: `NoneType`
        Relationship with ``dict_route_admin``.
    """
    __tablename__ = 'route_admin_lkp'

    route_admin_id = Column(
        Integer, Sequence('route_admin_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    route_admin_name = Column(
        String(60), nullable=False,
        doc="The name of the route of administration."
    )

    # -------------------------------------------------------------------------
    dict_route_admin = relationship(
        "DictRouteAdmin",
        back_populates='route_admin'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClassLkp(Base):
    """A representation of the ``class_lkp`` table. These are allowed types for
    prescription string dictionaries.

    Parameters
    ----------
    class_id : `int`
        Auto-incremented index.
    class_name : `str`
        The name of the dict class (length 50).
    sub_class_lkp : `prescription_db.orm.SubClassLkp`, optional, \
    default: `NoneType`
        Relationship with ``sub_class_lkp``.
    """
    __tablename__ = 'class_lkp'

    class_id = Column(
        Integer, Sequence('class_lkp_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    class_name = Column(
        String(50), nullable=False,
        doc="The name of the dict class."
    )

    # -------------------------------------------------------------------------
    sub_class_type = relationship(
        "SubClassLkp",
        back_populates='class_type'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SubClassLkp(Base):
    """A representation of the ``sub_class_lkp`` table. Allowed types for
    prescription string dictionaries, slightly more specific that classes.

    Parameters
    ----------
    sub_class_id : `int`
        Auto-incremented index.
    class_id : `int`
        Link back to the dict class. Foreign key to ``class_lkp.class_id``.
    sub_class_name : `str`
        The name of the dict sub-class (length 50).
    class_lkp : `prescription_db.orm.ClassLkp`, optional, default: `NoneType`
        Relationship with ``class_lkp``.
    dict : `prescription_db.orm.Dict`, optional, default: `NoneType`
        Relationship with ``dict``.
    pres_string_tokens : `prescription_db.orm.PresStringTokens`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_tokens``.
    """
    __tablename__ = 'sub_class_lkp'

    sub_class_id = Column(
        Integer, Sequence('sub_class_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    class_id = Column(
        Integer, ForeignKey("class_lkp.class_id"),
        nullable=False,
        doc="Link back to the dict class."
    )
    sub_class_name = Column(
        String(50), nullable=False,
        doc="The name of the dict sub-class."
    )

    # -------------------------------------------------------------------------
    class_type = relationship(
        "ClassLkp",
        back_populates='sub_class_type'
    )
    dict_link = relationship(
        "Dict",
        back_populates='sub_class_type'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Dict(Base):
    """A representation of the ``dict`` table. These are string tokens mapped
    to hints for form/type/ROA and a preferred name.

    Parameters
    ----------
    dict_id : `int`
        Auto-incremented index.
    name : `str`
        The name of the dict type. This is indexed (length 255).
    standard_name : `str`, optional, default: `NoneType`
        A standardised name for the dict. This is indexed (length 255).
    sub_class_id : `int`
        The foreign key link to the dict ``sub_class_lkp`` table. Foreign key
        to ``sub_class_lkp.sub_class_id``.
    is_regexp : `bool`
        Is the ``name`` a regular expression. This is indexed.
    is_checked : `bool`
        Has the ``name`` been manually checked. This is indexed.
    is_auto_checked : `bool`
        Has the ``name`` been computationally checked. This is indexed.
    sub_class_lkp : `prescription_db.orm.SubClassLkp`, optional, \
    default: `NoneType`
        Relationship with ``sub_class_lkp``.
    dict_base_form : `prescription_db.orm.DictBaseForm`, optional, \
    default: `NoneType`
        Relationship with ``dict_base_form``.
    dict_base_type : `prescription_db.orm.DictBaseType`, optional, \
    default: `NoneType`
        Relationship with ``dict_base_type``.
    dict_route_admin : `prescription_db.orm.DictRouteAdmin`, optional, \
    default: `NoneType`
        Relationship with ``dict_route_admin``.
    pres_string_tokens : `prescription_db.orm.PresStringTokens`, optional, \
    default: `NoneType`
        Relationship with ``pres_string_tokens``.

    Notes
    -----
    This also acts as a general dictionary and can be used to expand dicts to
    standard types. The dicts might not be unique but should be unique across
    dict_name, dict_type_id and dict_standard_name. This allows for dict
    expansion.
    """
    __tablename__ = 'dict'

    dict_id = Column(
        Integer, Sequence('dict_seq'), nullable=False, primary_key=True,
        doc="Auto-incremented index."
    )
    name = Column(
        String(255), nullable=False, index=True,
        doc="The name of the dict type."
    )
    standard_name = Column(
        String(255), nullable=True, index=True,
        doc="A standardised name for the dict."
    )
    sub_class_id = Column(
        Integer, ForeignKey("sub_class_lkp.sub_class_id"),
        nullable=False,
        doc="The foreign key link to the dict ``sub_class_lkp`` table."
    )
    is_regexp = Column(
        Boolean, nullable=False, index=True,
        doc="Is the ``name`` a regular expression."
    )
    is_checked = Column(
        Boolean, nullable=False, index=True, default=False,
        doc="Has the ``name`` been manually checked."
    )
    is_auto_checked = Column(
        Boolean, nullable=False, index=True, default=False,
        doc="Has the ``name`` been computationally checked."
    )

    # -------------------------------------------------------------------------#
    sub_class_type = relationship(
        "SubClassLkp",
        back_populates='dict_link'
    )
    dict_base_form = relationship(
        "DictBaseForm",
        back_populates='dict_link'
    )
    dict_base_type = relationship(
        "DictBaseType",
        back_populates='dict_link'
    )
    dict_route_admin = relationship(
        "DictRouteAdmin",
        back_populates='dict_link'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DictBaseForm(Base):
    """A representation of the ``dict_base_form`` table. Links between dict
    entries and base form hints.

    Parameters
    ----------
    dict_base_form_id : `int`
        Auto-incremented index.
    dict_id : `int`
        The foreign key link back to the ``dict`` table. Foreign key to
        ``dict.dict_id``.
    base_form_id : `int`
        The foreign key link back to the ``base_form_lkp`` table. Foreign key
        to ``base_form_lkp.base_form_id``.
    dict : `prescription_db.orm.Dict`, optional, default: `NoneType`
        Relationship with ``dict``.
    base_form_lkp : `prescription_db.orm.BaseFormLkp`, optional, \
    default: `NoneType`
        Relationship with ``base_form_lkp``.
    """
    __tablename__ = 'dict_base_form'

    dict_base_form_id = Column(
        Integer, Sequence('dict_base_form_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented index."
    )
    dict_id = Column(
        Integer, ForeignKey('dict.dict_id'), nullable=False,
        doc="The foreign key link back to the ``dict`` table."
    )
    base_form_id = Column(
        Integer, ForeignKey('base_form_lkp.base_form_id'), nullable=False,
        doc="The foreign key link back to the ``base_form_lkp`` table."
    )

    # -------------------------------------------------------------------------
    dict_link = relationship(
        "Dict",
        back_populates='dict_base_form'
    )
    base_form = relationship(
        "BaseFormLkp",
        back_populates='dict_base_form'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DictBaseType(Base):
    """A representation of the ``dict_base_type`` table. Links between dict
    entries and base type hints.

    Parameters
    ----------
    dict_base_type_id : `int`
        Auto-incremented index.
    dict_id : `int`
        The foreign key link back to the ``dict`` table. Foreign key to
        ``dict.dict_id``.
    base_type_id : `int`
        The foreign key link back to the ``base_type_lkp`` table. Foreign key
        to ``base_type_lkp.base_type_id``.
    dict : `prescription_db.orm.Dict`, optional, default: `NoneType`
        Relationship with ``dict``.
    base_type_lkp : `prescription_db.orm.BaseTypeLkp`, optional, \
    default: `NoneType`
        Relationship with ``base_type_lkp``.
    """
    __tablename__ = 'dict_base_type'

    dict_base_type_id = Column(
        Integer, Sequence('dict_base_type_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented index."
    )
    dict_id = Column(
        Integer, ForeignKey('dict.dict_id'), nullable=False,
        doc="The foreign key link back to the ``dict`` table."
    )
    base_type_id = Column(
        Integer, ForeignKey('base_type_lkp.base_type_id'), nullable=False,
        doc="The foreign key link back to the ``base_type_lkp`` table."
    )

    # -------------------------------------------------------------------------
    dict_link = relationship(
        "Dict",
        back_populates='dict_base_type'
    )
    base_type = relationship(
        "BaseTypeLkp",
        back_populates='dict_base_type'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DictRouteAdmin(Base):
    """A representation of the ``base_form_lkp`` table. Links between dict
    entries and route of administration hints.

    Parameters
    ----------
    dict_route_admin_id : `int`
        Auto-incremented index.
    dict_id : `int`
        The foreign key link back to the ``dict`` table. Foreign key to
        ``dict.dict_id``.
    route_admin_id : `int`
        The foreign key link back to the ``route_admin_lkp`` table. Foreign key
        to ``route_admin_lkp.route_admin_id``.
    dict : `prescription_db.orm.Dict`, optional, default: `NoneType`
        Relationship with ``dict``.
    route_admin_lkp : `prescription_db.orm.RouteAdminLkp`, optional, \
    default: `NoneType`
        Relationship with ``route_admin_lkp``.
    """
    __tablename__ = 'dict_route_admin'

    dict_route_admin_id = Column(
        Integer, Sequence('dict_route_admin_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented index."
    )
    dict_id = Column(
        Integer, ForeignKey('dict.dict_id'), nullable=False,
        doc="The foreign key link back to the ``dict`` table."
    )
    route_admin_id = Column(
        Integer, ForeignKey('route_admin_lkp.route_admin_id'), nullable=False,
        doc="The foreign key link back to the ``route_admin_lkp`` table."
    )

    # -------------------------------------------------------------------------
    dict_link = relationship(
        "Dict",
        back_populates='dict_route_admin'
    )
    route_admin = relationship(
        "RouteAdminLkp",
        back_populates='dict_route_admin'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# @@@@@@@@@@@@@@@@@@@@@@@@ TABLES DERIVED FROM THE DM+D @@@@@@@@@@@@@@@@@@@@@@@
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdCombProdLkp(Base):
    """A representation of the ``dmd_comb_prod_lkp`` table.

    Parameters
    ----------
    comb_prod_id : `int`
        Auto-incremented primary key.
    comb_prod_desc : `str`
        The combination product description (length 255).
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.

    Notes
    -----
    This is a lookup table of indicators if a prescription is a combination
    product (composed of multiple things).
    """
    __tablename__ = 'dmd_comb_prod_lkp'

    comb_prod_id = Column(
        Integer, Sequence('dmd_comb_prod_lkp_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented primary key."
    )
    comb_prod_desc = Column(
        String(255), nullable=False,
        doc="The combination product description."
    )

    # -------------------------------------------------------------------------~
    dmd_pres = relationship(
        "DmdPrescription",
        back_populates='comb_prod'
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdDoseFormLkp(Base):
    """A representation of the ``dmd_dose_form_lkp`` table.

    Parameters
    ----------
    dose_form_id : `int`
        Auto-incremented primary key.
    dose_form_desc : `str`
        The dose form description (length 20).
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.

    Notes
    -----
    This is the dose form indicator lookup table from the DM+D.
    """
    __tablename__ = 'dmd_dose_form_lkp'

    dose_form_id = Column(
        Integer, Sequence('dmd_dose_form_lkp_seq'), nullable=False,
        primary_key=True, doc="Auto-incremented primary key."
    )
    dose_form_desc = Column(
        String(20), nullable=False,
        doc="The dose form description."
    )

    # -------------------------------------------------------------------------#
    dmd_pres = relationship(
        "DmdPrescription",
        back_populates='dose_form'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdFormLkp(Base):
    """A representation of the ``dmd_form_lkp`` table, this is an analogue of
    the  ``lkp_form`` table in the DM+D database.

    Parameters
    ----------
    form_id : `int`
        SnomedCT concept ID primary key.
    form_id_date : `date.date`, optional, default: `NoneType`
        Date the code is applicable from.
    form_id_prev : `int`, optional, default: `NoneType`
        Previous form code, upto 18 digits.
    form_desc : `str`
        The form code description (length 60).
    dmd_pres_form : `prescription_db.orm.DmdPresForm`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_form``.
    """
    __tablename__ = 'dmd_form_lkp'

    form_id = Column(
        BigInteger, Sequence('dmd_form_lkp_seq'), nullable=False,
        primary_key=True, doc="SnomedCT concept ID primary key."
    )
    form_id_date = Column(
        Date, nullable=True,
        doc="Date the code is applicable from."
    )
    form_id_prev = Column(
        BigInteger, nullable=True,
        doc="Previous form code, upto 18 digits."
    )
    form_desc = Column(
        String(60), nullable=False,
        doc="The form code description."
    )

    # -------------------------------------------------------------------------#
    dmd_pres_form = relationship(
        'DmdPresForm',
        back_populates='form'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdRouteLkp(Base):
    """A representation of the ``dmd_route_lkp`` table. This is an analogue of
    the ``lkp_route`` table in the DM+D.

    Parameters
    ----------
    route_id : `int`
        SnomedCT concept ID primary key.
    route_id_date : `date.date`, optional, default: `NoneType`
        The date the code is applicable from.
    route_id_prev : `int`, optional, default: `NoneType`
        The previous route of administration code (max 18 digits).
    route_desc : `str`
        The route of administration description (length 60).
    dmd_pres_route : `prescription_db.orm.DmdPresRoute`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_route``.
    """
    __tablename__ = 'dmd_route_lkp'

    route_id = Column(
        BigInteger, Sequence('dmd_route_lkp_seq'), nullable=False,
        primary_key=True, doc="SnomedCT concept ID primary key."
    )
    route_id_date = Column(
        Date, nullable=True,
        doc="The date the code is applicable from."
    )
    route_id_prev = Column(
        BigInteger, nullable=True,
        doc="The previous route of administration code (max 18 digits)."
    )
    route_desc = Column(
        String(60), nullable=False,
        doc="The route of administration description."
    )

    # -------------------------------------------------------------------------#
    dmd_pres_route = relationship(
        'DmdPresRoute',
        back_populates='route'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdIngredientLkp(Base):
    """A representation of the ``dmd_ingredient_lkp`` table, this is an
    analogue of the ``ingredient`` table in the DM+D database.

    Parameters
    ----------
    ingredient_id : `int`
        SnomedCT concept ID primary key.
    ingredient_id_date : `date.date`, optional, default: `NoneType`
        The date the ingredient substance identifier became valid.
    ingredient_id_prev : `int`, optional, default: `NoneType`
        Previous ingredient substance identifier (SNOMED Code). Up to a maximum
        of 18 integers.
    ingredient_invalid : `bool`, optional, default: `NoneType`
        Invalidity flag. If set to 1 indicates this is an invalid entry in
        file. 1 integer only.
    ingredient_name : `str`, optional, default: `NoneType`
        The ingredient substance name (length 255).
    dmd_pres_ing : `prescription_db.orm.DmdPresIngredient`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_ing``.
    """
    __tablename__ = 'dmd_ingredient_lkp'

    ingredient_id = Column(
        BigInteger, Sequence('dmd_ingredient_lkp_seq'), nullable=False,
        primary_key=True, doc="SnomedCT concept ID primary key."
    )
    ingredient_id_date = Column(
        Date, nullable=True,
        doc="The date the ingredient substance identifier became valid."
    )
    ingredient_id_prev = Column(
        BigInteger, nullable=True,
        doc="Previous ingredient substance identifier (SNOMED Code). Up "
        "to a maximum of 18 integers."
    )
    ingredient_invalid = Column(
        Boolean, nullable=True,
        doc="Invalidity flag. If set to 1 indicates this is an invalid entry "
        "in file. 1 integer only."
    )
    ingredient_name = Column(
        String(255), nullable=True,
        doc="The ingredient substance name"
    )

    # -------------------------------------------------------------------------~
    dmd_pres_ing = relationship(
        'DmdPresIngredient',
        back_populates='ingredient'
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdPrescription(Base):
    """A representation of a prescription that is taken directly from the DM+D.

    Parameters
    ----------
    vmp_id : `int`
        SnomedCT concept ID primary key.
    vtm_id : `int`, optional, default: `NoneType`
        foreign key to the VTM table. Foreign key to
        ``dmd_pres_moiety.vtm_id``.
    n_vtms : `int`
        The number VTM entries.
    dose_form_id : `int`
        The dose form of the prescription. Foreign key to
        ``dmd_dose_form_lkp.dose_form_id``.
    comb_prod_id : `int`
        The indicator for the combination product type. Foreign key to
        ``dmd_comb_prod_lkp.comb_prod_id``.
    dose_form_size : `float`, optional, default: `NoneType`
        Unit dose form size - a numerical value relating to size of entity.
        This will only be present if the unit dose form attribute is discrete.
        Up to a maximum of 10 digits and 3 decimal places.
    dose_form_meas_id : `int`, optional, default: `NoneType`
        Unit dose form units - unit of measure code relating to the size. This
        will only be present if the unit dose form attribute is discrete.
        Narrative can be located in lookup file under tag <UNIT_OF_MEASURE> Up
        to a maximum of 18 digits. Foreign key to
        ``dmd_unit_measure_lkp.unit_meas_id``.
    unit_dose_meas_id : `int`, optional, default: `NoneType`
        Unit dose unit of measure - unit of measure code relating to a
        description of the entity that can be handled. This will only be
        present if the Unit dose form attribute is discrete. Narrative can be
        located in lookup file under tag <UNIT_OF_MEASURE>. Foreign key to
        ``dmd_unit_measure_lkp.unit_meas_id``.
    pres_str_to_dmd : `prescription_db.orm.PresStringDmd`, optional, \
    default: `NoneType`
        Relationship with ``pres_str_to_dmd``.
    dmd_comb_prod_lkp : `prescription_db.orm.DmdCombProdLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_comb_prod_lkp``.
    dmd_dose_form_lkp : `prescription_db.orm.DmdDoseFormLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_dose_form_lkp``.
    dmd_unit_measure_lkp : `prescription_db.orm.DmdUnitMeasureLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_unit_measure_lkp``.
    dmd_unit_measure_lkp : `prescription_db.orm.DmdUnitMeasureLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_unit_measure_lkp``.
    dmd_pres_moiety : `prescription_db.orm.DmdPresTherMoiety`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_moiety``.
    dmd_pres_route : `prescription_db.orm.DmdPresRoute`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_route``.
    dmd_pres_form : `prescription_db.orm.DmdPresForm`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_form``.
    dmd_pres_ing : `prescription_db.orm.DmdPresIngredient`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_ing``.
    dmd_ddd : `prescription_db.orm.DmdDefinedDailyDose`, optional, \
    default: `NoneType`
        Relationship with ``dmd_ddd``.
    """
    __tablename__ = 'dmd_prescription'

    vmp_id = Column(
        BigInteger, Sequence('dmd_prescription_seq'), nullable=False,
        primary_key=True, doc="SnomedCT concept ID primary key."
    )
    vtm_id = Column(
        BigInteger, ForeignKey("dmd_pres_moiety.vtm_id"),
        doc="foreign key to the VTM table"
    )
    n_vtms = Column(
        Integer, nullable=False,
        doc="The number VTM entries"
    )
    dose_form_id = Column(
        Integer, ForeignKey("dmd_dose_form_lkp.dose_form_id"), nullable=False,
        doc="The dose form of the prescription."
    )
    comb_prod_id = Column(
        Integer, ForeignKey("dmd_comb_prod_lkp.comb_prod_id"), nullable=False,
        doc="The indicator for the combination product type."
    )
    dose_form_size = Column(
        Float, nullable=True,
        doc="Unit dose form size - a numerical value relating to size of "
        "entity. This will only be present if the unit dose form attribute is"
        " discrete. Up to a maximum of 10 digits and 3 decimal places."
    )
    dose_form_meas_id = Column(
        BigInteger, ForeignKey('dmd_unit_measure_lkp.unit_meas_id'),
        nullable=True,
        doc="Unit dose form units - unit of measure code relating to the size."
        " This will only be present if the unit dose form attribute is "
        "discrete. Narrative can be located in lookup file under tag "
        "<UNIT_OF_MEASURE> Up to a maximum of 18 digits."
    )
    unit_dose_meas_id = Column(
        BigInteger, ForeignKey('dmd_unit_measure_lkp.unit_meas_id'),
        nullable=True,
        doc="Unit dose unit of measure - unit of measure code relating to a"
        " description of the entity that can be handled. This will only be "
        "present if the Unit dose form attribute is discrete. Narrative can "
        "be located in lookup file under tag <UNIT_OF_MEASURE>."
    )

    # -------------------------------------------------------------------------~
    dmd_pres = relationship(
        "PresStringDmd",
        back_populates='dmd_pres'
    )
    comb_prod = relationship(
        "DmdCombProdLkp",
        back_populates='dmd_pres'
    )
    dose_form = relationship(
        "DmdDoseFormLkp",
        back_populates='dmd_pres'
    )
    dose_form_unit_meas = relationship(
        'DmdUnitMeasureLkp',
        foreign_keys=[dose_form_meas_id],
        back_populates='dmd_pres_dose_form_unit'
    )
    unit_dose_unit_meas = relationship(
        'DmdUnitMeasureLkp',
        foreign_keys=[unit_dose_meas_id],
        back_populates='dmd_pres_unit_dose_unit'
    )
    dmd_pres_moiety = relationship(
        'DmdPresTherMoiety',
        back_populates='dmd_pres'
    )
    dmd_pres_route = relationship(
        'DmdPresRoute',
        back_populates='dmd_pres'
    )
    dmd_pres_form = relationship(
        'DmdPresForm',
        back_populates='dmd_pres'
    )
    dmd_pres_ing = relationship(
        'DmdPresIngredient',
        back_populates='dmd_pres'
    )
    ddd = relationship(
        "DmdDefinedDailyDose",
        back_populates='dmd_pres'
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PresStringDmd(Base):
    """A representation of the ``pres_str_to_dmd`` table. This maps
    prescription strings to DM+D prescription definitions.

    Parameters
    ----------
    pres_mapping_id : `int`
        The primary key.
    pres_str_id : `int`
        The link to the prescription string table. Foreign key to
        ``pres_string.pres_str_id``.
    vmp_id : `int`
        The link to the DMD prescription table. Foreign key to
        ``dmd_prescription.vmp_id``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    """
    __tablename__ = 'pres_str_to_dmd'

    pres_mapping_id = Column(
        Integer, Sequence('pres_str_to_dmd_seq'), nullable=False,
        primary_key=True, doc="The primary key."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"),
        nullable=False,
        doc="The link to the prescription string table."
    )
    vmp_id = Column(
        BigInteger, ForeignKey("dmd_prescription.vmp_id"),
        nullable=False,
        doc="The link to the DMD prescription table."
    )

    # -------------------------------------------------------------------------~
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_map'
    )
    dmd_pres = relationship(
        "DmdPrescription",
        back_populates='dmd_pres'
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdPresIngredient(Base):
    """A representation of the ``dmd_pres_ing`` table this is an analogue of
    the ``virt_prod_ing`` table in the DM+D database.

    Parameters
    ----------
    pres_ing_id : `int`
        The primary key.
    vmp_id : `int`
        The foreign key link to the ``dmd_prescription`` table. Foreign key to
        ``dmd_prescription.vmp_id``.
    ingredient_id : `int`
        The foreign key link to the ``dmd_ingredient_lkp`` table. Foreign key
        to ``dmd_ingredient_lkp.ingredient_id``.
    stren_value_num : `float`, optional, default: `NoneType`
        Strength value numerator - value of numerator element of strength up to
        a maximum of 10 digits and 3 decimal places.
    stren_value_num_unit_meas_id : `int`, optional, default: `NoneType`
        Strength value numerator unit - The foreign key link to the dmd unit of
        measure table. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.
    stren_value_den : `float`, optional, default: `NoneType`
        Strength value denominator - value of denominator element of strength.
        Up to a maximum of 10 digits and 3 decimal places.
    stren_value_den_unit_meas_id : `int`, optional, default: `NoneType`
        Strength value denominator unit - The foreign key link to the dmd unit
        of measure table. Foreign key to ``dmd_unit_measure_lkp.unit_meas_id``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    dmd_ingredient_lkp : `prescription_db.orm.DmdIngredientLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_ingredient_lkp``.
    dmd_unit_measure_lkp : `prescription_db.orm.DmdUnitMeasureLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_unit_measure_lkp``.
    dmd_unit_measure_lkp : `prescription_db.orm.DmdUnitMeasureLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_unit_measure_lkp``.
    """
    __tablename__ = 'dmd_pres_ing'

    pres_ing_id = Column(
        Integer, Sequence('dmd_pres_ing_seq'), nullable=False,
        primary_key=True,
        doc="The primary key."
    )
    vmp_id = Column(
        BigInteger, ForeignKey("dmd_prescription.vmp_id"), nullable=False,
        doc="The foreign key link to the ``dmd_prescription`` table."
    )
    ingredient_id = Column(
        BigInteger, ForeignKey("dmd_ingredient_lkp.ingredient_id"),
        nullable=False,
        doc="The foreign key link to the ``dmd_ingredient_lkp`` table."
    )
    stren_value_num = Column(
        Float, nullable=True,
        doc="Strength value numerator - value of numerator element of strength"
        " up to a maximum of 10 digits and 3 decimal places."
    )
    stren_value_num_unit_meas_id = Column(
        BigInteger, ForeignKey('dmd_unit_measure_lkp.unit_meas_id'),
        nullable=True,
        doc="Strength value numerator unit - The foreign key link to the "
        "dmd unit of measure table."
    )
    stren_value_den = Column(
        Float, nullable=True,
        doc="Strength value denominator - value of denominator element of "
        "strength. Up to a maximum of 10 digits and 3 decimal places."
    )
    stren_value_den_unit_meas_id = Column(
        BigInteger, ForeignKey('dmd_unit_measure_lkp.unit_meas_id'),
        nullable=True,
        doc="Strength value denominator unit - The foreign key link to the "
        "dmd unit of measure table."
    )

    # -------------------------------------------------------------------------~
    dmd_pres = relationship(
        'DmdPrescription',
        back_populates='dmd_pres_ing'
    )
    ingredient = relationship(
        'DmdIngredientLkp',
        back_populates='dmd_pres_ing'
    )
    stren_value_num_unit_meas = relationship(
        'DmdUnitMeasureLkp',
        foreign_keys=[stren_value_num_unit_meas_id],
        back_populates='dmd_pres_num_unit'
    )
    stren_value_den_unit_meas = relationship(
        'DmdUnitMeasureLkp',
        foreign_keys=[stren_value_den_unit_meas_id],
        back_populates='dmd_pres_den_unit'
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdUnitMeasureLkp(Base):
    """A representation of the ``dmd_unit_measure_lkp`` table. This is an
    analogue of the ``lkp_unit_measure`` table in the DM+D database.

    Parameters
    ----------
    unit_meas_id : `int`
        The primary key, whilst this is set as an auto-increment but is sourced
        from the XML file. Apparently it is a SNOMED code up to 18 digits long.
    unit_meas_id_date : `date.date`, optional, default: `NoneType`
        The date the code is applicable from.
    unit_meas_id_prev : `int`, optional, default: `NoneType`
        Previous code, up to 18 digits.
    unit_meas_desc : `str`
        The unit of measure description (length 255).
    dmd_pres_ing : `prescription_db.orm.DmdPresIngredient`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_ing``.
    dmd_pres_ing : `prescription_db.orm.DmdPresIngredient`, optional, \
    default: `NoneType`
        Relationship with ``dmd_pres_ing``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    dmd_ddd : `prescription_db.orm.DmdDefinedDailyDose`, optional, \
    default: `NoneType`
        Relationship with ``dmd_ddd``.
    """
    __tablename__ = 'dmd_unit_measure_lkp'

    unit_meas_id = Column(
        BigInteger, Sequence('dmd_unit_measure_lkp_seq'), nullable=False,
        primary_key=True,
        doc="The primary key, whilst this is set as an auto-increment but is"
        " sourced from the XML file. Apparently it is a SNOMED code up to 18"
        " digits long."
    )
    unit_meas_id_date = Column(
        Date, nullable=True,
        doc="The date the code is applicable from."
    )
    unit_meas_id_prev = Column(
        BigInteger, nullable=True,
        doc="Previous code, up to 18 digits."
    )
    unit_meas_desc = Column(
        String(255), nullable=False,
        doc="The unit of measure description."
    )

    # -------------------------------------------------------------------------#
    dmd_pres_num_unit = relationship(
        'DmdPresIngredient',
        foreign_keys=[DmdPresIngredient.stren_value_num_unit_meas_id],
        back_populates='stren_value_num_unit_meas'
    )
    dmd_pres_den_unit = relationship(
        'DmdPresIngredient',
        foreign_keys=[DmdPresIngredient.stren_value_den_unit_meas_id],
        back_populates='stren_value_den_unit_meas'
    )
    dmd_pres_dose_form_unit = relationship(
        'DmdPrescription',
        foreign_keys=[DmdPrescription.dose_form_meas_id],
        back_populates='dose_form_unit_meas'
    )
    dmd_pres_unit_dose_unit = relationship(
        'DmdPrescription',
        foreign_keys=[DmdPrescription.unit_dose_meas_id],
        back_populates='unit_dose_unit_meas'
    )
    ddd = relationship(
        'DmdDefinedDailyDose',
        back_populates='ddd_units'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdPresTherMoiety(Base):
    """A representation of the ``dmd_pres_moiety`` table this is an adaptation
    of the ``virt_ther_moit`` table in the DM+D database.

    Parameters
    ----------
    vtm_id : `int`
        Virtual therapeutic moiety identifier (SNOMED Code). Up to a maximum of
        18 integers.
    vtm_invalid : `bool`, optional, default: `NoneType`
        Invalidity flag. If set to 1 indicates this is an invalid entry in
        file. 1 integer only.
    vtm_name : `str`, optional, default: `NoneType`
        Virtual therapeutic moiety name (length 255).
    vtm_abbrev : `str`, optional, default: `NoneType`
        Virtual therapeutic moiety abbreviated name (length 60).
    vtm_id_prev : `int`, optional, default: `NoneType`
        Previous virtual therapeutic moiety identifier (SNOMED Code). Up to a
        maximum of 18 integers.
    vtm_id_date : `date.date`, optional, default: `NoneType`
        The date the virtual therapeutic moiety identifier became valid.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    """
    __tablename__ = 'dmd_pres_moiety'

    vtm_id = Column(
        BigInteger, Sequence('dmd_pres_moiety_seq'), nullable=False,
        primary_key=True,
        doc="Virtual therapeutic moiety identifier (SNOMED Code). Up to a "
        "maximum of 18 integers."
    )
    vtm_invalid = Column(
        Boolean, nullable=True,
        doc="Invalidity flag. If set to 1 indicates this is an invalid entry "
        "in file. 1 integer only."
    )
    vtm_name = Column(
        String(255), nullable=True,
        doc="Virtual therapeutic moiety name."
    )
    vtm_abbrev = Column(
        String(60), nullable=True,
        doc="Virtual therapeutic moiety abbreviated name."
    )
    vtm_id_prev = Column(
        BigInteger, nullable=True,
        doc="Previous virtual therapeutic moiety identifier (SNOMED Code). Up "
        "to a maximum of 18 integers."
    )
    vtm_id_date = Column(
        Date, nullable=True,
        doc="The date the virtual therapeutic moiety identifier became valid."
    )

    # -------------------------------------------------------------------------#
    dmd_pres = relationship(
        'DmdPrescription',
        back_populates='dmd_pres_moiety'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdPresRoute(Base):
    """A representation of the ``dmd_pres_ing`` table this is an analogue of
    the ``virt_prod_ing`` table in the DM+D database.

    Parameters
    ----------
    pres_route_id : `int`
        The primary key.
    vmp_id : `int`
        The foreign key link to the ``dmd_prescription`` table. Foreign key to
        ``dmd_prescription.vmp_id``.
    route_id : `int`
        The foreign key link to the ``dmd_route_lkp`` table. Foreign key to
        ``dmd_route_lkp.route_id``.
    dmd_route_lkp : `prescription_db.orm.DmdRouteLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_route_lkp``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    """
    __tablename__ = 'dmd_pres_route'

    pres_route_id = Column(
        Integer, Sequence('dmd_pres_route_seq'), nullable=False,
        primary_key=True,
        doc="The primary key."
    )
    vmp_id = Column(
        BigInteger, ForeignKey("dmd_prescription.vmp_id"), nullable=False,
        doc="The foreign key link to the ``dmd_prescription`` table."
    )
    route_id = Column(
        BigInteger, ForeignKey("dmd_route_lkp.route_id"),
        nullable=False,
        doc="The foreign key link to the ``dmd_route_lkp`` table."
    )

    # -------------------------------------------------------------------------#
    route = relationship(
        'DmdRouteLkp',
        back_populates='dmd_pres_route'
    )
    dmd_pres = relationship(
        'DmdPrescription',
        back_populates='dmd_pres_route'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdPresForm(Base):
    """A representation of the ``dmd_pres_ing`` table this is an analogue of
    the ``virt_prod_ing`` table in the DM+D database.

    Parameters
    ----------
    pres_form_id : `int`
        Auto-incremented primary key.
    vmp_id : `int`
        The foreign key link to the ``dmd_prescription`` table. Foreign key to
        ``dmd_prescription.vmp_id``.
    form_id : `int`
        The foreign key link to the ``dmd_form_lkp`` table. Foreign key to
        ``dmd_form_lkp.form_id``.
    dmd_form_lkp : `prescription_db.orm.DmdFormLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_form_lkp``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    """
    __tablename__ = 'dmd_pres_form'

    pres_form_id = Column(
        Integer, Sequence('dmd_pres_form_seq'), nullable=False,
        primary_key=True,
        doc="Auto-incremented primary key."
    )
    vmp_id = Column(
        BigInteger, ForeignKey("dmd_prescription.vmp_id"), nullable=False,
        doc="The foreign key link to the ``dmd_prescription`` table."
    )
    form_id = Column(
        BigInteger, ForeignKey("dmd_form_lkp.form_id"),
        nullable=False,
        doc="The foreign key link to the ``dmd_form_lkp`` table."
    )

    # -------------------------------------------------------------------------#
    form = relationship(
        'DmdFormLkp',
        back_populates='dmd_pres_form'
    )
    dmd_pres = relationship(
        'DmdPrescription',
        back_populates='dmd_pres_form'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ExternalCodeTypeLkp(Base):
    """A representation of the ``external_code_type_lkp`` table.

    Parameters
    ----------
    external_code_type_id : `int`
        Auto-incremented primary key.
    external_code_name : `str`
        The external code type name (length 255).
    dmd_external_code : `prescription_db.orm.DmdExternalCode`, optional, \
    default: `NoneType`
        Relationship with ``dmd_external_code``.
    """
    __tablename__ = 'external_code_type_lkp'

    external_code_type_id = Column(
        Integer, Sequence('external_code_type_lkp_seq'), nullable=False,
        primary_key=True,
        doc="Auto-incremented primary key."
    )
    external_code_name = Column(
        String(255), nullable=False,
        doc="The external code type name."
    )

    # -------------------------------------------------------------------------#
    pres_str_external_code = relationship(
        'DmdExternalCode',
        back_populates='external_code_type'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdExternalCode(Base):
    """A representation of the ``dmd_external_code`` table this is uses data
    from the ``virt_med_prod_map`` table in the DM+D database.

    Parameters
    ----------
    external_code_id : `int`
        The primary key.
    pres_str_id : `int`
        The foreign key link to the ``pres_string`` string table. Foreign key
        to ``pres_string.pres_str_id``.
    source_version_id : `int`
        Foreign key link to the ``source_version`` table. Foreign key to
        ``source_version.source_version_id``.
    external_code_type_id : `int`
        Foreign key link to the ``external_code_type_lkp`` table. Foreign key
        to ``external_code_type_lkp.external_code_type_id``.
    code_id : `str`
        The external code ID (length 255).
    external_code_type_lkp : `prescription_db.orm.ExternalCodeTypeLkp`, \
    optional, default: `NoneType`
        Relationship with ``external_code_type_lkp``.
    source_version : `prescription_db.orm.SourceVersion`, optional, \
    default: `NoneType`
        Relationship with ``source_version``.
    pres_string : `prescription_db.orm.PresString`, optional, \
    default: `NoneType`
        Relationship with ``pres_string``.
    """
    __tablename__ = 'dmd_external_code'

    external_code_id = Column(
        Integer, Sequence('dmd_external_code_seq'), nullable=False,
        primary_key=True,
        doc="The primary key."
    )
    pres_str_id = Column(
        Integer, ForeignKey("pres_string.pres_str_id"), nullable=False,
        doc="The foreign key link to the ``pres_string`` string table."
    )
    source_version_id = Column(
        Integer, ForeignKey("source_version.source_version_id"),
        nullable=False,
        doc="Foreign key link to the ``source_version`` table."
    )
    external_code_type_id = Column(
        Integer, ForeignKey("external_code_type_lkp.external_code_type_id"),
        nullable=False,
        doc="Foreign key link to the ``external_code_type_lkp`` table."
    )
    code_id = Column(
        String(255), nullable=False,
        doc="The external code ID."
    )

    # -------------------------------------------------------------------------#
    external_code_type = relationship(
        'ExternalCodeTypeLkp',
        back_populates='pres_str_external_code'
    )
    source_version = relationship(
        "SourceVersion",
        back_populates='pres_str_external_code'
    )
    pres_str = relationship(
        "PresString",
        back_populates='pres_str_external_code'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdDefinedDailyDose(Base):
    """A representation of the ``dmd_external_code`` table this is uses data
    from the ``virt_med_prod_map`` table in the DM+D database.

    Parameters
    ----------
    ddd_id : `int`
        The primary key.
    vmp_id : `int`
        The foreign key link to the ``dmd_prescription`` table. Foreign key to
        ``dmd_prescription.vmp_id``.
    ddd : `float`
        The defined daily dose value.
    ddd_unit_id : `int`
        Foreign key link to the ``dmd_unit_measure_lkp`` table. Foreign key to
        ``dmd_unit_measure_lkp.unit_meas_id``.
    dmd_prescription : `prescription_db.orm.DmdPrescription`, optional, \
    default: `NoneType`
        Relationship with ``dmd_prescription``.
    dmd_unit_measure_lkp : `prescription_db.orm.DmdUnitMeasureLkp`, optional, \
    default: `NoneType`
        Relationship with ``dmd_unit_measure_lkp``.
    """
    __tablename__ = 'dmd_ddd'

    ddd_id = Column(
        Integer, Sequence('dmd_ddd_seq'), nullable=False, primary_key=True,
        doc="The primary key."
    )
    vmp_id = Column(
        BigInteger, ForeignKey("dmd_prescription.vmp_id"), nullable=False,
        doc="The foreign key link to the ``dmd_prescription`` table."
    )
    ddd = Column(
        Float, nullable=False,
        doc="The defined daily dose value."
    )
    ddd_unit_id = Column(
        Integer, ForeignKey("dmd_unit_measure_lkp.unit_meas_id"),
        nullable=False,
        doc="Foreign key link to the ``dmd_unit_measure_lkp`` table."
    )

    # -------------------------------------------------------------------------#
    dmd_pres = relationship(
        "DmdPrescription",
        back_populates='ddd'
    )
    ddd_units = relationship(
        'DmdUnitMeasureLkp',
        back_populates='ddd'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)
