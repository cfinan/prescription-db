# Importing the version number (in __init__.py) and the package name
from prescription_db import (
    common,
    units,
    orm as porm
)
from prescription_db.example_data import examples
from sqlalchemy import and_, or_
import re
import pyparsing
import pprint as pp


UNCHECKED_TAG = 'UNCHECKED'
EXTEND_BELOW_TAG = 'EXTEND_BELOW'
EXTEND_ABOVE_TAG = 'EXTEND_ABOVE'
SPLIT_EXTEND_BELOW_TAG = 'SPLIT_EXTEND_BELOW'
SPLIT_EXTEND_ABOVE_TAG = 'SPLIT_EXTEND_ABOVE'

BRACKET_TAG = 'BRACKET'
"""The name for a bracket tag
"""
SUPPLIER_TAG = 'SUPPLIER'
"""The name for a supplier tag
"""
DRUG_TAG = 'DRUG'
"""The name for a drug tag
"""
HOMEO_TAG = 'HOMEOPATHIC'
"""The name for a homeopathic remedy tag
"""
INGREDIENT_TAG = 'INGREDIENT'
"""The name for an ingredient tag
"""
UNIT_TAG = 'UNIT'
"""The name for a unit tag
"""
OTHER_TAG = 'UNKNOWN'
"""The name for any untagged chunks tag
"""
ROA_TAG = 'ROA'
"""The name for any route of administration chunk tag
"""
FORM_TAG = 'FORM'
"""The name for any dose form chunk tag
"""
DRUG_SALT_TAG = 'DRUG_SALT'
"""The name for any dose form chunk tag
"""
PERSON_TYPE_TAG = 'PERSON_TYPE'
"""The name for any dose form chunk tag
"""
QUAL_UNIT_TAG = 'QUAL_UNIT'
"""The name for any dose form chunk tag
"""
RATIO_UNIT_TAG = 'RATIO_UNIT'
RATE_UNIT_TAG = 'RATE_UNIT'
RANGE_UNIT_TAG = 'RANGE_UNIT'
PERCENT_UNIT_TAG = 'PERCENT_UNIT'
CONBINED_UNIT_TAG = 'COMBINED_UNIT'
VOLUME_UNIT_TAG = 'VOLUME_UNIT'
AREA_UNIT_TAG = 'AREA_UNIT'
INTERNATIONAL_UNIT_TAG = 'INTERNATIONAL_UNIT'
RANGE_TAG = 'RANGE'
PRODUCT_CODE_TAG = 'PRODUCT_CODE'
NUMERIC_TAG = 'NUMERIC'

TAGS = [
    UNCHECKED_TAG,
    EXTEND_BELOW_TAG,
    EXTEND_ABOVE_TAG,
    SPLIT_EXTEND_BELOW_TAG,
    SPLIT_EXTEND_ABOVE_TAG,
    SUPPLIER_TAG,
    DRUG_TAG,
    HOMEO_TAG,
    INGREDIENT_TAG,
    UNIT_TAG,
    OTHER_TAG,
    ROA_TAG,
    FORM_TAG,
    DRUG_SALT_TAG,
    PERSON_TYPE_TAG,
    QUAL_UNIT_TAG,
    RATIO_UNIT_TAG,
    RATE_UNIT_TAG,
    RANGE_UNIT_TAG,
    PERCENT_UNIT_TAG,
    CONBINED_UNIT_TAG,
    VOLUME_UNIT_TAG,
    AREA_UNIT_TAG,
    INTERNATIONAL_UNIT_TAG,
    RANGE_TAG,
    PRODUCT_CODE_TAG,
    NUMERIC_TAG
]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PrescriptionString(object):
    """A wrapper around the prescription string.

    Parameters
    ----------
    pres_str : `str`
        The prescription string to parse.
    prefix_tags : `list` of `str`
        A tag name that is given to every tag that is generated in this
        prescription string, in addition to the desired name.
    offset : `int`
        The coordinate of the start of the prescription string.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, pres_str, prefix_tags=None, offset=0):
        # Perform any pre-processing on the prescription string before we do
        # anything else
        self.pres_str = self.str_subs(pres_str)
        self.prefix_func = self.dummy_prefix
        self.prefix_tags = prefix_tags or list()
        if len(self.prefix_tags) > 0:
            self.prefix_func = self.add_prefix
        else:
            self.prefix_tags = list()
        self.offset = offset

        # QC the prescription string
        self.is_missing()

        # The processed string is used for most of the value searches, it will
        # have all matched values removed and replaced with whitespace. So will
        # gradually become more sparse as things are located.
        self.proc_str = self.pres_str.lower()

        # Will hold all the matched tags, it is keyed by tuples of the tag
        # locations
        self._tags = dict()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """pretty printing
        """
        return self.pres_str

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def add_prefix(tags, prefix):
        """
        """
        tags = list(tags)
        tags.extend(prefix)
        return tags

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def dummy_prefix(tags, prefix):
        """
        """
        return tags

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def str_subs(pres_str):
        """Perform string pre-processing steps that attempt to clean up some
        bad strings.

        Notes
        -----
        These are removal of leading/trailing whitespace, space substitutions:
        * ``%\W``
        * ``[a-zA-Z]\.[\da-zA-Z]``

        Removals:
        * ``,`` or space between 0
        """
        # Add a space next to percent signs that are next to non-words
        pres_str = re.sub(r'%(\W)', '% \\1', pres_str.strip())

        # Substitute dots for spaces when leading with a word and lagging with
        # a letter or digit
        pres_str = re.sub(r'([a-zA-Z])\.([\da-zA-Z])', '\\1 \\2', pres_str)
        pres_str = re.sub(r'([0-9])\.([a-zA-Z])', '\\1 \\2', pres_str)

        # Remove spaces or commas between 0 based digits, so 100,000 or
        # 100 000 become 100000, I might need to refine this
        pres_str = re.sub(r'(0+)[\s,](0+)', '\\1\\2', pres_str)
        return re.sub(r',', '', pres_str)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def is_missing(self):
        """Check if the prescription string is missing.

        Raises
        ------
        ValueError
            If the prescription string is missing.
        """
        # There is a general function for this
        if common.is_missing(self.pres_str):
            raise ValueError("the prescription string is empty")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_tag(self, start, end, *names, update=True, is_valid=False):
        """Add a tag to a chunk in the prescription string.

        Parameters
        ----------
        start : `int`
            The start position of the chunk.
        end : `int`
            The end position of the chunk.
        *names : `str`
            One or more tag names for the chunk.
        update : `bool`, optional, default: `True`
            If the tag already exists then this new tag is added to it,
            otherwise it will overwrite it.
        is_valid : `bool`, optional, default: `False`
            If a new tag is being created then create it with the ``is_valid``
            flag set, any existing tags should be modified with
            the ``Tag.is_valid`` attribute.
        """
        # start = start + self.offset
        # end = end + self.offset
        names = self.prefix_func(names, self.prefix_tags)

        try:
            cur_tag = self._tags[(start, end)]
            if update is True:
                cur_tag.add_name(*names)
                self.splice_spaces(start, end)
        except KeyError:
            self._tags[(start + self.offset, end + self.offset)] = Tag(
                start + self.offset, end + self.offset,
                self.pres_str[start:end], *names, is_valid=is_valid
            )
            self.splice_spaces(start, end)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_tag_obj(self, tag_obj):
        """Add a tag object to the prescription string.

        Parameters
        ----------
        tag_obj : `dmd.parser.Tag`
            The tag object to add.

        Raises
        ------
        KeyError
            If a tag object with the same ``start``/``end`` positions already
            exists.

        Notes
        -----
        This is to add pre-created tags and performs a similar operation to
        `dmd.parser.PrescriptionString.add_tag`.
        """
        tag_obj.start += self.offset
        tag_obj.end += self.offset

        for i in self._prefix_tags:
            tag_obj.add_name(i)

        try:
            self._tags[(tag_obj.start, tag_obj.end)]
            raise KeyError("tag index already present")
        except KeyError:
            self._tags[(tag_obj.start, tag_obj.end)] = tag_obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def splice_spaces(self, start, end):
        """Acts on the ``dmd.parser.PrescriptionString.proc_str`` attribute and
        replaces the text between ``start``, ``end`` and replace with
        whitespace.

        Parameters
        ----------
        start : `int`
            The start position of the region.
        end : `int`
            The end position of the region.
        """
        self.proc_str = (
            self.proc_str[:start] +
            " " * (end - start) +
            self.proc_str[end:]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def tags(self):
        """Get all the tag objects.

        returns
        -------
        tags : `list` of `dmd.parser.Tag`

        Notes
        -----
        The returned objects are sorted on start position (ascending) and
        length (descending).
        """
        all_tags = []
        for i in self.brackets:
            all_tags.extend(i.tags)
        all_tags.extend(self._tags.values())
        all_tags = sorted(all_tags, key=lambda x: len(x))
        all_tags.sort(key=lambda x: x.start)
        return all_tags
        # tags = sorted(self._tags.values(), key=lambda x: len(x))
        # tags.sort(key=lambda x: x.start)
        # return tags

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_tags(self, *tag_names):
        """Extract tags in sort order that have specific tag names.

        Parameters
        ----------
        *tag_names : `str`
            One or more tag names to search/extract.

        Returns
        -------
        match_tag_names : `list` of `dmd.parser.Tag`
            Tags with matching tag names.
        """
        match_tags = []
        for i in self.tags:
            for j in tag_names:
                if j in i.name:
                    match_tags.append(i)
                    break
        return match_tags


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Tag(object):
    """A representation of a tag.

    Parameters
    ----------
    start : `int`
        The start position of the tag.
    end : `int`
        The end position of the tag.
    value : `str`
        The value of the tag.
    *names : `str`
        One or more names for the tag. names are stored internally in a set.
    is_valid : `bool`, optional, default: `False`
        Has the tag value been validated against a dictionary?
    """
    BRACKET_TAG = 'BRACKET'
    """The name for a bracket tag
    """
    SUPPLIER_TAG = 'SUPPLIER'
    """The name for a supplier tag
    """
    DRUG_TAG = 'DRUG'
    """The name for a drug tag
    """
    HOMEO_TAG = 'HOMEOPATHIC'
    """The name for a homeopathic remedy tag
    """
    INGREDIENT_TAG = 'INGREDIENT'
    """The name for an ingredient tag
    """
    UNIT_TAG = 'UNIT'
    """The name for a unit tag
    """
    OTHER_TAG = 'UNKNOWN'
    """The name for any untagged chunks tag
    """
    ROA_TAG = 'ROA'
    """The name for any route of administration chunk tag
    """
    FORM_TAG = 'FORM'
    """The name for any dose form chunk tag
    """
    DRUG_SALT = 'DRUG_SALT'
    """The name for any dose form chunk tag
    """
    PERSON_TYPE = 'PERSON_TYPE'
    """The name for any dose form chunk tag
    """
    QUAL_UNIT = 'QUAL_UNIT'
    """The name for any dose form chunk tag
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, start, end, value, *names, is_valid=False):
        self.start = start
        self.end = end
        self.value = value
        self.name = set(list(names))
        self.is_valid = is_valid

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """The length of the tag (in characters)
        """
        return "<{0}({1},{2},{3},{4},{5})>".format(
            self.__class__.__name__,
            f"start={self.start}",
            f"end={self.end}",
            f"length={len(self)}",
            f"value={self.value}",
            f"labels={'|'.join(self.name)}"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        """The length of the tag (in characters)
        """
        return self.end - self.start

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_name(self, *names):
        """Add one or more names to the tag

        Parameters
        ----------
        *names
            The names (`str`) to add.
        """
        self.name.update(names)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset_names(self, *new_names):
        """Remove all names in the tag and add new ones.

        Parameters
        ----------
        *new_names
            The new names (`str`) to initialise with.
        """
        self.name = set(list(new_names))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PrescriptionParser(object):
    """Attempt to ID drugs, concentrations and company names in a DM+D-like
    prescription string.
    """
    ROUND_BRACKET_SCAN = pyparsing.locatedExpr(
        pyparsing.originalTextFor(
            pyparsing.nestedExpr('(', ')')
        )
    )
    """A scanner for outermost brackets in potentially nested brackets
    (`pyparsing.locatedExpr`)
    """
    SQUARE_BRACKET_SCAN = pyparsing.locatedExpr(
        pyparsing.originalTextFor(
            pyparsing.nestedExpr('[', ']')
        )
    )
    """A scanner for outermost brackets in potentially nested brackets
    (`pyparsing.locatedExpr`)
    """
    TOKENS = re.compile(r'\b[^\s\t]+\b')
    """A regular expression to pull out tokens from the prescription string
    (has a word boundary delimiter) (`re.Pattern`)
    """
    GENERAL_NUMERIC = (
        (re.compile(r'\s(\d+-\d+)\s'), 'RANGE'),
        (re.compile(r'\s(\d+-\d+-\d+)\s'), 'PRODUCT_CODE'),
        (re.compile(
            r'\s{0}\s'.format(
                units._UnitParser.DIGIT_DEFINITION(1, named_group=False)
            )), 'NUMERIC'
         ),
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        self.load_drugs()
        self.load_salts()
        self.load_units()
        self.load_ingredients()
        self.load_dose_formulation()
        self.load_suppliers()
        self.load_homeopathic()
        self.load_person_type()
        self.load_qual_units()
        self.extractor = units.UnitExtract()
        self.static_map = examples.get_data('static_mappings')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_drugs(self):
        """Load the drug definition dictionary.
        """
        self.drugs = examples.get_data('drugs')
        self.drugs = sorted(
            self.drugs, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_salts(self):
        """Load the drug salt definitions dictionary.
        """
        self.drug_salts = examples.get_data('salts')
        self.drug_salts = sorted(
            self.drug_salts, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_units(self):
        """Load the unit definition dictionary.

        Notes
        -----
        The units have two columns one for the actual unit and one for the has
        reversal flag, that means we might expect ``<DIGIT><UNIT>`` or
        ``<UNIT><DIGIT>``, if so a regexp is compiled for both.
        """
        self.units = examples.get_data('units')
        # self.units = sorted(
        #     self.units, key=lambda x: len(x.token), reverse=True
        # )
        self.units = sorted(
            self.units, key=lambda x: len(x.token), reverse=False
        )

        compiled_units = []
        for u in self.units:
            has_reverse = False
            if u.token_sub_class == "reverse":
                has_reverse = True

            uc = re.compile(
                r'\b{0}\s*{1}\b'.format(
                    units.UnitRateParser.DIGIT_DEFINITION(1), u.token
                )
            )
            compiled_units.append(uc)
            if has_reverse is True:
                uc = re.compile(
                    r'\b{0}\s*{1}\b'.format(
                        u.token, units.UnitRateParser.DIGIT_DEFINITION(1)
                    )
                )
                compiled_units.append(uc)
        self.units = compiled_units

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_ingredients(self):
        """Load the ingredients definition dictionary.
        """
        self.ingredients = examples.get_data('ingredients')
        self.ingredients = sorted(
            self.ingredients, key=lambda x: len(x.token), reverse=True
        )
            # [i.token.lower() for i in examples.get_data('ingredients')]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_dose_formulation(self):
        """Load the dose formulation definition dictionary.
        """
        # self.forms = [i.token_regex for i in examples.get_data('forms')]
        self.forms = examples.get_data('forms')
        self.forms = sorted(
            self.forms, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_suppliers(self):
        """Load the suppliers definition dictionary.
        """
        # self.suppliers = [i.lower() for i in examples.get_data('suppliers')]
        self.suppliers = examples.get_data('suppliers')
        self.suppliers = sorted(
            self.suppliers, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_homeopathic(self):
        """Load the homeopathic definition dictionary.
        """
        # self.homeo = [
        #     i.token.lower() for i in examples.get_data('homeopathic')
        # ]
        self.homeo = examples.get_data('homeopathic')
        self.homeo = sorted(
            self.homeo, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_person_type(self):
        """Load the person type definition dictionary, i.e child, male, female.
        """
        # self.person_type = [i[0] for i in examples.get_data('person_type')]
        self.person_type = examples.get_data('person_type')
        self.person_type = sorted(
            self.person_type, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_qual_units(self):
        """Load the qualitative units dictionary.
        """
        self.qual_units = examples.get_data('qualitative_units')
        self.qual_units = sorted(
            self.qual_units, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self, pres_str, extract_brackets=True):
        """Parse the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """

        # Extract all the bracketed terms, these will be searched recursively
        # later
        brackets = []
        if extract_brackets is True:
            brackets = self.extract_brackets(pres_str)

        # Extract companies valid/unvalid - prop of non word characters?
        # min/max len? Loop back through matching brackets
        self.tag_suppliers(pres_str)

        # ID drugs
        self.tag_drugs(pres_str)

        # ID drug salts
        self.tag_drug_salts(pres_str)

        # ID homeopathic remedies
        self.tag_homeopathic(pres_str)

        # ID other ingredients
        self.tag_ingredients(pres_str)

        # ID units
        self.tag_units(pres_str)

        # TODO: Remove stop words (build list)
        # ID any indicators of forms, types or ROA
        self.tag_form(pres_str)

        # ID any qualitative units
        self.tag_qual_units(pres_str)

        # ID any indicators for types of people the prescription is for
        self.tag_person_type(pres_str)

        # ID Any other numeric values that might be useful
        self.tag_general_numeric(pres_str)

        # ID Other units
        self.tag_others(pres_str)

        # Now loop through any bracketed terms and parse them again.
        for i in brackets:
            # print(i, file=sys.stderr)
            self.parse(i, extract_brackets=False)
        pres_str.brackets = brackets

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def extract_brackets(self, pres_str):
        """Extract bracketed terms from the prescription string and create tags
        from them.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        brackets = []
        for j in [self.ROUND_BRACKET_SCAN, self.SQUARE_BRACKET_SCAN]:
            for i in j.search_string(pres_str.pres_str):
                start, match, end = i[0].as_list()
                bracket_str = PrescriptionString(
                    pres_str.pres_str[start+1:end-1],
                    prefix_tags=[Tag.BRACKET_TAG],
                    offset=start+1
                )
                # pres_str.add_tag(start+1, end-1, Tag.BRACKET_TAG)
                pres_str.splice_spaces(start, end)
                brackets.append(bracket_str)
        return brackets

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_suppliers(self, pres_str):
        """Tag the suppliers in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        # Not regexps
        # TODO: Add un-validated lookups for tags ending in Ltd/GmBH/Plc etc...
        self._tag_values(pres_str, self.suppliers, Tag.SUPPLIER_TAG)
        # for t in pres_str.get_tags(Tag.SUPPLIER_TAG):
        #     if t.value.lower() in self.suppliers:
        #         t.reset_names(Tag.SUPPLIER_TAG)
        #         t.is_valid = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_drugs(self, pres_str):
        """Tag the drugs in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        # Not regexps
        self._tag_values(pres_str, self.drugs, Tag.DRUG_TAG)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_drug_salts(self, pres_str):
        """Tag any drug salt names in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        for d in self.drug_salts:
            for j in d.token_regex.finditer(pres_str.proc_str):
                match_start = j.start()
                match_end = j.end()
                pres_str.add_tag(
                    match_start, match_end, Tag.DRUG_SALT, is_valid=True
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_homeopathic(self, pres_str):
        """Tag the drugs in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        # Not regexps
        self._tag_values(pres_str, self.homeo, Tag.HOMEO_TAG)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_ingredients(self, pres_str):
        """Tag the drugs in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        # Not regexps
        self._tag_values(pres_str, self.drugs, Tag.INGREDIENT_TAG)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_person_type(self, pres_str):
        """Tag the drugs in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        self._tag_values(pres_str, self.person_type, Tag.PERSON_TYPE)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_units(self, pres_str):
        """Tag the units in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        try:
            # Search for generalised units that are a bit more complex
            for u in self.extractor.extract_units(pres_str.proc_str):
                reg_match = u[0].regex
                label = u[0].label
                match_start = reg_match.start()
                match_end = reg_match.end()
                pres_str.add_tag(
                    match_start, match_end, label, is_valid=False
                )
        except KeyError:
            pass

        # Now search for "known" dictionary units
        for u in self.units:
            for j in u.finditer(pres_str.proc_str):
                match_start = j.start()
                match_end = j.end()
                pres_str.add_tag(
                    match_start, match_end, Tag.UNIT_TAG, is_valid=True
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_form(self, pres_str):
        """Tag the units in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        for i in self.forms:
            for j in i.token_regex.finditer(pres_str.proc_str):
                match_start = j.start()
                match_end = j.end()
                pres_str.add_tag(
                    match_start, match_end, Tag.FORM_TAG, is_valid=True
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_qual_units(self, pres_str):
        """Tag the units in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        # Now search for "known" dictionary units
        for i in self.qual_units:
            for j in i.token_regex.finditer(pres_str.proc_str):
                match_start = j.start()
                match_end = j.end()
                pres_str.add_tag(
                    match_start, match_end, Tag.QUAL_UNIT, is_valid=True
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_general_numeric(self, pres_str):
        """Tag any general numeric values in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        for i, label in self.GENERAL_NUMERIC:
            for j in i.finditer(pres_str.proc_str):
                match_start = j.start()
                match_end = j.end()
                # The +1/-1 are because all the general numeric regexps
                # have leading/lagging single whitespace. I must make this
                # more robust.
                pres_str.add_tag(
                    match_start+1, match_end-1, label, is_valid=False
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_others(self, pres_str):
        """Tag the remaining un-tagged strings prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        for i in self.TOKENS.finditer(pres_str.proc_str):
            match_start = i.start()
            match_end = i.end()
            pres_str.add_tag(
                match_start, match_end, Tag.OTHER_TAG, is_valid=False
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _tag_values(pres_str, search_values, *insert_tags):
        """Tag the drugs in the prescription string.

        Parameters
        ----------
        pres_str : `dmd.parser.PrescriptionString`
            The prescription string to process.
        """
        for d in search_values:
            # Not sure why I am not using itervalues here?
            while True:
                try:
                    # if insert_tags[0] == Tag.SUPPLIER_TAG:
                    #     print(pres_str, file=sys.stderr)
                    #     print(pres_str.proc_str, file=sys.stderr)
                    match_start = pres_str.proc_str.index(d.token_lower)
                    # if insert_tags[0] == Tag.SUPPLIER_TAG:
                    #     print(pres_str, file=sys.stderr)
                    #     print(pres_str.proc_str, file=sys.stderr)
                    # if insert_tags[0] == Tag.SUPPLIER_TAG:
                    #     print(pres_str, file=sys.stderr)
                    reg_match = re.search(
                        r'\b{0}\b'.format(re.escape(d.token_lower)),
                        pres_str.proc_str
                    )
                    # if insert_tags[0] == Tag.SUPPLIER_TAG:
                    #     print(reg_match, file=sys.stderr)
                    match_start = reg_match.start()
                    match_end = reg_match.end()
                    pres_str.add_tag(
                        match_start, match_end, *insert_tags,
                        is_valid=True
                    )
                except (AttributeError, ValueError):
                    break


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DBPrescriptionParser(PrescriptionParser):
    """Attempt to ID drugs, concentrations and company names in a DM+D-like
    prescription string.

    This uses dictionaries in the prescription string database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session attached to the prescription string database.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, session):
        self.session = session
        super().__init__()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_drugs(self):
        """Load the drug definition dictionary.
        """
        self.drugs = []

        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(
            and_(
                porm.ClassLkp.class_name == common.CLASS_DRUG,
                porm.SubClassLkp.sub_class_name != common.SUB_CLASS_DRUG_SALT,
                porm.SubClassLkp.sub_class_name != common.SUB_CLASS_HOMEO,
                porm.SubClassLkp.sub_class_name != common.SUB_CLASS_PLANT
            )
        )
        for row in q:
            d, scl, cl = row
            self.drugs.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.drugs = sorted(
            self.drugs, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_salts(self):
        """Load the drug salt definitions dictionary.
        """
        # Regexps
        self.drug_salts = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(
            and_(
                porm.ClassLkp.class_name == common.CLASS_DRUG,
                porm.SubClassLkp.sub_class_name == common.SUB_CLASS_DRUG_SALT
            )
        )
        for row in q:
            d, scl, cl = row
            self.drug_salts.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.drug_salts = sorted(
            self.drug_salts, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_units(self):
        """Load the unit definition dictionary.

        Notes
        -----
        The units have two columns one for the actual unit and one for the has
        reversal flag, that means we might expect ``<DIGIT><UNIT>`` or
        ``<UNIT><DIGIT>``, if so a regexp is compiled for both.
        """
        self.units = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(
            and_(
                porm.ClassLkp.class_name == common.CLASS_UNIT,
                porm.SubClassLkp.sub_class_name != common.SUB_CLASS_QUALITATIVE
            )
        )
        for row in q:
            d, scl, cl = row
            self.units.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )

        self.units = sorted(
            self.units, key=lambda x: len(x.token), reverse=False
        )

        compiled_units = []
        for u in self.units:
            has_reverse = False
            if u.token_sub_class == "reverse":
                has_reverse = True

            uc = re.compile(
                r'\b{0}\s*{1}\b'.format(
                    units.UnitRateParser.DIGIT_DEFINITION(1), u.token
                )
            )
            compiled_units.append(uc)
            if has_reverse is True:
                uc = re.compile(
                    r'\b{0}\s*{1}\b'.format(
                        u.token, units.UnitRateParser.DIGIT_DEFINITION(1)
                    )
                )
                compiled_units.append(uc)
        self.units = compiled_units

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_ingredients(self):
        """Load the ingredients definition dictionary.
        """
        self.ingredients = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(porm.ClassLkp.class_name == common.CLASS_ING)
        for row in q:
            d, scl, cl = row
            self.ingredients.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.ingredients = sorted(
            self.ingredients, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_dose_formulation(self):
        """Load the dose formulation definition dictionary.
        """
        self.forms = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(porm.ClassLkp.class_name == common.CLASS_ROA)
        for row in q:
            d, scl, cl = row
            self.forms.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.forms = sorted(
            self.forms, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_suppliers(self):
        """Load the suppliers definition dictionary.
        """
        self.suppliers = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(porm.ClassLkp.class_name == common.CLASS_SUPPLIER)
        for row in q:
            d, scl, cl = row
            self.suppliers.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.suppliers = sorted(
            self.suppliers, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_homeopathic(self):
        """Load the homeopathic definition dictionary.
        """
        self.homeo = []

        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(
            and_(
                porm.ClassLkp.class_name == common.CLASS_DRUG,
                or_(
                    porm.SubClassLkp.sub_class_name == common.SUB_CLASS_HOMEO,
                    porm.SubClassLkp.sub_class_name == common.SUB_CLASS_PLANT
                )
            )
        )
        for row in q:
            d, scl, cl = row
            self.homeo.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.homeo = sorted(
            self.homeo, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_person_type(self):
        """Load the person type definition dictionary, i.e child, male, female.
        """
        self.person_type = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(porm.ClassLkp.class_name == common.CLASS_PERSON)
        for row in q:
            d, scl, cl = row
            self.person_type.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.person_type = sorted(
            self.person_type, key=lambda x: len(x.token), reverse=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def load_qual_units(self):
        """Load the qualitative units dictionary.
        """
        self.qual_units = []
        q = self.session.query(
            porm.Dict, porm.SubClassLkp.sub_class_name,
            porm.ClassLkp.class_name
        ).select_from(porm.Dict).join(
            porm.SubClassLkp
        ).join(
            porm.ClassLkp
        ).filter(
            and_(
                porm.ClassLkp.class_name == common.CLASS_UNIT,
                porm.SubClassLkp.sub_class_name == common.SUB_CLASS_QUALITATIVE
            )
        )
        for row in q:
            d, scl, cl = row
            self.qual_units.append(
                examples.DictEntry(
                    token=d.name,
                    token_class=cl,
                    token_sub_class=scl,
                    is_regex=str(int(d.is_regexp)),
                    standard_token=d.standard_name or ""
                )
            )
        self.qual_units = sorted(
            self.qual_units, key=lambda x: len(x.token), reverse=True
        )
