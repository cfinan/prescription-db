"""Run the heuristic parser over the all the prescription strings in the
 database.
"""
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    orm as porm,
    common,
    parser_common as pc
)
from sqlalchemy_config import config as cfg
from pyaddons import log
from tqdm import tqdm
import argparse
import os
import sys
# import pprint as pp


_PROG_NAME = 'pres-db-parser'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_snomed.add_snomed``.
    """
    # Initialise and parse the command line arguments
    arg_parser = _init_cmd_args()
    args = _parse_cmd_args(arg_parser)
    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    sm = cfg.get_sessionmaker(
        args.config_section,
        common._DEFAULT_PREFIX,
        url_arg=args.pres_db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    session = sm()

    try:
        # Add the snomed relationships
        nadded = parse_strings(session, verbose=args.verbose,
                               drop=args.drop)
        logger.info(f"added #parsed strings: {nadded}")
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    arg_parser = argparse.ArgumentParser(
        description=_DESC
    )

    arg_parser.add_argument(
        'pres_db_url', type=str,
        help="The prescription database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    arg_parser.add_argument(
        '-v', '--verbose', action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    arg_parser.add_argument(
        '-D', '--drop', action="store_true",
        help="Drop any existing snomed tables rather than updating missing"
        " prescription strings"
    )
    arg_parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    arg_parser.add_argument(
        '-T', '--tmpdir', type=str,
        help="The location of the temp directory. Relationships are written"
        " to temp before being imported. If not supplied then the default"
        "system temp location is used."
    )
    arg_parser.add_argument(
        '-p', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name for the prescription DB in the config file"
    )
    return arg_parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(arg_parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = arg_parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.realpath(os.path.expanduser(args.config))
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_tokens(session):
    """Cache all of the tokens observed in the prescription strings, I have no
    feeling about how big this will get.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQLAlchemy session linked to the prescription database.

    Returns
    -------
    cache : `dict`
        The keys are the token values and the values are token IDs.
    """
    cache = dict()
    for row in session.query(porm.TokenLkp):
        cache[row.token_value] = row.token_id
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_tags(session):
    """Cache all of the tokens observed in the prescription strings, I have no
    feeling about how big this will get.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQLAlchemy session linked to the prescription database.

    Returns
    -------
    cache : `dict`
        The keys are the token values and the values are token IDs.
    """
    cache = dict()
    for row in session.query(porm.TagLkp):
        cache[row.tag_name] = row.tag_id
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_tags(session):
    """Cache all of the tokens observed in the prescription strings, I have no
    feeling about how big this will get.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQLAlchemy session linked to the prescription database.

    Returns
    -------
    cache : `dict`
        The keys are the token values and the values are token IDs.
    """
    cache = cache_tags(session)
    for i in pc.TAGS:
        try:
            cache[i]
        except KeyError:
            session.add(
                porm.TagLkp(tag_name=i)
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_strings(session, verbose=False, drop=False):
    """Add snomed relationships to the prescription string database.

    Parameters
    ----------
    pres_session : `sqlalchemy.Session`
        The session object attached to the prescription database.
    snomed_session : `sqlalchemy.Session`
        The session object attached to the snomed database.
    verbose : `bool`, optional, default: `False`
        Show progress of the input file parsing.
    distance : `int`, optional, default: `3`
        Query snomed relationships that are up to this many nodes away from the
        source prescription string.
    drop : `bool`, optional, default: `False`
        Drop any existing snomed tables before re-querying.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    if drop is True:
        # TODO: Handle backing up of manual annotations of parsed strings
        try:
            logger.info("dropping previous parse tables")
            porm.PresStringTokens.__table__.drop(session.get_bind())
            porm.TokenLkp.__table__.drop(session.get_bind())

            logger.info("ensuring all pres-db tables are in place")
            common.create_tables(session)
        except Exception:
            logger.info("ensuring all pres-db tables are in place")
            common.create_tables(session)

            # The exception probably means the table is not there so we see if
            # there are rows in it, is so we error
            nrows = session.query(porm.PresStringTokens).limit(10).count()
            nrows += session.query(porm.TokenLkp).limit(10).count()
            if nrows > 0:
                raise ValueError(
                    "parsed string table(s) still have data in them"
                )

    nadded = 0
    nmissing = missing_id_query(session).count()
    logger.info(f"need to process #prescriptions: {nmissing}")

    tqdm_kwargs = dict(
        total=nmissing,
        desc="[info] processing prescriptions",
        unit=" prescriptions",
        disable=not prog_verbose
    )

    # The prescription string parser
    presp = pc.DBPrescriptionParser(session)

    import_tags(session)
    tag_cache = cache_tags(session)
    token_cache = cache_tokens(session)

    break_at = -20
    row_buffer = []
    token_buffer = set()
    for idx, row in enumerate(tqdm(missing_id_query(session),
                                   **tqdm_kwargs)):
        pres = pc.PrescriptionString(row.pres_str_name)
        presp.parse(pres)
        for tidx, t in enumerate(pres.tags, 1):
            names = list(t.name)

            is_bracket = False
            if pc.BRACKET_TAG in names:
                is_bracket = True
                names.pop(names.index(pc.BRACKET_TAG))

            try:
                token_id = token_cache[t.value]
            except KeyError:
                token_id = None
                token_buffer.add(t.value)

            row_buffer.append(
                (
                    porm.PresStringTokens(
                        pres_str_id=row.pres_str_id,
                        token_id=token_id,
                        token_str_idx=tidx,
                        token_start_pos=t.start,
                        token_end_pos=t.end,
                        in_bracket=is_bracket,
                        tag_id=tag_cache[names[0]],
                        manual_tag_id=tag_cache[pc.UNCHECKED_TAG]
                    ),
                    t.value
                )
            )

        if len(row_buffer) >= 10000:
            row_buffer, token_buffer = commit_buffers(
                session, token_cache, row_buffer, token_buffer
            )

        nadded += 1
        if idx == break_at:
            break

    if len(row_buffer) > 0:
        row_buffer, token_buffer = commit_buffers(
            session, token_cache, row_buffer, token_buffer
        )
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def commit_buffers(session, token_cache, row_buffer, token_buffer):
    commit_buffer = []
    for i in token_buffer:
        commit_buffer.append(porm.TokenLkp(token_value=i))
    token_buffer = set()
    session.add_all(commit_buffer)
    session.flush()
    session.commit()
    for i in commit_buffer:
        token_cache[i.token_value] = i.token_id

    commit_buffer = []
    for pstr, tok in row_buffer:
        if pstr.token_id is None:
            pstr.token_id = token_cache[tok]
        commit_buffer.append(pstr)
    session.add_all(commit_buffer)
    session.flush()
    session.commit()
    row_buffer = []
    return row_buffer, token_buffer


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def missing_id_query(session):
    """Return a query for obtaining the prescription strings that have no
    snomed data associated with them.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object attached to the prescription string database.

    Returns
    -------
    query : `sqlalchemy.Query`
        An un-executed query object.
    """
    return session.query(
        porm.PresString
    ).join(
        porm.PresStringTokens,
        porm.PresStringTokens.pres_str_id == porm.PresString.pres_str_id,
        isouter=True
    ).filter(
        porm.PresStringTokens.pres_str_id == None
    ).group_by(
        porm.PresString.pres_str_id
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
