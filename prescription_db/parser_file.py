"""Parsing classes and functions, these attempt to ID entities in a
prescription string through rules and dictionary lookups.
"""
# Importing the version number (in __init__.py) and the package name
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    common,
    parse_common as pc,
    units
)
from prescription_db.example_data import examples
from tqdm import tqdm
import stdopen
import csv
import re
import os
import sys
import argparse
import pyparsing
import pprint as pp


_PROG_NAME = 'pres-db-parse-file'
"""The name of the program (`str`)
"""
INTERNAL_DELIMITER = "|"
"""The default internal delimiter symbol (`str`)
"""
PRESCRIPTION_TYPE_COL = 'pres_type'
"""The prescription type column name (`str`)
"""
INPUT_INDEX_COL = 'input_row_idx'
"""The input row index column name (`str`)
"""
CHUNK_INDEX_COL = 'chunk_idx'
"""The tagged chunk index column name (`str`)
"""
CHUNK_START_COL = 'chunk_start'
"""The tagged chunk start column name (`str`)
"""
CHUNK_END_COL = 'chunk_end'
"""The tagged chunk end column name (`str`)
"""
CHUNK_VALUE_COL = 'chunk_value'
"""The tagged chunk value column name (`str`)
"""
CHUNK_TAG_COL = 'chunk_tags'
"""The tagged chunk tag names column name (`str`)
"""
CHUNK_TAG_VALID_COL = 'chunk_tag_validated'
"""The tagged chunk is valid column name (`str`)
"""
OUTCOLS = [
    PRESCRIPTION_TYPE_COL,
    INPUT_INDEX_COL,
    CHUNK_INDEX_COL,
    CHUNK_START_COL,
    CHUNK_END_COL,
    CHUNK_VALUE_COL,
    CHUNK_TAG_COL,
    CHUNK_TAG_VALID_COL
]
"""The additional columns that will be output in the output file, in the order
they are output (`list` of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage to parse a file see
    `dmd.parser.parse_pres_file`
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = common.Msg(prefix='', verbose=args.verbose, file=common.MSG_OUT)
    m.msg("= {2} ({0} v{1}) =".format(
        pkg_name, __version__, _PROG_NAME
    ))
    m.prefix = common.MSG_PREFIX
    m.msg_atts(args)

    try:
        parse_pres_file(
            args.infile, args.outfile, verbose=args.verbose, delimiter="\t",
            pres_str_col=args.pres_str_col, index=args.index
        )

        m.msg("*** END ***", prefix="")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        m.msg("*** INTERRUPT ***", prefix="")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description="Parse prescription strings into their components."
    )
    parser.add_argument('-x', '--index',
                        type=int,
                        nargs='+',
                        help="Only process certain row numbers")
    parser.add_argument('-i', '--infile',
                        type=str,
                        help="The input data file to map, if not provided "
                        "input will be from <STDIN>")
    parser.add_argument('-o', '--outfile',
                        type=str,
                        help="The output data file, if not provided "
                        "input will be from <STDOUT>")
    parser.add_argument('-T', '--tmp',
                        type=str,
                        help="The location of tmp, if not provided will "
                        "use the system tmp")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")
    parser.add_argument(
        '-P', '--pres-str-col', default=None,
        help="The column name containing the prescription string in the"
        " input file."
    )
    parser.add_argument(
        '--delimiter', default='\t',
        help="The delimiter for the input file."
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    if args.index is None:
        args.index = []
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_pres_file(infile, outfile, verbose=False, delimiter="\t",
                    pres_str_col=None, index=None):
    """Parse the prescription strings containing in a file.

    Parameters
    ----------
    infile : `str` or `NoneType`
        A path to the input file. ``NoneType`` or ``-`` is interpreted as
        STDIN.
    outfile : `str` or `NoneType`
        A path to the output file. ``NoneType`` or ``-`` is interpreted as
        STDOUT.
    verbose : `bool`, optional, default: `False`
        Show parsing progress.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input (and by extension the output) file.
    pres_str_col : `str`, optional, default: `NoneType`
        The column name of the prescription string column. The input file must
        have a header.
    """
    if index is not None and len(index) == 0:
        index = None

    # This is a template dictionary that will be added to each output row and
    # filled in with the current row data
    output_dict = dict([(i, None) for i in OUTCOLS])
    # reader = csv.DictReader(infile, delimiter=delimiter)
    # break_at = 1000
    break_at = 2000000

    # The prescription string parser
    presp = pc.PrescriptionParser()

    # input from file or STDIN
    with stdopen.open(infile) as incsv:
        # print(incsv)
        reader = csv.DictReader(incsv, delimiter=delimiter)
        # Output to file or STDOUT
        with stdopen.open(outfile, 'wt') as outfile:
            out_header = reader.fieldnames + OUTCOLS
            writer = csv.DictWriter(
                outfile, out_header, delimiter=delimiter,
                lineterminator=os.linesep
            )
            writer.writeheader()

            for c, row in enumerate(
                    tqdm(reader, desc="[info] Processing...", unit=' rows'), 1
            ):
                if index is not None:
                    if c not in index:
                        continue
                # The base output row has input row columns and the template
                # output row
                outrow = {**row, **output_dict}

                # Add the row index value to the output
                outrow[INPUT_INDEX_COL] = c

                try:
                    # Attempt to locate the prescription string
                    pres_string = row[pres_str_col]
                except KeyError as e:
                    raise KeyError(
                        "can't find prescription name column: '{0}'".format(
                            pres_str_col
                        )
                    ) from e

                try:
                    pres = pc.PrescriptionString(pres_string)
                except ValueError:
                    # The ValueError could be missing data if so we ignore if
                    # not we raise
                    if common.is_missing(pres_string) is False:
                        raise
                    writer.writerow(outrow)
                    continue

                presp.parse(pres)

                output = False
                for idx, t in enumerate(pres.tags, 1):
                    cur_out_row = outrow.copy()
                    cur_out_row[CHUNK_INDEX_COL] = idx
                    cur_out_row[CHUNK_START_COL] = t.start
                    cur_out_row[CHUNK_END_COL] = t.end
                    cur_out_row[CHUNK_VALUE_COL] = t.value
                    cur_out_row[CHUNK_TAG_COL] = \
                        INTERNAL_DELIMITER.join(t.name)
                    cur_out_row[CHUNK_TAG_VALID_COL] = int(t.is_valid)
                    output = True
                    writer.writerow(cur_out_row)

                if output is False:
                    writer.writerow(outrow)

                if c >= break_at:
                    break
