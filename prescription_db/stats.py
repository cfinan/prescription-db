"""Descriptive statistics on the prescription string database.
"""
from prescription_db import (
    __version__,
    __name__ as pkg_name,
    orm as porm,
    common
)
from snomed_ct import common as scommon
from sqlalchemy_config import config as cfg
from pyaddons import log
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from sqlalchemy.exc import IntegrityError
from tqdm import tqdm
import plotly.graph_objects as go
import argparse
import re
import os
import sys
import pandas as pd
import pprint as pp


_PROG_NAME = 'dmd-pres-db-stats'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""

TABLE_IMAGES_SUB_DIR = "table_images"
"""The sub-directory name for table images
"""
TABLE_RST_SUB_DIR = "table_rst"
"""The sub-directory name for table restructured text
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``dmd.pres_db.add_dict``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    pres_sm = cfg.get_sessionmaker(
        args.config_section, common._DEFAULT_PREFIX,
        url_arg=args.pres_db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    pres_session = pres_sm()

    try:
        # Add the dict file
        run_stats(pres_session, args.outdir, verbose=args.verbose)
        # Create a temp directory that we will blitz if we error out
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        pres_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'pres_db_url',
        nargs='?',
        type=str,
        help="The prescription database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    parser.add_argument(
        '-o', '--outdir',
        type=str, default="~/",
        help="The location of the output directory"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.realpath(os.path.expanduser(args.config))
    args.outdir = os.path.realpath(os.path.expanduser(args.outdir))
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_stats(session, outdir, verbose=False):
    """Run the battery of descriptive stats.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription database.
    outdir : `str`
        The root of the output directory.
     verbose : `bool`, optional, default: `False`
        Report progress through the stats.
    """
    table_images_dir = os.path.join(outdir, TABLE_IMAGES_SUB_DIR)
    table_rst_dir = os.path.join(outdir, TABLE_RST_SUB_DIR)

    for i in [table_images_dir, table_rst_dir]:
        os.makedirs(i, exist_ok=True)

    run_counts(session, outdir, verbose=verbose)
    run_no_dmd_namespace(session, outdir, verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_image_table(df, outdir, basename, width=1024, height=375, scale=3,
                       columnwidth=None):
    """Output the data frame as a table image.

    Parameters
    ----------
    df : `pandas.DataFrame`
        The dataframe to output.
    outdir : `str`
        The output directory name.
    basename : `str`
        The base name of the output file.
    width : `int`, optional, default: `1024`
        The width of the image.
    height : `int`, optional, default: `1024`
        The height of the image.
    scale : `int`, optional, default: `3`
        A scaling factor for the width/height.
    """
    align = ['center'] * len(df.columns)
    align[0] = 'left'
    align[-1] = 'right'

    layout = go.Layout(
        autosize=True,
        # transparent background
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
    )
    fig = go.Figure(
        layout=layout,
        data=[
            go.Table(
                columnwidth=columnwidth,
                header=dict(
                    values=list(df.columns),
                    align=align
                ),
                cells=dict(
                    values=df.transpose(),
                    align=align
                )
            )
        ]
    )
    # fig.update_layout(width=5000, height=300)
    outpath = os.path.join(outdir, basename)
    fig.write_image(outpath, width=width, height=height, scale=scale)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_rst_table(df, outdir, basename, name="data", columnwidth=None):
    """Output the data frame as a restructured text table.

    Parameters
    ----------
    df : `pandas.DataFrame`
        The dataframe to output.
    outdir : `str`
        The output directory name.
    basename : `str`
        The base name of the output file.
    width : `int`, optional, default: `1024`
        The width of the image.
    height : `int`, optional, default: `1024`
        The height of the image.
    scale : `int`, optional, default: `3`
        A scaling factor for the width/height.

    Notes
    -----
    This outputs a csv table:
    .. csv-table:: Frozen Delights!
      :header: "Treat", "Quantity", "Description"
      :widths: 15, 10, 30

      "Albatross", 2.99, "On a stick!"
      "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
      crunchy, now would it?"
     "Gannet Ripple", 1.99, "On a stick!"
    """
    inset = " " * 3
    outpath = os.path.join(outdir, basename)

    if columnwidth is None:
        columnwidth = [10] * df.shape[1]

    with open(outpath, 'wt') as outfile:
        outfile.write(f".. csv-table:: {name}\n")
        outfile.write(
            "{0}:header: {1}\n".format(
                inset,
                ", ".join([f'"{i}"' for i in df.columns])
            )
        )
        outfile.write(
            "{0}:widths: {1}\n".format(
                inset,
                ", ".join([str(i) for i in columnwidth])
            )
        )
        outfile.write("\n")
        for i in df.itertuples(index=False):
            outfile.write(
                "{0}{1}\n".format(
                    inset,
                    ", ".join([f'"{j}"' for j in i])
                )
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_table_height(df, scale=20.5, header_row_ratio=1.357):
    """Get the table height based on the number of rows in the output data.

    Parameters
    ----------
    df : `pandas.DataFrame`
        The data frame to size.
    scale : `float`, optional, default: `20.5`
        The scaling factor for each row.
    header_row_ratio : `float`, optional, default: `1.357`
        The ratio of the header row size to row size.

    Returns
    -------
    height : `float`
        The height for the table image.
    """
    # 57 px 28.5
    # 42 px 21.0
    # 22.4
    return (df.shape[0] * scale) + (1.357 * scale)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_counts(session, outdir, verbose=False):
    """Run summary counts of number so prescription strings in the various
    sources.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription database.
    outdir : `str`
        The output directory name.
    verbose : `bool`, optional, default: `False`
        Report progress through the stats.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    table_images_dir = os.path.join(outdir, TABLE_IMAGES_SUB_DIR)
    table_rst_dir = os.path.join(outdir, TABLE_RST_SUB_DIR)

    # Get the total count of the number of prescription strings
    nstrings = session.query(porm.PresString.pres_str_id).count()
    total_count = pd.DataFrame([nstrings], columns=['# prescription str.'])

    total_count_base = "pres-str-total-count"
    output_image_table(
        total_count, table_images_dir, f"{total_count_base}.png",
        width=200, height=get_table_height(total_count), scale=2
    )
    output_rst_table(
        total_count, table_rst_dir, f"{total_count_base}.rst",
        name="total count", columnwidth=None
    )

    # Get counts of the number of prescription strings in each source
    o = pd.DataFrame(get_occurs(session, verbose=prog_verbose))
    trunc_count = _get_truncated_counts(o)

    # Output the data as a table image
    source_counts = "pres-str-source-counts"
    output_image_table(
        trunc_count, table_images_dir, f"{source_counts}.png",
        width=620, height=get_table_height(trunc_count), scale=2
    )
    output_rst_table(
        trunc_count, table_rst_dir, f"{source_counts}.rst",
        name="source counts", columnwidth=None
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_truncated_counts(o):
    """Get a table of total counts and the number of truncated for each source.

    Parameters
    ----------
    o : `pandas.DataFrame`
        The data from the occurrence query.

    Returns
    -------
    counts : `pandas.DataFrame`
        The counts data
    """
    # Get the total counts for each source
    z = pd.DataFrame(o['data_source_pretty'].value_counts())

    for i in ['is_truncated', 'is_dmd']:
        # Get the counts of truncated by source data group
        x = o.groupby(
            ['data_source_pretty', i]
        )['pres_str_id'].count()

        # This adds in 0's for those sources with no truncations
        idx = pd.MultiIndex.from_product(
            [x.index.levels[0], [True, False]],
            names=['data_source_pretty', i]
        )
        x = x.reindex(idx, fill_value=0)

        # Extract only the truncated rows 0 or above
        y = x.loc[(x.index.get_level_values(i) == True)].\
            reset_index()[['data_source_pretty', 'pres_str_id']].\
            set_index('data_source_pretty')

        # Add the truncated back to the total counts
        z[i] = y['pres_str_id']

    z = z.reset_index()
    z.columns = ['data source', '# prescriptions', '# truncated', '# DM+D']

    z['# not DM+D'] = z['# prescriptions'] - z['# DM+D']
    z['% DM+D'] = ((z['# DM+D'] / z['# prescriptions']) * 100).round(2)
    return z


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_no_dmd_namespace(session, outdir, verbose=False):
    """Get counts of the namespace IDs for codes that do not map to the DM+D.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object bound to the prescription database.
    outdir : `str`
        The output directory name.
    verbose : `bool`, optional, default: `False`
        Report progress through the stats.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    table_images_dir = os.path.join(outdir, TABLE_IMAGES_SUB_DIR)
    table_rst_dir = os.path.join(outdir, TABLE_RST_SUB_DIR)

    # Get the occurrences
    o = pd.DataFrame(get_occurs(session, verbose=prog_verbose))
    o = o.loc[o.is_dmd == False, ['data_source_pretty', 'pres_str_id']].\
        set_index('pres_str_id')
    # print(o)

    # Get the strings and DM+D codes, missing codes have a -1
    s = pd.DataFrame(get_strings(session, verbose=prog_verbose))
    # print(s)

    # Run the validation over all the codes, this will produce a validation
    # table with the namespace in it
    v = pd.DataFrame(_validate_codes(s)).set_index('pres_str_id')

    # Merge the occurrences with the validation
    ov = o.merge(
        v, left_index=True, right_index=True
    )[['data_source_pretty', 'namespace_id', 'organisation']]

    # Get counts of the namespaces that occur in the codes attached to non-DM+D
    # strings
    ns_counts = ov.groupby(
        ['data_source_pretty', 'namespace_id', 'organisation']
    )['organisation'].count()
    ns_counts.name = '# codes'
    ns_counts = pd.DataFrame(ns_counts).reset_index()
    ns_counts.columns = [
        'data source', 'namespace ID', 'organisation', '# codes'
    ]

    no_nsp = "pres-str-no-dmd-namespace"
    output_image_table(
        ns_counts, table_images_dir, f"{no_nsp}.png",
        width=800, height=get_table_height(ns_counts), scale=2
    )
    output_rst_table(
        ns_counts, table_rst_dir, f"{no_nsp}.rst",
        name="namespace counts", columnwidth=None
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _validate_codes(s):
    """Yield code validations.

    Parameters
    ----------
    s : `pandas.DataFrame`
        A dataframe of prescription strings and codes must have column names
        ``pres_str_id`` and ``pres_str_code``.

    Yields
    ------
    validation : `dict`
        A validation dictionary for the prescription Snomed code. Codes that
        are `NoneType` are included as unvalidated.
    """
    parser = scommon.SnomedCodeParser()
    for pid, pc in zip(s.pres_str_id, s.pres_str_code):
        val = parser.parse(pc)._asdict()
        val['pres_str_id'] = pid
        yield val


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_occurs(session, verbose=False):
    """Yield prescription string ID occurrences and their sources.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session bound to the prescription database.
    verbose : `bool`, optional, default: `False`
        Log query progress.
    Notes
    -----
    Please note that the prescription strings are not unique as their could be
    > 1 code per string. Strings with no codes with no are also returned.
    """
    q = session.query(
        porm.PresStringOccurs.pres_str_id,
        porm.PresStringOccurs.source_version_id,
        porm.PresString.is_truncated,
        porm.PresString.is_dmd,
        porm.SourceVersion.version_name,
        porm.DataSource.data_source_id,
        porm.DataSource.data_source_pretty
    ).join(
        porm.SourceVersion,
        porm.SourceVersion.source_version_id ==
        porm.PresStringOccurs.source_version_id
    ).join(
        porm.DataSource,
        porm.DataSource.data_source_id ==
        porm.SourceVersion.data_source_id
    ).join(
        porm.PresString,
        porm.PresString.pres_str_id ==
        porm.PresStringOccurs.pres_str_id
    )

    tqdm_kwargs = dict(
        disable=not verbose,
        desc="[info] getting occurrences",
        unit=" rows",
        leave=False
    )
    for i in tqdm(q, **tqdm_kwargs):
        yield i


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_strings(session, verbose=False):
    """Yield prescription strings and codes.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session bound to the prescription database.
    verbose : `bool`, optional, default: `False`
        Log query progress.
    Notes
    -----
    Please note that the prescription strings are not unique as their could be
    > 1 code per string. Strings with no codes with no are also returned.
    """
    q = session.query(
        porm.PresString.pres_str_id,
        porm.PresString.pres_str_name,
        porm.PresString.is_truncated,
        porm.PresString.is_dmd,
        porm.PresStringCode.pres_str_code
    ).join(
        porm.PresStringCode,
        porm.PresStringCode.pres_str_id == porm.PresString.pres_str_id,
        isouter=True
    )

    tqdm_kwargs = dict(
        disable=not verbose,
        desc="[info] getting prescription strings",
        unit=" rows",
        leave=False
    )
    for i in tqdm(q, **tqdm_kwargs):
        # yield i
        strings = i._asdict()
        if strings['pres_str_code'] is None:
            strings['pres_str_code'] = -1
        yield strings
