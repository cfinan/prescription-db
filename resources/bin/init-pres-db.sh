#!/bin/bash
DB="$1"
cur_dir="$PWD"
script_dir="$(dirname $0)"
cd "$script_dir"
echo "*** initialise prescription-DB ***" 1>&2
echo "[info] DB path: $DB" 1>&2
if [[ -e "$DB" ]]; then
   echo "[info] $DB exists, this will delete and re-create? [CTRL-C to exit]" 1>&2
   read go
   rm "$DB"
fi

dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_drugs.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_form.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_hom.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_ingredients.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_person_types.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_qual_units.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_salts.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_suppliers.txt "$DB"
dmd-pres-db-dict -vv ../../dmd/example_data/example_datasets/dmd_dict_units.txt "$DB"

cd "$cur_dir"
echo "*** END ***" 1>&2

