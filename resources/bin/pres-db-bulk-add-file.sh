#!/bin/bash
abort() {
    echo "[info] aborting..." 1>&2
    if [[ -e "$BAK_DB" ]]; then
        cp "$BAK_DB" "$PRES_DB"
    fi
    exit 1
}

PROG_NAME="=== pres-db-bulk-add-file ==="
USAGE=$(
cat <<EOF
$PROG_NAME
Bulk load the available EHR prescription summary data into the
prescription string database. The files should have the preset names:

cprd_gold_gemscript_dmd_2020_07.txt.gz
cprd_aurum.txt.gz
ukbb_prescription_counts.txt.gz

With defined columns that can be seen in the source code of the script.
This is not really designed as a generalisable script but more of a
convienience script for loading the data in the prescription string
characterisation project. The <pres-str-db> should be the SQLite DM+D
database and the <file-dir> should be the root of the directory that
contains the files listed above.

USAGE: $0 [flags] <pres-str-db> <file-dir>
EOF
)
. shflags

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Make sure the root directory is set
if [[ -z "$1" ]]; then
  echo "[error] no prescription string database defined" 2>&1
  exit 1
fi

# Make sure the root directory is set
if [[ -z "$2" ]]; then
  echo "[error] no file root dir defined" 2>&1
  exit 1
fi

# exit on error and undef not allowed
set -eu

# source in some common functions
run_dir="$(dirname $0)"

PRES_DB=$(readlink -f "$1")
ROOT_DIR=$(readlink -f "$2")

BAK_DB=$PRES_DB."BAK"
cp "$PRES_DB" "$BAK_DB"

echo "$PROG_NAME"
echo "[info] prescription string database: $PRES_DB"
echo "[info] file root dir: $ROOT_DIR"

curdir="$PWD"

dmd-pres-db-file -vv -P"UK biobank" \
                 --delimiter $'\t' \
                 --pres-str "drug_name" \
                 --dmd-code "dmd_code" \
                 --other-codes "bnf_code" \
                 --code-types "bnf" \
                 --count "count" \
                 "$ROOT_DIR"/ukbb_prescription_counts.txt.gz \
                 "uk_biobank" \
                 "20220901" \
                 "$PRES_DB"

dmd-pres-db-file -vv \
                 -P"CPRD aurum" \
                 --delimiter "," \
                 --pres-str "termfromemis" \
                 --dmd-code "prodcodeid" \
                 --other-codes "bnf" \
                 --code-types "bnf" \
                 --count "n" \
                 "$ROOT_DIR"/cprd_aurum.txt.gz \
                 "cprd_aurum" \
                 "20190101" \
                 "$PRES_DB"

dmd-pres-db-file -vv \
                 -P"CPRD gold" \
                 --pres-str "productname" \
                 --dmd-code "dmdcode" \
                 --other-codes "gemscriptcode" \
                 --code-types "gemscript" \
                 --metadata-cols "drugsubstance" "strength" "formulation" "route" \
                 --metadata-type "ingredient" "strength" "form" "route" \
                 -- \
                 "$ROOT_DIR"/cprd_gold_gemscript_dmd_2020_07.txt.gz \
                 "cprd_gold" \
                 "20200701" \
                 "$PRES_DB"
echo "*** END ***"
