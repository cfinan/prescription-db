#!/bin/bash
abort() {
    echo "[info] aborting..." 1>&2
    if [[ -e "$BAK_DB" ]]; then
        cp "$BAK_DB" "$PRES_DB"
    fi
    exit 1
}

PROG_NAME="=== pres-db-bulk-add-dmd ==="
USAGE=$(
cat <<EOF
$PROG_NAME
Perform a full end-to-end build of the prescription string database, this will:

1. Import the DM+D versions
2. Insert all of the EHR files
3. Add the SnomedCT annotations

This could take a while to run, I would allow 24-36 hours.

USAGE: $0 [flags] <pres-str-db> <dmd-dbs> <file-dir> <snomed-ct-db>

The <snomed-ct-db> can be a path to an SQLite database or an intry in a
config file.
EOF
)
. shflags

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Make sure the root directory is set
if [[ -z "$1" ]]; then
  echo "[error] no prescription string database defined" 2>&1
  exit 1
fi

# Make sure the root directory is set
if [[ -z "$2" ]]; then
    echo "[error] no DM+D database directory defined" 2>&1
    exit 1
fi

# Make sure the root directory is set
if [[ -z "$3" ]]; then
  echo "[error] no file root dir defined" 2>&1
  exit 1
fi

# Make sure the root directory is set
if [[ -z "$4" ]]; then
    echo "[error] no snomed-ct version defined" 2>&1
    exit 1
fi

# exit on error and undef not allowed
set -eu

# source in some common functions
run_dir="$(dirname $0)"

PRES_DB=$(readlink -f "$1")
DMD_DIR=$(readlink -f "$2")
ROOT_DIR=$(readlink -f "$3")
SNOMED_CT="$4"

BAK_DB=$PRES_DB."BAK"
cp "$PRES_DB" "$BAK_DB"

echo "$PROG_NAME"
echo "[info] prescription string database: $PRES_DB"
echo "[info] DM+D dir: $DMD_DIR"
echo "[info] file root dir: $ROOT_DIR"
echo "[info] Snomed-CT: $SNOMED_CT"

curdir="$PWD"

./pres-db-bulk-add-dmd.sh "$PRES_DB" "$DMD_DIR"
./pres-db-bulk-add-file.sh "$PRES_DB" "$ROOT_DIR"
./pres-db-snomed --pres-db-url "$PRES_DB" --snomed-db-url "$SNOMED_CT" -d 4
echo "*** END ***"
