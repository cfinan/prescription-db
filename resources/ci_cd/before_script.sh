#!/bin/sh
# This is assumed to be running from the root of the repo
echo "[before] running in: $PWD"
# For debugging
echo $CI_COMMIT_BRANCH
echo $CI_DEFAULT_BRANCH
python --version
git branch

# Needed for Sphinx (make in build-base) and pysam (build-base + others)
apk add build-base bzip2-dev zlib-dev xz-dev openblas-dev linux-headers

# Upgrade pip first
pip install --upgrade pip

# Sphinx doc building requirements
pip install -r resources/ci_cd/sphinx.txt

# I have no idea but it can't find kaleido!? It is there
# but and that screws other installs so I will hack it out
cp requirements.txt use_requirements.txt
sed -i '/kaleido/d' use_requirements.txt
# Package requirements
cat use_requirements.txt
python -m pip install -r use_requirements.txt

pip install kaleido
# Install the package being built/tested
pip install .
