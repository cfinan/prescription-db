#!/bin/sh
# This is assumed to be running from the root of the repo
echo "[pages] running in: $PWD"

# Ensure the documentation build directory is present
mkdir -p ./docs/build

cp README.md docs/source/getting_started.md
sed -i 's/\.\/docs\/source\///' docs/source/getting_started.md
sed -i 's|src="\./resources/images|src="./_static/images|'  docs/source/getting_started.md

# Copy the images over to the source static directory
echo "[pages] copying images"
mkdir -p docs/source/_static
cp ./resources/images/*.png ./docs/source/_static/

orm-doc-schema -o ./docs/source/data_dict/db-tables.rst ./prescription_db/orm.py
orm-doc-api -o ./docs/source/data_dict/pres-db-orm.rst ./prescription_db/orm.py "prescription_db.orm"
ls -1 ./docs/source/data_dict

# Copy all the stats tables over
mkdir -p ./docs/source/stats
cp ./resources/stats/table_rst/*.rst ./docs/source/stats/

cd docs
make html
cd ..

echo "[pages] removing public"
if [[ -e public ]]; then
    rm -r public
fi
echo "[pages] creating public"
mkdir -p public
echo "[pages] moving HTML to public"
mv docs/build/html/* public/
echo "[pages] PWD: $PWD"
echo "[pages] contents of public"
ls "$PWD"/public
