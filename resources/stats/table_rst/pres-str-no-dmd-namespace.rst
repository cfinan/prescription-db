.. csv-table:: namespace counts
   :header: "data source", "namespace ID", "organisation", "# codes"
   :widths: 10, 10, 10, 10

   "CPRD aurum", "0", "SNOMED International", "15"
   "CPRD aurum", "1000000", "NHS Digital, UK", "1"
   "CPRD aurum", "1000001", "NHS Digital, UK", "2"
   "CPRD aurum", "1000006", "EMIS (Egton Medical Information Systems Ltd)", "1"
   "CPRD aurum", "1000027", "In Practice Systems", "1"
   "CPRD aurum", "1000033", "EMIS", "11529"
   "CPRD gold", "-1", "UNKNOWN", "2400"
   "CPRD gold", "0", "SNOMED International", "26"
   "CPRD gold", "1000001", "NHS Digital, UK", "51"
   "CPRD gold", "1000027", "In Practice Systems", "19453"
   "UK biobank", "-1", "UNKNOWN", "21668"
   "UK biobank", "0", "SNOMED International", "11051"
   "UK biobank", "1000001", "NHS Digital, UK", "16230"
   "UK biobank", "1000027", "In Practice Systems", "15362"
   "UK biobank", "1000033", "EMIS", "679"
