.. csv-table:: source counts
   :header: "data source", "# prescriptions", "# truncated", "# DM+D", "# not DM+D", "% DM+D"
   :widths: 10, 10, 10, 10, 10, 10

   "DM+D", "298583", "0", "298583", "0", "100.0"
   "UK biobank", "80679", "2963", "20841", "59838", "25.83"
   "CPRD gold", "79530", "0", "60071", "19459", "75.53"
   "CPRD aurum", "59416", "1", "47901", "11515", "80.62"
